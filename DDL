


CREATE TABLE `level` (
    `id_level` varchar(10) NOT NULL,
    `nama_level` varchar(50) NOT NULL,
    PRIMARY KEY (`id_level`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `user` (
    `id_user` varchar(10) NOT NULL,
    `username` varchar(50) DEFAULT NULL,
    `password` varchar(15) DEFAULT NULL,
    `nama_admin` varchar(15) DEFAULT NULL,
    `id_level` varchar(10) DEFAULT NULL,
    PRIMARY KEY (`id_user`),
    KEY `FK_USER_LEVEL` (`id_level`),
    CONSTRAINT `FK_USER_LEVEL` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`) 
        ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `tarif` (
    `id_tarif` varchar(10) NOT NULL,
    `daya` int(11) NOT NULL,
    `tarifperkwh` float NOT NULL,
    PRIMARY KEY (`id_tarif`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `pelanggan` (
    `id_pelanggan` varchar(10) NOT NULL,
    `username` varchar(50) NOT NULL,
    `password` varchar(15) NOT NULL,
    `nomor_kwh` varchar(15) NOT NULL,
    `nama_pelanggan` varchar(50) NOT NULL,
    `alamat` varchar(100) DEFAULT NULL,
    `id_tarif` varchar(10) NOT NULL,
    PRIMARY KEY (`id_pelanggan`),
    KEY `FK_TARIF_PELANGGAN` (`id_tarif`),
    CONSTRAINT `FK_TARIF_PELANGGAN` FOREIGN KEY (`id_tarif`) REFERENCES `tarif` (`id_tarif`) 
        ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `penggunaan` (
    `id_penggunaan` varchar(10) NOT NULL,
    `id_pelanggan` varchar(10) NOT NULL,
    `bulan` varchar(2) NOT NULL,
    `tahun` varchar(4) NOT NULL,
    `meter_awal` float NOT NULL,
    `meter_akhir` float NOT NULL,
    PRIMARY KEY (`id_penggunaan`),
    KEY `FK_PENGGUNAAN_PELANGGAN` (`id_pelanggan`),
    CONSTRAINT `FK_PENGGUNAAN_PELANGGAN` FOREIGN KEY (`id_pelanggan`) 
    REFERENCES `pelanggan` (`id_pelanggan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tagihan` (
    `id_tagihan` varchar(10) NOT NULL,
    `id_penggunaan` varchar(10) NOT NULL,
    `id_pelanggan` varchar(10) NOT NULL,
    `bulan` varchar(2) NOT NULL,
    `tahun` varchar(4) NOT NULL,
    `jumlah_meter` float NOT NULL,
    `status` varchar(20) NOT NULL,
    PRIMARY KEY (`id_tagihan`),
    KEY `FK_TAGIHAN_PENGGUNAAN` (`id_penggunaan`),
    KEY `FK_TAGIHAN_PELANGGAN` (`id_pelanggan`),
    CONSTRAINT `FK_TAGIHAN_PELANGGAN` FOREIGN KEY (`id_pelanggan`) 
        REFERENCES `pelanggan` (`id_pelanggan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_TAGIHAN_PENGGUNAAN` FOREIGN KEY (`id_penggunaan`) 
        REFERENCES `tagihan` (`id_tagihan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `pembayaran` (
    `id_pembayaran` varchar(10) NOT NULL,
    `id_tagihan` varchar(10) NOT NULL,
    `id_pelanggan` varchar(10) NOT NULL,
    `tanggal_pembayaran` varchar(50) NOT NULL DEFAULT '',
    `bulan_bayar` varchar(2) NOT NULL,
    `biaya_admin` float NOT NULL,
    `total_bayar` float NOT NULL,
    `id_user` varchar(10) NOT NULL,
    PRIMARY KEY (`id_pembayaran`),
    KEY `FK_PEMBAYARAN_TAGIHAN` (`id_tagihan`),
    KEY `FK_PEMBAYARAN_PELANGGAN` (`id_pelanggan`),
    KEY `FK_PEMBAYARAN_USER` (`id_user`),
    CONSTRAINT `FK_PEMBAYARAN_PELANGGAN` FOREIGN KEY (`id_pelanggan`) 
        REFERENCES `pelanggan` (`id_pelanggan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_PEMBAYARAN_TAGIHAN` FOREIGN KEY (`id_tagihan`) 
        REFERENCES `tagihan` (`id_tagihan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT `FK_PEMBAYARAN_USER` FOREIGN KEY (`id_user`) 
        REFERENCES `user` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;




# INSERT DATA

INSERT INTO tagihan_listrik_pascabayar.`user`
    (id_user, username, password, nama_admin, id_level)
VALUES
    ('USR001', 'USERNAME01', 'usr0123', 'Username 1', 'ADM'),    
    ('USR001', 'USERNAME01', 'usr0123', 'Username 1', 'OPR'),
    ('USR001', 'USERNAME01', 'usr0123', 'Username 1', 'BDH')


INSERT INTO tagihan_listrik_pascabayar.`level` (id_level, nama_level)
VALUES('ADM', 'Administrator'),('BDH', 'Bendahara'),('OPR', 'Operator');


INSERT INTO tagihan_listrik_pascabayar.pelanggan
    (id_pelanggan, username, password, nomor_kwh, nama_pelanggan, alamat, id_tarif)
VALUES
    ('CUST001', 'customer01', 'cust123', '16983823', 'Bona', 'Penjaringan Jakarta Barat', 'TRFVA900'),
    ('CUST002', 'customer02', 'cust123', '11660272', 'Rong-rong', 'Kampung Rambutan Jakarta Timur', 'TRFVA1300'),
    ('CUST003', 'customer03', 'cust123', '22855711', 'Nando', 'Kelapa Gading Jakarta Utara', 'TRFVA3500'),
    ('CUST004', 'customer04', 'cust123', '27601325', 'Riko', 'Kuningan Jakarta Selatan', 'TRFKVA200'),
    ('CUST007', 'customer07', 'cust123', '16968323', 'Gusti', 'Penjaringan Jakarta Barat', 'TRFVA900')


INSERT INTO tagihan_listrik_pascabayar.tarif
    (id_tarif, daya, tarifperkwh)
VALUES
    ('TRFKVA200', 200, 1114.74),
    ('TRFKVA30K', 3000, 996.74),
    ('TRFVA1300', 1300, 1444.7),
    ('TRFVA3500', 3500, 1444.7),
    ('TRFVA6600', 6600, 1444.7),
    ('TRFVA900', 900, 1352.0)


CREATE UNIQUE INDEX 
    index_no_kwh on pelanggan(nomor_kwh);


# VIEW

CREATE or REPLACE
VIEW `tagihan_listrik_pascabayar`.`v_penggunaan_listrik` AS
SELECT
    `pu`.`id_penggunaan` as `id_penggunaan`,
    `pl`.`id_pelanggan` as `id_pelanggan`,
    `pl`.`username` as `username`,
    `pl`.`nama_pelanggan` as `nama_pelanggan`,
    `pl`.`nomor_kwh` as `nomor_kwh`,
    `pl`.`alamat` as `alamat`,
    `tf`.`daya` as `daya`,
    `pu`.`bulan` as `bulan`,
    `pu`.`tahun` as `tahun`,
    `pu`.`meter_awal` as `meter_awal`,
    `pu`.`meter_akhir` as `meter_akhir`
FROM
    `tagihan_listrik_pascabayar`.`pelanggan` `pl`  JOIN 
    `tagihan_listrik_pascabayar`.`tarif` `tf` ON `pl`.`id_tarif` = `tf`.`id_tarif` JOIN 
    `tagihan_listrik_pascabayar`.`penggunaan` `pu` ON `pl`.`id_pelanggan` = `pu`.`id_pelanggan`;



CREATE or REPLACE
VIEW `tagihan_listrik_pascabayar`.`v_tagihan_listrik` as
SELECT
    `tg`.`id_tagihan` as `id_tagihan`,
    `plg`.`id_pelanggan` as `id_pelanggan`,
    `plg`.`nama_pelanggan` as `nama_pelanggan`,
    `plg`.`nomor_kwh` as `nomor_kwh`,
    `plg`.`alamat` as `alamat`,
    `trf`.`daya` as `daya`,
    `tg`.`bulan` as `bulan`,
    `tg`.`tahun` as `tahun`,
    `tg`.`jumlah_meter` as `jumlah_meter`,
    `trf`.`tarifperkwh` as `tarifperkwh`,
    `tg`.`status` as `status`
FROM
    `tagihan_listrik_pascabayar`.`pelanggan` `plg` JOIN 
    `tagihan_listrik_pascabayar`.`tagihan` `tg` ON `plg`.`id_pelanggan` = `tg`.`id_pelanggan` JOIN
    `tagihan_listrik_pascabayar`.`tarif` `trf` on `plg`.`id_tarif` = `trf`.`id_tarif`;


# JOIN

SELECT 
    p.nama_pelanggan,
    p.nomor_kwh,
    p.nomor_kwh,
    t.daya,
    pu.bulan,
    pu.tahun,
    pu.meter_awal,
    pu.meter_akhir 
FROM    
    pelanggan p join 
    tarif t on p.id_tarif = t.id_tarif join 
    penggunaan pu on p.id_pelanggan = pu.id_pelanggan 


SELECT 
    vp.nama_pelanggan,
    vp.nomor_kwh,
    vp.daya,
    vp.bulan,
    vp.tahun,
    vl.jumlah_meter,
    vl.status 
FROM 
    v_penggunaan_listrik vp JOIN 
    v_tagihan_listrik vl on concat(vp.nomor_kwh,vp.bulan,vp.tahun) = concat(vl.nomor_kwh,vl.bulan,vl.tahun)


# PROCEDURE

CREATE or REPLACE 
    PROCEDURE `tagihan_listrik_pascabayar`.`getDataPelanggan900W`()
BEGIN                        
    SELECT pl.nama_pelanggan,nomor_kwh,tr.daya,alamat 
    FROM 
        pelanggan pl JOIN 
        tarif tr ON pl.id_tarif = tr.id_tarif 
    WHERE tr.daya LIKE '%900%';
END

{ CALL tagihan_listrik_pascabayar.getDataPelanggan900W() }

# FUNCTION 

CREATE or REPLACE 
    FUNCTION `tagihan_listrik_pascabayar`.`fnHitungPenggunaanListrik`(meter_awal INT,meter_akhir INT) 
RETURNS int(11)
    RETURN meter_akhir - meter_awal;


SELECT 
    p.nama_pelanggan,
    p.nomor_kwh,
    p.nomor_kwh,
    t.daya,
    pu.bulan,
    pu.tahun,
    pu.meter_awal,
    pu.meter_akhir,
    fnHitungPenggunaanListrik( pu.meter_awal, pu.meter_akhir) as total
FROM    
    pelanggan p join 
    tarif t on p.id_tarif = t.id_tarif join 
    penggunaan pu on p.id_pelanggan = pu.id_pelanggan 



# TRIGER 

CALL triggerSimpanDataTagihan('CUST002','01','2023',2000,2500);


CREATE or replace PROCEDURE triggerSimpanDataTagihan(
        idPelanggan VARCHAR(10),
        bulan VARCHAR(2),
        tahun VARCHAR(4),
        meterAwal integer,
        meterAkhir integer
    )
BEGIN                          
    INSERT INTO 
        penggunaan(
            id_penggunaan,
            id_pelanggan,
            bulan,
            tahun,
            meter_awal,
            meter_akhir
        )
        VALUES(
            CONCAT(@bulan,RIGHT(@tahun,2),LEFT(UUID(), 6)),
            idPelanggan,
            bulan,
            tahun,
            meterAwal,
            meterAkhir
        );  
  END;


CREATE or REPLACE TRIGGER 
    `insertDataTagihan` AFTER INSERT ON `penggunaan` FOR EACH ROW 
BEGIN
    INSERT INTO tagihan(id_penggunaan,id_tagihan,id_pelanggan,bulan,tahun,jumlah_meter,STATUS)
    VALUES(
        -- CONCAT(new.id_penggunaan,RIGHT(new.tahun,2),LEFT(UUID(), 2)),
        CONCAT(new.id_penggunaan),
        NEW.id_penggunaan,
        NEW.id_pelanggan,
        NEW.bulan,
        new.tahun,
        fnHitungPenggunaanListrik(new.meter_awal,new.meter_akhir),'BELUM DIBAYAR'
    );
END;



START TRANSACTION;
INSERT INTO pelanggan(id_pelanggan, username, password, nomor_kwh, nama_pelanggan, alamat, id_tarif)
VALUES('CUST009', 'customer09', 'cust123', '96968323', 'Rahmat Hidayat', 'Bekasi Timur', 'TRFVA900');
COMMIT;



create or replace TRIGGER updateDataTagihan
AFTER update
ON penggunaan FOR EACH row
    UPDATE tagihan 
    set 
        id_penggunaan = NEW.id_penggunaan,
        bulan = new.bulan,
        tahun = new.tahun,
        id_pelanggan = new.id_pelanggan,
        jumlah_meter = fnHitungPenggunaanListrik(new.meter_awal,new.meter_akhir)
    where id_penggunaan = old.id_penggunaan

    
create or replace TRIGGER deleteDataTagihan
BEFORE DELETE
ON penggunaan FOR EACH row
    delete FROM tagihan 
    where 
        id_penggunaan = old.id_penggunaan 
