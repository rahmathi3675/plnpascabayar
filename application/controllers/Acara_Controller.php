<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
/*use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;*/
class Acara_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Acara_Model','acara',false);
		date_default_timezone_set('Asia/Jakarta');
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$this->api_url = "http://localhost/seradu_api/";
		}
		else{
			$this->api_url = "https://api.seradu.id/";	
		}
	}

	public function getAcara()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->acara->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            $edit = "
                    <form action='".base_url('acara/edit')."' method='post' style='display: inline-block'>
                        <input type='hidden' name='kode' value='".$field->kode_acara."'>
                        <button class='btn btn-sm btn-primary' type='submit'><i class='fas fa-edit'></i></button>
                    </form>";

            $delete = "<button type='button' class='btn btn-sm btn-danger btn-delete ml-1' data-id='".$field->kode_acara."'><i class='fas fa-trash'></i></button>
                    ";

            $view = "
                    <form action='".base_url('acara/view')."' method='post' style='display: inline-block'>
                        <input type='hidden' name='id' value='".$field->kode_acara."'>
                        <button class='btn btn-sm btn-info' type='submit'><i class='fas fa-eye'></i></button>
                    </form>";



            //  GROUPING BUTTON
            //  ---------------------
            $all_button = $view;

            if( $this->session->userdata('role') == 'adm'){
            	$all_button .= $edit;
            }

            $row[] = $all_button;
            $row[] = $field->kode_acara;
            $row[] = $field->kode_kegiatan;
            $row[] = $field->deskripsi_acara;
            $row[] = $field->tanggal_mulai;
            $row[] = $field->tanggal_selesai;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->acara->count_all(),
            "recordsFiltered" => $this->acara->count_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}

	public function uploadPhoto()
	{
        $curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'Api/Kegiatan/upload/'.str_replace('/', '', $this->input->post('kode_kegiatan')),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => [
				'file_upload'=> new CURLFILE( 
					$_FILES['file_upload']['tmp_name'], 
					$_FILES['file_upload']['type'], 
					$_FILES['file_upload']['name'] 
				)
			],
			CURLOPT_HTTPHEADER => [
				'key: development_key',
				'Authorization: Basic YWRtaW46MTIz',
				'Content-Type: multipart/form-data'
			]
		]);

        $response = curl_exec($curl);
		curl_close($curl);
		echo $response;
	}


	public function view( $page=NULL, $act=NULL , $id=NULL )
	{	

  		$this->load->view('template/header_public');
		switch ($page) {

			case 'create' :

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Kegiatan/comming',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);

		        $result_kegiatan = curl_exec($curl);
		        $kegiatan = json_decode($result_kegiatan);
				curl_close($curl);	

				$response = [
					"data_kegiatan" => $kegiatan->data
				];

				$this->load->view('acara/create_view', $response);
				break;

			case 'save':

				$form = $this->input->post();

				$data_form = [	
					'kode_kegiatan' => $this->input->post('kode_kegiatan'),
	    			'deskripsi_acara' 	=> $this->input->post('deskripsi'),
	    			'tanggal_mulai' 	=> date('Y-m-d H:i:s', strtotime( $this->input->post('tanggal_mulai').' '.$this->input->post('waktu_mulai'))),
	    			'tanggal_selesai' 	=> date('Y-m-d H:i:s', strtotime( $this->input->post('tanggal_selesai').' '.$this->input->post('waktu_selesai'))),
	    			'create_by' 		=> $this->session->userdata('username'),
	    			'create_date' 		=> date('Y-m-d H-i-s')
	    		];


				$curl = curl_init();
		        curl_setopt_array( $curl, [
					CURLOPT_URL => $this->api_url.'Api/Acara',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'POST',
					CURLOPT_POSTFIELDS => json_encode($data_form),
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization,
						"Content-Type:application/json", 
						"Accept:application/json"
					]
				]);

		        $result = curl_exec($curl);
		        $response = json_decode($result);
				curl_close($curl);
				$this->response['status'] = $response->response;
				$this->response['message'] = "Event";
				$this->response['line'][] = [ 'status' => false, 'message' => $response->message ];
				$this->session->set_flashdata('flash_messages', $this->response);
				redirect( base_url('acara'));
				break;

			case 'edit':


				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Acara/comming',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_kegiatan = curl_exec($curl);
		        $kegiatan = json_decode($result_kegiatan);
				curl_close($curl);	

				$curl3 = curl_init();
		        curl_setopt_array( 
		        	$curl3, [
					CURLOPT_URL => $this->api_url.'Api/Acara/'.str_replace('/', '', $this->input->post('kode')),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl3);
		        $data_header = json_decode($result_header);
				curl_close($curl3);	
				//echo str_replace('/', '', $this->input->post('kode'));
				//die();

				$response = [
					"data_kegiatan" => $kegiatan->data,
					"data_header"	=> $data_header->data
				];

				/*echo "<pre>";
				print_r($response);
				die();*/

				$this->load->view('acara/edit_view', $response);	
				break;
			
			case 'update':

				$form = $this->input->post();

				$data_form = [	
					'kode_kegiatan' 	=> $this->input->post('kode_kegiatan'),
	    			'deskripsi_acara' 	=> $this->input->post('deskripsi'),
	    			'tanggal_mulai' 	=> date('Y-m-d H:i:s', strtotime( $this->input->post('tanggal_mulai').' '.$this->input->post('waktu_mulai'))),
	    			'tanggal_selesai' 	=> date('Y-m-d H:i:s', strtotime( $this->input->post('tanggal_selesai').' '.$this->input->post('waktu_selesai'))),
	    			'update_by' 		=> $this->session->userdata('username'),
	    			'update_date' 		=> date('Y-m-d H-i-s')
	    		];

				$curl = curl_init();
		        curl_setopt_array( $curl, [
					CURLOPT_URL => $this->api_url.'Api/Acara/'.str_replace('/','',$this->input->post('kode_acara')),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'PUT',
					CURLOPT_POSTFIELDS => json_encode($data_form),
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization,
						"Content-Type:application/json", 
						"Accept:application/json"
					]
				]);

		        $result = curl_exec($curl);
		        $response = json_decode($result);
				curl_close($curl);
				$this->response['status'] = $response->response;
				$this->response['message'] = "Activity";
				$this->response['line'][] = [ 'status' => false, 'message' => $response->message ];
				$this->session->set_flashdata('flash_messages', $this->response);
				redirect( base_url('acara'));
				break;

			default:	
				$this->load->view( 'template/global_notification');
				$this->load->view( 'acara/list_view');
				break;
		}
		$this->load->view('template/footer');
	}
}
