<?php

include_once('Auth.php');

class Access_Menu extends Auth {
	

	public function __construct()
	{	

		parent::__construct();
		$this->load->model('Access_Menu_Model','access',false);
		$this->load->model('Role_Model','role',false);
		date_default_timezone_set('Asia/Jakarta');
		
		// RESTRICT CONTROLLER
		$this->authKey();
	}

	public function getAllAccess()
	{	
		


		$list = $this->access->get_datatables();
	    $data = array();
	    $no = $_POST['start'];
	    $i = 1;
	    foreach ($list as $field) {
	    	
	    	$edit = "
	        		
        				<input type='hidden' name='id_role' value='".$field->id."'>
        				<select class='form-control'>
        					<option value='0'>Disabled</option>
        				</select>
        			";

        	
	    	$no++;
	        $row = array();
	        $row[] = $i;
	        $row[] = "
	        		<div>
	        			<form method='post' name='abc' action='".base_url('master_menu_access/detail_role_menu')."'>
	        				<input type='hidden' name='role_id' value='".$field->id."'> 
	        				<button type='submit' class='btn btn-sm btn-success btn-block'><i class='fas fa-key fa-xs'></i> ".ucfirst($field->role_code)."</button>
	        			</form>
	        		</div>";
	        $row[] = "<div>".$field->role_name."</div>";
	        $row[] = "<span class='badge badge-pill badge-primary'>".$field->menu_active." Active</span> <span class='badge badge-pill badge-secondary'>".$field->menu_innactive." Innactive</span>";

	        $data[] = $row;
	        $i++;
	    }
	 	
	    $response = array(
	    	"draw" => $_POST['draw'],
	        "recordsTotal" => $this->access->count_all(),
	        "recordsFiltered" => $this->access->count_filtered(),
	        "data" => $data,
	    );
		echo json_encode($response);
	}

	/*public function insertNewRole()
	{
		$this->load->library('form_validation');
		$config = array(
	        array(
                'field' => 'role_name',
                'label' => 'Role Name',
                'rules' => 'required',
                'errors' => array(
                	'required' => 'Role Name Required',
            	),
	        ),
	        array(
                'field' => 'role_code',
                'label' => 'Role Code',
                'rules' => 'required',
                'errors' => array(
                	'required' => 'Role Code Required',
            	),
	        ),
		);

		$this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
        	$this->loadHeader();
    		$this->load->view('role/role_add');
        }
        else
        {	
        	$data = [
        		'role_name' 		=> $this->input->post('role_name'),
        		'role_code'	 	=> $this->input->post('role_code'),
        		'created_by'		=> $this->session->userdata('id'),
        		'created_date'		=> date('Y-m-d H:i:s')
        	];

        	$ress = $this->role->insertRole( $data );
        	if($ress > 0){
	            $response = [ 'status' => true, 'message' => 'Success Insert Role' ];
	        }
	        else{
	            $response = [ 'status' => false, 'message' => 'Insert Role Failed'];
	        }
	        
	        $this->session->set_flashdata('response', $response);
	        redirect('master_role');
        }
	}

	public function updateRole()
	{
		$this->load->library('form_validation');
		$config = array(
	        array(
                'field' => 'role_name',
                'label' => 'Role Name',
                'rules' => 'required',
                'errors' => array(
                	'required' => 'Role Name Required',
            	),
	        ),
	        array(
                'field' => 'role_code',
                'label' => 'Role Code',
                'rules' => 'required',
                'errors' => array(
                	'required' => 'Role Code Required',
            	),
	        ),
		);

		$this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE)
        {
        	$this->loadHeader();
        	$response = [ 
				'data_role' => $this->role->getDetailRole( ['id' => $this->input->post('id_role') ] )->row()
			];
    		$this->load->view('role/role_edit',$response);
        }
        else
        {	
        	$data = [
        		'role_code' 		=> $this->input->post('role_code'),
        		'role_name'	 	=> $this->input->post('role_name'),
        		'updated_by'		=> $this->session->userdata('id'),
        		'updated_date'		=> date('Y-m-d H:i:s')
        	];

        	$condition = [
        		'id' 				=> $this->input->post('id_role'),
        	];

        	$ress = $this->role->updateRole( $data , $condition );
        	if($ress > 0){
	            $response = [ 'status' => true, 'message' => 'Success Update Role' ];
	        }
	        else{
	            $response = [ 'status' => false, 'message' => 'Update Role Failed'];
	        }
	        
	        $this->session->set_flashdata('response', $response);
	        redirect('master_role');
        }
	}*/

	public function activateMenuRole()
	{
		$check_menu = $this->access->getCountData([ 
			'menu_id' => $this->input->post('menu_id'),
			'role_id' => $this->input->post('role_id')
		]); 
		if( $check_menu->result_id->num_rows  == 0 ){
			$next_status = 1;
			$status = 'insert';
		}
		else{
			$data_menu = $check_menu->row();
			if( $data_menu->is_active == 1){
				$next_status = 0;
				$status = 'update';
			}
			else{
				$next_status = 1;
				$status = 'update';
			}
		}

		$result = $this->nextActivateMenuRole($next_status, $status, $this->input->post('menu_id'), $this->input->post('role_id'));
		if( $result ==  true){
			$response = ['status' => true, 'message' => 'Sukses Update Menu Role', 'next_status'=>$next_status ];
		}
		else{
			$response = ['status' => true, 'message' => 'Sukses Update Menu Role', 'next_status'=>$next_status];	
		}
		echo json_encode($response);

	}

	private function nextActivateMenuRole( $next_status, $action, $menu_id, $role_id )
	{
		if($action == 'update'){
			$result = $this->access->updateAccess( $menu_id, $role_id , $next_status);
			if($result > 0){
				return true;
			}
			else{
				return false;
			}
		}
		elseif($action == 'insert'){
			$result = $this->access->insertAccess( $menu_id, $role_id , $next_status);
			if($result > 0){
				return true;
			}
			else{
				return false;
			}	
		}
	}
	
	public function loadHeader()
	{
		switch ($this->session->userdata('logged_type')) 
		{
			case 'internal':
				$this->load->view('template/header_internal');
				break;
			
			default:
				$this->load->view('template/header_vendor');
				break;
		}
	}



	public function view( $page=NULL, $act=NULL , $id=NULL )
	{	
		
		$this->loadHeader();


		switch ($page) {

			case 'detail_role_menu': 
				
				$response = [
					'detail_role'	=> $this->role->getDetailRole( ['id' => $this->input->post('role_id')] )->row(),
					'menu_role'		=> $this->access->getMenuRole( $this->input->post('role_id'))->result(),
					'role_id'		=> $this->input->post('role_id'),
				];
				$this->load->view('access_menu/detail_access_menu', $response);
				break;	

			default:

				$this->load->view('access_menu/list_access_menu');
				break;
		}
		
		$this->load->view('template/footer');
	}


}
