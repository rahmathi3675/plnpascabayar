<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public $data_dir;
	public $global_session = false;
	public $api_key = "key: development_key";
	public $authorization = "Authorization: Basic YWRtaW46MTIz";

	public function __construct()
	{	
		parent::__construct();
		$this->load->model('Login_Model','auth',false);
		date_default_timezone_set('Asia/Jakarta');	

		if($_SERVER['HTTP_HOST'] == "localhost"){ 
			$this->link_api = "http://localhost/seradu_api/";
		}
		else{
			$this->link_api = "http://localhost/seradu_api/";	
		}
	}


	public function googleOauth($value='')
	{
		
		include_once APPPATH . "../vendor/autoload.php";
		$google_client = new Google_Client();
		$google_client->setClientId('179961485284-5fmvcshn0jbq9m5d7gnii0vg722ma3cs.apps.googleusercontent.com'); 
		$google_client->setClientSecret('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'); 
		$google_client->addScope('email');
		$google_client->addScope('profile');
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$google_client->setRedirectUri('http://localhost/elearning/landing_page/auth_google');
		}
		else{
			$google_client->setRedirectUri('https://elearning.seradu.id/landing_page/auth_google');	
		}
		if(isset($_GET["code"]))
	  	{
		   	$token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

		   	if(!isset($token["error"]))
		   	{
			    $google_client->setAccessToken($token['access_token']);

			    $token_code = $token['access_token'];
			    $token_expired = date('Y-m-d H:i:s', strtotime( date('Y-m-d H:i:s') ) + (  ( ( 60 * 60 ) * 24 ) ) );
			    $this->session->set_userdata('access_token', $token_code );
			    $this->session->set_userdata('token_expired_date', $token_expired );

			    $google_service = new Google_Service_Oauth2($google_client);
			    $data = $google_service->userinfo->get();


			    $form_tokens = [
			    	'token'		=> $token_code,
			    	'token_expired_date'	=> $token_expired 
			    ];

			    // GET DATA BY EMAIL 
			    $anggota = $this->getAnggotaByEmail( $data, $form_tokens );
		   	}
		   	else{
		   		die();
		   	}									
	  	}
	  	else{
	  		redirect( base_url('landing_page') );
	  	}
	}

	public function getAnggotaByEmail( $data, $tokens =  null )
	{	
		/*$this->session->unset_userdata([
			'access_token','logged_in','id','nama_lengkap','email_google','google_photo_link','logged_in','nik','tempat_lahir','tanggal_lahir','alamat_rumah','rt','rw','kode_pos','provinsi','kabupaten_kota','kecamatan','kelurahan_desa','no_hp','pendidikan_terakhir','jabatan','is_verified','verified_by','username','password','photo_profil','token','token_expired_date'
		]);
		$this->session->unset_userdata('access_token');
		$this->session->sess_destroy();*/

		$result = $this->auth->getAnggotaByEmail( $data['email'] );
		if( $result->result_id->num_rows > 0 ){

			$data = $result->row();

			$update = $this->auth->upateToken( $tokens, ['id' => $data->id ] );
			if( $update == true ){
				// EXIST


				$user_data = [];
				foreach ($result->row() as $index => $column) {
					if( $index == 'token') {
					}
					elseif( $index == 'token_expired_date'){}
					else{
						$user_data += [ $index => $column ]; 
					}
				}

				foreach ($tokens as $indexB => $columnB) {
					// code...
					$user_data += [ $indexB =>  $columnB ]; 
				}


				$user_data += [ 'logged_in'	=> true];
				
				$this->session->set_userdata($user_data);
				print_r( $this->session->all_userdata());
				redirect( base_url('landing_page') );
			}
			else{
				redirect( base_url('landing_page') );
				die();
			}
		}
		else{
			$username = rand(11111111,99999999);

			$form = [
		      	'nama_lengkap' 	=> $data['name'],
		      	'email_google' 	=> $data['email'],
		      	'google_photo_link'	=> $data['picture'],
		      	'username'		=> $username

		    ];

		    $form += $tokens;

		    $result = $this->auth->insertNewAnggota( $form );
		    if( $result == true ){
		    	$user_data = [
		    		'id'			=> '',
			      	'nama_lengkap' 	=> $data['name'],
			      	'email_google' 	=> $data['email'],
			      	'google_photo_link'	=> $data['picture'],
			      	'logged_in'		=> true,
			      	'nik'			=> '',
			      	'tempat_lahir'	=> '',
			      	'tanggal_lahir'	=> '',
			      	'alamat_rumah'	=> '',
			      	'rt'			=> '',
			      	'rw'			=> '',
			      	'kode_pos'		=> '',
			      	'provinsi'		=> '',
			      	'kabupaten_kota'	=> '',
			      	'kecamatan'			=> '',
			      	'kelurahan_desa'	=> '',
			      	'no_hp'				=> '',
			      	'pendidikan_terakhir'	=> '',
			      	'jabatan'		=> '',
			      	'is_verified'	=> '',
			      	'verified_by'	=> '',
			      	'username'		=> '',
			      	'password'		=> '',
			      	'photo_profil'	=> '',
			      	'username'		=> $username
			    ];

			    $user_data += $tokens;

			    $this->session->set_userdata($user_data);
		    }
		    redirect( base_url('landing_page') );
		}
	}

	public function logOut() {

		$this->session->unset_userdata([
			'id_user','id_pelanggan','username','password','nama_admin','nama_pelanggan','id_tarif','alamat','id_level'
		]);
		$this->session->sess_destroy();
		redirect( base_url() );
	}
	

	public function generateID($id, $len)
	{
		return substr($id, $len, strlen($id) - ( $len * 2 ) );
	}

	public function authKey() {


		if( $this->session->userdata('logged_in') )
		{	




			/*echo "<pre>";
			print_r($this->session->userdata('logged_in'));
			//echo $sess['reimbursement_taco']['logged_in'];
			die();*/
			/*echo "ada";

			if($this->session->userdata('logged_type') == 'internal') 
			{
				
				$username = $this->session->userdata('id');
				if(isset($username))
				{	
					$global_session = $this->session->userdata('reimbursement_taco');
				}
				else{

				}
			}
			elseif($this->session->userdata('reimbursement_taco')['logged_type'] == 'external_taco'){
				
				$username = $this->session->userdata('id');
				if(isset($username))
				{	
					$global_session = $this->session->userdata('reimbursement_taco');
				}
				else{

				}
			} 
			else{
				redirect('login');		
			}*/
		}
		else{
			redirect('landing_page');
		}
	}

	public function loadHeader()
	{
		switch ($this->session->userdata('logged_type')) 
		{
			case 'internal':
				$this->load->view('template/header');
				break;
			
			default:
				$this->load->view('template/header_vendor');
				break;
		}
	}

	public function getPublicApi( $link, $params = false )
	{
		$curl = curl_init();
		/*$params = [
			'is_verified' => 1,
			'verified_by'	=> $this->session->userdata('id')
		];*/

		$key = "key:development_key";
		curl_setopt_array(
			$curl, 
			array(
				CURLOPT_URL => $link,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_USERPWD => 'admin' . ":" . '123',
				CURLOPT_POSTFIELDS => json_encode($params),
				CURLOPT_HTTPHEADER => array(
					"key:development_key", 
					"Content-Type:application/json", 
					"Accept:application/json"
				)
			)
		);

		$data = curl_exec($curl);

		$response = json_decode($data);
		return $response->data;
	}

	public function sendGetApi( $link )
	{
		$curl = curl_init();
		curl_setopt_array(
			$curl, 
			array(
				CURLOPT_URL => $link,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_USERPWD => 'admin' . ":" . '123',
				CURLOPT_HTTPHEADER => array(
					$this->api_key,
					//"Content-Type:application/json", "Accept:application/json"
				)
			)
		);

		$data = curl_exec($curl);
		$response = json_decode($data);
		return $response->data;
	}

	public function getPublicApiForSearchableSelect2( $link, $params )
	{
		$curl = curl_init();
		$data = http_build_query( $params );
		$getUrl = $link."?".$data;

		curl_setopt_array(
			$curl, 
			array(
				CURLOPT_URL => $getUrl,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_USERPWD => 'admin' . ":" . '123',
				CURLOPT_HTTPHEADER => array(
					"key:development_key", 
					//"Content-Type:application/json", "Accept:application/json"
				)
			)
		);

		$data = curl_exec($curl);
		$source = json_decode($data);
		
		if( !empty($source->data) ){
			foreach ($source->data  as $index => $column) {
				
				$sub_data = [];
				foreach ($column as $x => $y) {
					$sub_data += [ $x => $y ];
				}
				$obj[] = $sub_data;
				//$obj[] = ['postal_code' => $column->postal_code, 'kelurahan' => $column->kelurahan, 'kecamatan'=> $column->kecamatan, 'province' => $column->province, 'city'=> $column->city ];
			}
			$response['data'] = (object) $obj; 
		}
		else{
			$response['data'] = false;
		}
		return $response;
	}
}
