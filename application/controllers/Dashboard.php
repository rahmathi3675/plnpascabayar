<?php

//include_once('Auth.php');

class Dashboard extends CI_Controller {
	

	public function __construct()
	{	

		parent::__construct();
		$this->load->model('Menu_Model','menu',false);
		date_default_timezone_set('Asia/Jakarta');
		//include_once APPPATH . "../vendor/autoload.php";
		  
	
	}

	public function index()
	{
		//	GET MENU 
		//	==============
		$this->load->view('template/public_header');
		$this->load->view('home_page');
		$this->load->view('template/footer');
	}

	public function GoogleApi()
	{
		$google_client = new Google_Client();
	  	$google_client->setClientId('179961485284-5fmvcshn0jbq9m5d7gnii0vg722ma3cs.apps.googleusercontent.com'); //masukkan ClientID anda 
	  	$google_client->setClientSecret('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'); //masukkan Client Secret Key anda
	  	$google_client->setRedirectUri('http://localhost/dilus'); //Masukkan Redirect Uri anda
	  	$google_client->addScope('email');
	  	$google_client->addScope('profile');
		if(isset($_GET["code"]))
	  	{	
		   	$token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
		   	if(!isset($token["error"]))
		   	{
			    $google_client->setAccessToken($token['access_token']);
			    $this->session->set_userdata('access_token', $token['access_token']);
			    $google_service = new Google_Service_Oauth2($google_client);
			    $data = $google_service->userinfo->get();

			    $current_datetime = date('Y-m-d H:i:s');
			    $user_data = array(
			      	'first_name' => $data['given_name'],
			      	'last_name'  => $data['family_name'],
			      	'email_address' => $data['email'],
			      	'profile_picture'=> $data['picture'],
			      	'updated_at' => $current_datetime
			     );
			    $this->session->set_userdata('user_data', $data);
		   	}									
	  	}
	}
}
