<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
class Elearning_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Acara_Model','acara',false);
		date_default_timezone_set('Asia/Jakarta');
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$this->api_url = "http://localhost/seradu_api/";
		}
		else{
			$this->api_url = "https://api.seradu.id/";	
		}
	}

	public function getElearning()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->acara->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            $edit = "
                    <form action='".base_url('elearning/detail')."' method='post' style='display: inline-block'>
                        <input type='hidden' name='kode_acara' value='".$field->kode_acara."'>
                        <button class='btn btn-sm btn-primary' type='submit'><i class='fas fa-book-open'></i></button>
                    </form>";


            //  GROUPING BUTTON
            //  ---------------------
            $all_button = $edit;

            /*if( $this->session->userdata('role') == 'adm'){
            	$all_button .= $edit;
            }*/

            $row[] = $all_button;
            $row[] = $field->kode_acara;
            $row[] = $field->kode_kegiatan;
            $row[] = $field->deskripsi_acara;
            $row[] = $field->tanggal_mulai;
            $row[] = $field->tanggal_selesai;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->acara->count_all(),
            "recordsFiltered" => $this->acara->count_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}

	public function uploadPhoto()
	{

        $curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'Api/Kegiatan/upload/'.str_replace('/', '', $this->input->post('kode_kegiatan')),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => [
				'file_upload'=> new CURLFILE( 
					$_FILES['file_upload']['tmp_name'], 
					$_FILES['file_upload']['type'], 
					$_FILES['file_upload']['name'] 
				)
			],
			CURLOPT_HTTPHEADER => [
				'key: development_key',
				'Authorization: Basic YWRtaW46MTIz',
				'Content-Type: multipart/form-data'
			]
		]);

        $response = curl_exec($curl);
		curl_close($curl);
		echo $response;
	}


	public function view( $page=NULL, $act=NULL , $id=NULL )
	{	

  		$this->load->view('template/header_public');
		switch ($page) {

			case 'detail':

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Acara/detail/'.str_replace('/', '', $this->input->post('kode_acara')),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl);
		        $data_header = json_decode($result_header);
				curl_close($curl);	
			
				$response = [
					"data_header"	=> $data_header->data
				];

				$this->load->view('elearning/detail_view', $response);	
				break;
			
			case 'daftar_acara':

				$form = $this->input->post();


				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Acara/'.str_replace('/', '', $form['kode_acara']),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl);
		        $data_header = json_decode($result_header);
				curl_close($curl);	

				if( $data_header->response == true ){
					
					$data_form = [	
						'kode_acara' 			=> $this->input->post('kode_acara'),
		    			'username' 				=> $this->session->userdata('username'),
		    			'nik' 				=> $this->session->userdata('nik'),
		    		];

		    		

	    			$curl = curl_init();
			        curl_setopt_array( $curl, [
						CURLOPT_URL => $this->api_url.'Api/Acara/register',
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => '',
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 0,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => 'POST',
						CURLOPT_POSTFIELDS => json_encode($data_form),
						CURLOPT_HTTPHEADER => [
							$this->api_key,
							$this->authorization,
							"Content-Type:application/json", 
							"Accept:application/json"
						]
					]);

			        $result = curl_exec($curl);
			        $response = json_decode($result);
					curl_close($curl);

					if( $response->response  == true){

						redirect ( base_url('quiz/confirmation/'. str_replace('/', '', $response->no_reg) )  );

						/*$this->response['status'] = true;
						$this->response['message'] = $response->message;
						$this->response['line'][] = [ 'status' => true, 'message' => $response->message ];*/

					}
					else{
						$this->response['status'] = false;
						$this->response['message'] = $response->message;
					}
				}
				else{
					$this->response['status'] = false;
					$this->response['message'] =$response->message;
				}
				$this->session->set_flashdata('flash_messages', $this->response);
				redirect( base_url('elearning'));
				break;

			default:	
				$this->load->view( 'template/global_notification');
				$this->load->view( 'elearning/list_view');
				break;
		}
		$this->load->view('template/footer');
	}
}
