<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
/*use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;*/
class Kategori_Kegiatan_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Kategori_Kegiatan_Model','kategori',false);
		date_default_timezone_set('Asia/Jakarta');
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$this->api_url = "http://localhost/seradu_api/";
		}
		else{
			$this->api_url = "https://api.seradu.id/";	
		}
	}

	public function getDataKategori()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->kategori->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            $edit = "
                    <form action='".base_url('kategori_kegiatan/edit')."' method='post' style='display: inline-block'>
                        <input type='hidden' name='kode' value='".$field->kode_kategori."'>
                        <button class='btn btn-sm btn-primary' type='submit'><i class='fas fa-edit'></i></button>
                    </form>";

            $delete = "<button type='button' class='btn btn-sm btn-danger btn-delete ml-1' data-id='".$field->id."'><i class='fas fa-trash'></i></button>
                    ";

            $view = "
                    <form action='".base_url('kategori_kegiatan/view')."' method='post' style='display: inline-block'>
                        <input type='hidden' name='id' value='".$field->id."'>
                        <button class='btn btn-sm btn-info' type='submit'><i class='fas fa-eye'></i></button>
                    </form>";



            //  GROUPING BUTTON
            //  ---------------------
            $all_button = ""; //$view;

            if( $this->session->userdata('role') == 'adm'){
            	$all_button .= $edit;
            }

            $row[] = $all_button;
            $row[] = strtoupper($field->kode_kategori);
            $row[] = strtoupper($field->nama_kategori);
            $row[] = $field->create_by;
            $row[] = $field->create_date;
            $row[] = $field->update_by;
            $row[] = $field->update_date;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->kategori->count_all(),
            "recordsFiltered" => $this->kategori->count_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}


	public function view( $page=NULL, $act=NULL , $id=NULL )
	{	

  		$this->load->view('template/header_public');
		switch ($page) {

			case 'create' :
				$this->load->view('kategori/create_view');
				break;

			case 'save':

				$form = $this->input->post();

				$data_form = [];	
				foreach ($form as $index => $field) {
					$data_form += [ $index => $field ];
				}

				$data_form += [ 'create_by' => $this->session->userdata('username') ];
				$data_form += [ 'create_date' => date('Y-m-d H-i-s') ];

				$curl = curl_init();
		        curl_setopt_array( $curl, [
					CURLOPT_URL => $this->api_url.'Api/Kategori',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'POST',
					CURLOPT_POSTFIELDS => json_encode($data_form),
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization,
						"Content-Type:application/json", 
						"Accept:application/json"
					]
				]);

		        $result = curl_exec($curl);
		        $response = json_decode($result);
				curl_close($curl);
				$this->response['status'] = $response->response;
				$this->response['message'] = "Create Category";
				$this->response['line'][] = [ 'status' => false, 'message' => $response->data ];
				$this->session->set_flashdata('flash_messages', $this->response);
				redirect( base_url('kategori_kegiatan'));
				break;

			case 'edit' :

				$kode = $this->input->post('kode');

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Kategori/'.$kode,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);

		        $result = curl_exec($curl);
		        $response = json_decode($result);
				curl_close($curl);	

				$response = [
					'data'	=> $response->data
				];

				$this->load->view('kategori/edit_view', $response);
				break;
			
			case 'update':

				$form = $this->input->post();

				$data_form = [];	
				foreach ($form as $index => $field) {
					$data_form += [ $index => $field ];
				}

				$data_form += [ 'update_by' => $this->session->userdata('id') ];
				$data_form += [ 'update_date' => date('Y-m-d H-i-s') ];

				$curl = curl_init();
		        curl_setopt_array( $curl, [
					CURLOPT_URL => $this->api_url.'Api/Kategori',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'PUT',
					CURLOPT_POSTFIELDS => json_encode($data_form),
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization,
						"Content-Type:application/json", 
						"Accept:application/json"
					]
				]);

		        $result = curl_exec($curl);
		        $response = json_decode($result);
				curl_close($curl);
				$this->response['status'] = $response->response;
				$this->response['message'] = "Update Category";
				$this->response['line'][] = [ 'status' => false, 'message' => $response->data ];
				$this->session->set_flashdata('flash_messages', $this->response);
				redirect( base_url('kategori_kegiatan'));
				break;

			default:	
				$this->load->view( 'template/global_notification');
				$this->load->view( 'kategori/list_view');
				break;
		}
		$this->load->view('template/footer');
	}
}
