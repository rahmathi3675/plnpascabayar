<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
class Landing_Page_Controller extends Auth {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Profil_Model','profil',false);
		date_default_timezone_set('Asia/Jakarta');
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$this->api_url = "http://localhost/seradu_api/";
		}
		else{
			$this->api_url = "https://api.seradu.id/";	
		}

	}

	public function prosesRegistrasi()
	{
		$form 	= (object) $this->input->post();
		$email 	= $form->email; 
		$username = $form->username;
		$password = $form->password;

		$user_profil = $this->checkExistingUsername( $form->username );
		if( $user_profil->response == true ){
			
			$this->response['status'] = false;
			$this->response['line'][] = ['status' => false, 'message' => 'Username Telah Dipakai'];
			echo json_encode($this->response);
		}
		else{
			$form_data = [ 
				'username' => $username,
				'password' => sha1($password), 
				'email_google' 	=> $email
			];

			$curl = curl_init();
	        curl_setopt_array( $curl, [
				CURLOPT_URL => $this->api_url.'Api/Profil/regist/',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS => json_encode( $form_data ),
				CURLOPT_HTTPHEADER => [
					'key: development_key',
					'Authorization: Basic YWRtaW46MTIz',
					"Content-Type: application/json"
				]
			]);

	        $response = curl_exec($curl);
			curl_close($curl);
			echo $response;
		}

	}

	public function prosesLogin()
	{
		$check = $this->profil->getUser($this->input->post('username'));
		if( $check->result_id->num_rows > 0 ){

			$data = $check->row();	
			if ( $this->input->post('password') == $data->password ){

				$user_data = [];
				foreach ($data as $index => $column) {
					$user_data += [ $index => $column ]; 
				}

				$user_data += [ 'role'	=> 'admin'];
				$user_data += [ 'logged_in'	=> true];
				$this->session->set_userdata($user_data);			
				$this->response['status'] = true;
				$this->response['line'][] = ['status' => true, 'message' => 'Login Sukses'];
			}
			else{
				$this->response['status'] = false;
				$this->response['line'][] = ['status' => false, 'message' => 'Invalid Password'];
			}
		}
		else{

			$check_pelanggan = $this->profil->getPelanggan($this->input->post('username'));
			if( $check_pelanggan->result_id->num_rows > 0 ){
				
				$data_pelanggan = $check_pelanggan->row();	
				if ( $this->input->post('password') == $data_pelanggan->password ){
					
					$user_data = [];
					foreach ($data_pelanggan as $index => $column) {
						$user_data += [ $index => $column ]; 
					}

					$user_data += [ 'role'	=> 'pelanggan'];
					$user_data += [ 'logged_in'	=> true];
					$this->session->set_userdata($user_data);			
					$this->response['status'] = true;
					$this->response['line'][] = ['status' => true, 'message' => 'Login Sukses'];
				}
			}
			else{
				$this->response['status'] = false;
				$this->response['line'][] = ['status' => false, 'message' => 'Invalid Password'];
			}
		}
		echo json_encode($this->response);
	}

	public function checkExistingUsername($username)
	{
		$curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'/Api/Profil/search/'.$username,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_USERPWD => 'admin' . ":" . '123',
			CURLOPT_HTTPHEADER => [
				'key: development_key'
			]
		]);

        $response = curl_exec($curl);
		curl_close($curl);

		return json_decode($response);
	}

	public function reset_password()
	{
		$email = $this->input->post('email');
		if($this->input->post('password') != '' && $this->input->post('re_password') != ''){
			
			if( $this->input->post('password') === $this->input->post('re_password') ) {
					
				$new_password = sha1($this->input->post('password'));
				$result = $this->auth->updatePassword(
					[
						'password' => $new_password
					], 
					$email
				);
				if($result > 0){
					$response['auth'] = [
						'success' 	=> '1',
						'message' 	=> 'Password has been updated',
					];
					$this->load->view('login_view',$response);
				}
				else{
					$response['auth'] = [
						'error' 	=> '1',
						'message' 	=> 'The password hasnt changed!',
					];
					$this->load->view('reset_password_view',$response);	
				}
			}
			else{
				$response['auth'] = [
					'error' 	=> '1',
					'message' 	=> 'Username and Password Not Match',
					'Related_information_email'		=> $email
				];
				$this->load->view('reset_password_view',$response);
			}
		}
	}

	public function forgot_password()
	{
		$response['auth'] = [
			'error' 	=> '0',
			'message' 	=> 'Please Fill The User EMail'
		];
		$this->load->view('forgot_password',$response);
	}

	public function apply_forgot_password()
	{
		$result = $this->auth->getVendor($this->input->post('email'));
		if($result->num_rows()>0){
			
			$result_update = $this->auth->forgotPassword($this->input->post('email'));
			if($result_update === true){			

				$response['auth'] = [
					'success' 	=> '1',
					'message' 	=> 'Password has been Reset',
				];
				$this->load->view('forgot_password',$response);
			}
			else{
				$response['auth'] = [
					'error' 	=> '1',
					'message' 	=> 'Reset Password Failed!',
				];
				$this->load->view('forgot_password',$response);	
			}

		}
		else{
			$response['auth'] = [
				'error' 	=> '1',
				'message' 	=> 'The password hasnt reset!',
			];
			$this->load->view('forgot_password',$response);	
		}
	}

	function logout() {

		$this->session->unset_userdata([
			'id_user','username','password','nama_admin','id_level'
		]);
		$this->session->sess_destroy();
		$response['auth'] = [
			'error' 	=> '1',
			'message' 	=> 'Username or Password Required',

		];
		$this->load->view('login_view',$response);
	}

	public function view( $page=NULL, $act=NULL , $id=NULL )
	{
		switch ($page) {

			case 'login':

		  		$this->load->view('template/header_public');
				$this->load->view('visitor/login');
				$this->load->view('template/footer');
				break;

			case 'daftar':

				include_once APPPATH . "../vendor/autoload.php";
				$google_client = new Google_Client();
				$google_client->setClientId('179961485284-5fmvcshn0jbq9m5d7gnii0vg722ma3cs.apps.googleusercontent.com'); 
				$google_client->setClientSecret('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'); 
				$google_client->addScope('email');
				$google_client->addScope('profile');
				
				if($_SERVER['HTTP_HOST'] == "localhost"){
					$google_client->setRedirectUri('http://localhost/elearning/landing_page/auth_google'); //Masukkan Redirect Uri anda
				}
				else{
					$google_client->setRedirectUri('https://elearning.seradu.id/landing_page/auth_google');	
				}

				if(isset($_GET["code"]))
			  	{
				   	$token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

				   	if(!isset($token["error"]))
				   	{
					    $google_client->setAccessToken($token['access_token']);
					    $this->session->set_userdata('access_token', $token['access_token']);
					    $google_service = new Google_Service_Oauth2($google_client);
					    $data = $google_service->userinfo->get();
				   	}									
			  	}

				$response = [
					'google_auth_url' 	=> $google_client->createAuthUrl()
				];
		  		$this->load->view('template/header_public', $response);
				$this->load->view('visitor/register');
				$this->load->view('template/footer');
				break;

			case 'auth_google':

				$this->googleOauth();
				break;
				
			default:
		  	
				

			  	if(!$this->session->userdata('logged_in'))
			  	{
				  	
				  	$this->session->unset_userdata(['access_token']);
					$this->session->sess_destroy();
					$this->load->view('template/header_public');
					$this->load->view('landing_page');
					$this->load->view('template/footer');
			  	}
			  	else
			  	{

			  		$response = [
						'data_pelanggan' 	=> $this->profil->getDataPelanggan()->result()
					];
			  		$this->load->view('template/header_public');

					$this->load->view('landing_page', $response);
					$this->load->view('template/footer');
			  	}
				break;
		}
	}

}
