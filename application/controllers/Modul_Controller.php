<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
/*use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;*/
class Modul_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Modul_Model','video',false);
		date_default_timezone_set('Asia/Jakarta');
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$this->api_url = "http://localhost/seradu_api/";
		}
		else{
			$this->api_url = "https://api.seradu.id/";	
		}
	}

	public function getDataModul()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->video->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            $edit = "
                    <form action='".base_url('modul/edit')."' method='post' style='display: inline-block'>
                        <input type='hidden' name='kode' value='".$field->kode_kegiatan."'>
                        <button class='btn btn-sm btn-primary' type='submit'><i class='fas fa-edit'></i></button>
                    </form>";

            $delete = "<button type='button' class='btn btn-sm btn-danger btn-delete ml-1' data-id='".$field->kode_kegiatan."'><i class='fas fa-trash'></i></button>
                    ";

            $view = "
                    <form action='".base_url('modul/view')."' method='post' style='display: inline-block'>
                        <input type='hidden' name='id' value='".$field->kode_kegiatan."'>
                        <button class='btn btn-sm btn-info' type='submit'><i class='fas fa-eye'></i></button>
                    </form>";



            //  GROUPING BUTTON
            //  ---------------------
            $all_button = $view;

            if( $this->session->userdata('role') == 'adm' || $this->session->userdata('role') == 'nsr'){
            	$all_button .= $edit;
            }

            $row[] = $all_button;
            $row[] = strtoupper($field->kode_kegiatan);
            $row[] = strtoupper($field->nama_kegiatan);
            $row[] = strtoupper(substr( $field->deskripsi, 0,100));
            $row[] = $field->tanggal_terbit;
            $row[] = $field->nama_kategori;
            $row[] = $field->nama_jenis;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->video->count_all(),
            "recordsFiltered" => $this->video->count_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}

	public function uploadPhoto()
	{
        $curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'Api/Modul/upload/'.str_replace('/', '', $this->input->post('kode_kegiatan')),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => [
				'file_upload'=> new CURLFILE( 
					$_FILES['file_upload']['tmp_name'], 
					$_FILES['file_upload']['type'], 
					$_FILES['file_upload']['name'] 
				)
			],
			CURLOPT_HTTPHEADER => [
				'key: development_key',
				'Authorization: Basic YWRtaW46MTIz',
				'Content-Type: multipart/form-data'
			]
		]);

        $response = curl_exec($curl);
		curl_close($curl);
		echo $response;
	}

	public function getModulPage()
	{

		$curl = curl_init();
        curl_setopt_array( 
        	$curl, [
			CURLOPT_URL => $this->api_url.'Api/Modul/page/'.$this->uri->segment(3),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => [
				$this->api_key,
				$this->authorization
			]
		]);
        $result_header = curl_exec($curl);
		curl_close($curl);
		echo $result_header;
	}

	public function uploadPhotoDetail( $kode_kegiatan, $dir_name , $file_name )
	{
		//echo $dir_name.$file_name;
        $curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'Api/Modul/upload/'.$dir_name.'/'.$file_name,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => [
				$file_name 	=> new CURLFILE( 
					$_FILES[$file_name]['tmp_name'], 
					$_FILES[$file_name]['type'], 
					$_FILES[$file_name]['name'] 
				)
			],
			CURLOPT_HTTPHEADER => [
				'key: development_key',
				'Authorization: Basic YWRtaW46MTIz',
				'Content-Type: multipart/form-data'
			]
		]);

        $response = curl_exec($curl);
		curl_close($curl);
		return json_decode($response);
	}


	public function uploadPhotoAjax()
	{

		$hasil_upload = $this->uploadPhotoDetail( $this->input->post('kode_kegiatan'), $this->input->post('type') , $this->input->post('file_name'));
		if( $hasil_upload->response == true ){
			$form_upload = [	
				'directory'	=> $hasil_upload->message->directory,
				'gambar' 	=> $hasil_upload->message->file_name,
				'directory_modul'	=> false,
				'file_modul'	=> false
    		];
				
			$curl = curl_init();
	        curl_setopt_array( $curl, [
				CURLOPT_URL => $this->api_url.'Api/Kegiatan/modul/'.str_replace('/','',$this->input->post('kode_kegiatan')),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'PUT',
				CURLOPT_POSTFIELDS => json_encode($form_upload),
				CURLOPT_HTTPHEADER => [
					$this->api_key,
					$this->authorization,
					"Content-Type:application/json", 
					"Accept:application/json"
				]
			]);

	        $result_upload = curl_exec($curl);
	        $response_upload = json_decode($result_upload);
	        if( $response_upload->response ){
	        	$response['status'] = true;
	        	$response['data']	= $hasil_upload;
	        }
	        else{
	        	$response['status'] = false;
	        }
	        
		}
		else{
			$response['status'] = true;
			
		}
		echo json_encode($response);
	}

	public function view( $page=NULL, $act=NULL , $id=NULL )
	{	

  		$this->load->view('template/header_public');
		switch ($page) {

			case 'detail':

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Kategori/all',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_kategori = curl_exec($curl);
		        $kategori = json_decode($result_kategori);
				curl_close($curl);	


				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Jenis/all',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_jenis = curl_exec($curl);
		        $jenis = json_decode($result_jenis);
				curl_close($curl);	

				$curl3 = curl_init();
		        curl_setopt_array( 
		        	$curl3, [
					CURLOPT_URL => $this->api_url.'Api/Kegiatan/'.$act,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl3);
		        $data_header = json_decode($result_header);
				curl_close($curl3);	
		
				$response = [
					"data_kategori" => $kategori->data,
					"data_jenis"	=> $jenis->data,
					"data_header"	=> $data_header->data
				];

				$this->load->view('modul/detail_modul', $response);	
				break;

			case 'create' :

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Kategori/all',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);

		        $result_kategori = curl_exec($curl);
		        $kategori = json_decode($result_kategori);
				curl_close($curl);	

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Jenis/all',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);

		        $result_jenis = curl_exec($curl);
		        $jenis = json_decode($result_jenis);
				curl_close($curl);	

				$response = [
					"data_kategori" => $kategori->data,
					"data_jenis"	=> $jenis->data
				];

				$this->load->view('modul/create_view', $response);
				break;

			case 'save':

				$form = $this->input->post();
				$data_form = [	
					'kategori_kegiatan' => $this->input->post('kategori_kegiatan'),
	    			'jenis_kegiatan' 	=> 'MDL',
	    			'nama_kegiatan' 	=> $this->input->post('nama_kegiatan'),
	    			'deskripsi' 		=> $this->input->post('deskripsi'),
	    			'tanggal_terbit' 	=> $this->input->post('tanggal_terbit'),
	    			'create_by' 		=> $this->session->userdata('username'),
	    			'create_date' 		=> date('Y-m-d H-i-s')
	    		];

				$curl = curl_init();
		        curl_setopt_array( $curl, [
					CURLOPT_URL => $this->api_url.'Api/Kegiatan/modul',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'POST',
					CURLOPT_POSTFIELDS => json_encode($data_form),
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization,
						"Content-Type:application/json", 
						"Accept:application/json"
					]
				]);

		        $result = curl_exec($curl);
		        $response = json_decode($result);
				curl_close($curl);

				if( $response->response == true ){

					$new_kode_kegiatan = $response->insert_id;

					$hasil_upload = $this->uploadPhotoDetail( $new_kode_kegiatan, 'cover' , 'file_upload_cover');
					if( $hasil_upload->response == true ){
						$form_upload = [	
							'directory'	=> $hasil_upload->message->directory,
							'gambar' 	=> $hasil_upload->message->file_name,
							'directory_modul'	=> false,
							'file_modul'	=> false
			    		];
						
						$curl = curl_init();
				        curl_setopt_array( $curl, [
							CURLOPT_URL => $this->api_url.'Api/Kegiatan/modul/'.str_replace('/','',$new_kode_kegiatan),
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => '',
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 0,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => 'PUT',
							CURLOPT_POSTFIELDS => json_encode($form_upload),
							CURLOPT_HTTPHEADER => [
								$this->api_key,
								$this->authorization,
								"Content-Type:application/json", 
								"Accept:application/json"
							]
						]);

				        $result_upload = curl_exec($curl);
				        $response = json_decode($result_upload);
				        if( $response->response ){
				        	$this->response['line'][] = [ 'status' => true, 'message' => "Cover Upload Succesfully" ];
				        }
				        else{
				        	$this->response['line'][] = [ 'status' => false, 'message' => "File Cover Update Failed" ];
				        }
				        curl_close($curl);
					}
					else{
						$this->response['line'][] = [ 'status' => false, 'message' => "File Cover Update Failed" ];
					}




					$hasil_upload = $this->uploadPhotoDetail( $new_kode_kegiatan, 'modul' , 'file_upload_modul');
					if( $hasil_upload->response == true ){

						$form_upload = [	
							'directory'	=> false,
							'gambar' 	=> false,
							'directory_modul'	=> $hasil_upload->message->directory,
							'file_modul'	=> $hasil_upload->message->file_name,
			    		];
						
						$curl = curl_init();
				        curl_setopt_array( $curl, [
							CURLOPT_URL => $this->api_url.'Api/Kegiatan/modul/'.str_replace('/','',$new_kode_kegiatan),
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => '',
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 0,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => 'PUT',
							CURLOPT_POSTFIELDS => json_encode($form_upload),
							CURLOPT_HTTPHEADER => [
								$this->api_key,
								$this->authorization,
								"Content-Type:application/json", 
								"Accept:application/json"
							]
						]);

				        $result_upload = curl_exec($curl);
				        $response = json_decode($result_upload);
				        curl_close($curl);
				        if( $response->response ){
				        	$this->response['line'][] = [ 'status' => true, 'message' => "File Modul Update Succesfully" ];
				        }
				        else{
				        	$this->response['line'][] = [ 'status' => false, 'message' => "File Modul Update Failed" ];
				        }
					}
					else{
						$this->response['line'][] = [ 'status' => false, 'message' => "Modul Upload Failed" ];
					}
					$this->response['status'] = true;
					$this->response['message'] = "Create Modul Successfully";
				}
				else{
					$this->response['status'] = false;
					$this->response['message'] = "Create Modul Failed";
				}
				$this->session->set_flashdata('flash_messages', $this->response);
				redirect( base_url('modul'));
				break;

			case 'edit':

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Kategori/all',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_kategori = curl_exec($curl);
		        $kategori = json_decode($result_kategori);
				curl_close($curl);	


				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Jenis/all',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_jenis = curl_exec($curl);
		        $jenis = json_decode($result_jenis);
				curl_close($curl);	

				$curl3 = curl_init();
		        curl_setopt_array( 
		        	$curl3, [
					CURLOPT_URL => $this->api_url.'Api/Kegiatan/'.str_replace('/', '', $this->input->post('kode')),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl3);
		        $data_header = json_decode($result_header);
				curl_close($curl3);	
		
				$response = [
					"data_kategori" => $kategori->data,
					"data_jenis"	=> $jenis->data,
					"data_header"	=> $data_header->data
				];


				$this->load->view('modul/edit_view', $response);	
				break;
			
			case 'update':

				$form = $this->input->post();

				$data_form = [	
					'kode_kegiatan' => $this->input->post('kode_kegiatan'),
					'kategori_kegiatan' => $this->input->post('kategori_kegiatan'),
	    			'jenis_kegiatan' 	=> 'MDL',
	    			'nama_kegiatan' 	=> $this->input->post('nama_kegiatan'),
	    			'deskripsi' 		=> $this->input->post('deskripsi'),
	    			'tanggal_terbit' 	=> $this->input->post('tanggal_terbit'),
	    			'link_video' 		=> $this->input->post('link_video'),
	    			'update_by' 		=> $this->session->userdata('username'),
	    			'update_date' 		=> date('Y-m-d H-i-s')
	    		];

				$curl = curl_init();
		        curl_setopt_array( $curl, [
					CURLOPT_URL => $this->api_url.'Api/Kegiatan/modul/'.str_replace('/','',$this->input->post('kode_kegiatan')),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'PUT',
					CURLOPT_POSTFIELDS => json_encode($data_form),
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization,
						"Content-Type:application/json", 
						"Accept:application/json"
					]
				]);

		        $result = curl_exec($curl);
		        $response = json_decode($result);
				curl_close($curl);
				$this->response['status'] = $response->response;
				$this->response['message'] = "Modul";
				$this->response['line'][] = [ 'status' => $response->response, 'message' => $response->message ];
				$this->session->set_flashdata('flash_messages', $this->response);
				redirect( base_url('modul'));
				break;

			default:	
				$this->load->view( 'template/global_notification');
				$this->load->view( 'modul/list_view');
				break;
		}
		$this->load->view('template/footer');
	}
}
