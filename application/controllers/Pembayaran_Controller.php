<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
/*use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;*/
class Pembayaran_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Pembayaran_Model','pembayaran',false);
		date_default_timezone_set('Asia/Jakarta');
	}

    
    public function getDetailPembayaran()
    {
        $result = $this->pembayaran->getDetailPenggunaan( $this->input->post('id_penggunaan'))->row();
        echo json_encode($result);
    }

    public function createPembayaran()
    {
        

        $result = $this->pembayaran->createPembayaran([
            'id_pembayaran' => 'PB-'.$this->input->post('bulan').rand(1111,9999),
            'id_tagihan'  => $this->input->post('id_tagihan_penggunaan'),
            'id_pelanggan'  => $this->input->post('id_pelanggan_pembayaran'),
            'bulan_bayar'  => $this->input->post('bulan_penggunaan_pembayaran'),
            'tanggal_pembayaran'  => date('Y-m-d H:i:s'),
            'biaya_admin'  => $this->input->post('total_biaya_admin'),
            'total_bayar'  => $this->input->post('total_pembayaran'),
            'id_user'   => $this->session->userdata('id_user')
        ]);

        if( $result == true ){
            $response['status'] = true;
            $response['message'] = "Sukses";
        }
        else{
            $response['status'] = false;
            $response['message'] = "Gagal";   
        }
        echo json_encode($response);
    }

	public function getPembayaran()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->pembayaran->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            $edit   = "<button type='button' class='btn btn-sm btn-primary btn-edit-pembayaran' data-id='".$field->id_penggunaan."' ><i class='fas fa-edit'></i></button>";

            $delete = "<button type='button' class='btn btn-sm btn-danger btn-delete-penggunaan ml-1' data-id='".$field->id_penggunaan."'><i class='fas fa-trash'></i></button>";


            //  GROUPING BUTTON
            //  ---------------------
            $all_button = $edit;


            $row[] = $all_button;
            $row[] = $field->id_penggunaan;
            $row[] = $field->id_pelanggan;
            $row[] = $field->nama_pelanggan;
            $row[] = $field->nomor_kwh;
            $row[] = $field->alamat;
            $row[] = $field->daya;
            $row[] = $field->bulan;
            $row[] = $field->tahun;
            $row[] = $field->jumlah_meter;
            $row[] = number_format($field->total_bayar,2);
            $row[] = $field->tanggal_pembayaran;
            $row[] = $field->status;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->pembayaran->count_all(),
            "recordsFiltered" => $this->pembayaran->count_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}


}