<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
/*use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;*/
class Penggunaan_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Penggunaan_Model','penggunaan',false);
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getPenggunaan()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->penggunaan->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            $edit   = "<button type='button' class='btn btn-sm btn-primary btn-edit-penggunaan' data-id='".$field->id_penggunaan."' ><i class='fas fa-edit'></i></button>";

            $delete = "<button type='button' class='btn btn-sm btn-danger btn-delete-penggunaan ml-1' data-id='".$field->id_penggunaan."'><i class='fas fa-trash'></i></button>";



            //  GROUPING BUTTON
            //  ---------------------
            $all_button = "";

            $all_button .= $edit.$delete;

            $row[] = $all_button;
            $row[] = $field->id_penggunaan;
            $row[] = $field->id_pelanggan;
            $row[] = $field->nama_pelanggan;
            $row[] = $field->nomor_kwh;
            $row[] = $field->alamat;
            $row[] = $field->daya;
            $row[] = $field->bulan;
            $row[] = $field->tahun;
            $row[] = $field->meter_awal;
            $row[] = $field->meter_akhir;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->penggunaan->count_all(),
            "recordsFiltered" => $this->penggunaan->count_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}

    public function createPenggunaan()
    {
        if ( $this->input->post('id_penggunaan') == '' ){

            $result = $this->penggunaan->createPenggunaan([
                'id_penggunaan' => 'P-'.date('Y', strtotime($this->input->post('tahun'))).rand(1111,9999),
                'id_pelanggan'  => $this->input->post('id_pelanggan'),
                'bulan'  => $this->input->post('bulan'),
                'tahun'  => $this->input->post('tahun'),
                'meter_awal'  => $this->input->post('kwh_awal'),
                'meter_akhir'  => $this->input->post('kwh_akhir')
            ]);
        }
        else{
            $result = $this->penggunaan->updatePenggunaan([
                'id_pelanggan'  => $this->input->post('id_pelanggan'),
                'bulan'  => $this->input->post('bulan'),
                'tahun'  => $this->input->post('tahun'),
                'meter_awal'  => $this->input->post('kwh_awal'),
                'meter_akhir'  => $this->input->post('kwh_akhir')
            ],[
                'id_penggunaan' => $this->input->post('id_penggunaan')
            ]);
        }

        if( $result == true ){
            $response['status'] = true;
            $response['message'] = "Sukses";
        }
        else{
            $response['status'] = false;
            $response['message'] = "Gagal";   
        }
        echo json_encode($response);
    }

    public function deletePenggunaan()
    {
       
        $result = $this->penggunaan->deletePenggunaan([
            'id_penggunaan' => $this->input->post('id_penggunaan')
        ]);

        if( $result == true ){
            $response['status'] = true;
            $response['message'] = "Sukses";
        }
        else{
            $response['status'] = false;
            $response['message'] = "Gagal";   
        }
        echo json_encode($response);
    }

    public function getDetailPenggunaan()
    {
        $result = $this->penggunaan->getDetailPenggunaan( $this->input->post('id_penggunaan'))->row();
        echo json_encode($result);
    }
}