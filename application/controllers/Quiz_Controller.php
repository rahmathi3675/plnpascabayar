<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
/*use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;*/
class Quiz_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Kegiatan_Model','kegiatan',false);
		$this->load->model('Quiz_Model','quiz',false);
		date_default_timezone_set('Asia/Jakarta');
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$this->api_url = "http://localhost/seradu_api/";
		}
		else{
			$this->api_url = "https://api.seradu.id/";	
		}
	}

	public function getDataKegiatan()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->quiz->get_datatables_list();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            $edit = "
                    <form action='".base_url('quiz/manage')."' method='post' style='display: inline-block'>
                        <input type='hidden' name='kode' value='".$field->kode_kegiatan."'>
                        <button class='btn btn-sm btn-primary' type='submit'><i class='fas fa-list-ol'></i></button>
                    </form>";



            //  GROUPING BUTTON
            //  ---------------------
            $all_button = "";

            if( $this->session->userdata('role') == 'adm' || $this->session->userdata('role') == 'nsr'){
            	$all_button .= $edit;
            }

            $row[] = $all_button;
            $row[] = strtoupper($field->kode_kegiatan);
            $row[] = strtoupper($field->nama_kegiatan);
            $row[] = strtoupper(substr( $field->deskripsi, 0,100));
            $row[] = $field->count_detail." MATERI";
            $row[] = $field->tanggal_kegiatan;
            $row[] = $field->waktu_kegiatan;
            $row[] = $field->nama_kategori;
            $row[] = $field->nama_jenis;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->quiz->count_all_list(),
            "recordsFiltered" => $this->quiz->count_filtered_list(),
            "data" => $data,
        );
        echo json_encode($response);
	}

	public function getDataQuiz()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->quiz->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();
            $row[] = "<button class='btn btn-sm btn-edit btn-success' data-id='".$field->kode_materi."' type='submit'><i class='fas fa-edit'></i> EDIT</button>";
            $deskripsi = "<i>".$field->deskripsi_materi."</i>";

            foreach (explode(',', $field->jawaban)  as $key => $value) {
            	$detail = explode('|', $value);
            	
            	if( $detail[2] == 1 ) {
            		$label  = "<br><b>( ".$detail[0]." ) &nbsp;".$detail[1]."</b>";
            	} 
            	else{
            		$label  = "<br>( ".$detail[0]." ) &nbsp;".$detail[1];
            	}
            	$deskripsi .= $label;

            }

            $row[] = $deskripsi;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->quiz->count_all(),
            "recordsFiltered" => $this->quiz->count_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}

	public function uploadPhoto()
	{
        $curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'Api/Kegiatan/upload/'.str_replace('/', '', $this->input->post('kode_kegiatan')),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => [
				'file_upload'=> new CURLFILE( 
					$_FILES['file_upload']['tmp_name'], 
					$_FILES['file_upload']['type'], 
					$_FILES['file_upload']['name'] 
				)
			],
			CURLOPT_HTTPHEADER => [
				'key: development_key',
				'Authorization: Basic YWRtaW46MTIz',
				'Content-Type: multipart/form-data'
			]
		]);

        $response = curl_exec($curl);
		curl_close($curl);
		echo $response;
	}

	public function createQuiz()
	{
		$form = [	
			'kode_kegiatan' => $this->input->post('kode_kegiatan'),
			'deskripsi_materi' 	=> $this->input->post('deskripsi_materi'),
			'create_by' 		=> $this->session->userdata('username'),
			'create_date' 		=> date('Y-m-d H-i-s'),
			'is_key'		=> $this->input->post('is_key'),
			'jawaban'		=> $this->input->post('jawaban')
		];

		$curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'Api/Quiz',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => json_encode($form),
			CURLOPT_HTTPHEADER => [
				$this->api_key,
				$this->authorization,
				"Content-Type:application/json", 
				"Accept:application/json"
			]
		]);

        $result = curl_exec($curl);
        $response = json_decode($result);
		curl_close($curl);
		$this->response['status'] = $response->response;
		$this->response['message'] =$response->message;
		echo json_encode($this->response);
	}

	public function updateQuiz()
	{


		$form = [	
			'kode_materi' => $this->input->post('kode_materi'),
			'deskripsi_materi' 	=> $this->input->post('deskripsi_materi'),
			'update_by' 		=> $this->session->userdata('username'),
			'update_date' 		=> date('Y-m-d H-i-s'),
			'is_key'		=> $this->input->post('is_key'),
			'jawaban'		=> $this->input->post('jawaban_edit'),
			'kode_detail'	=> $this->input->post('kode_detail')
		];


		$curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'Api/Quiz',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'PUT',
			CURLOPT_POSTFIELDS => json_encode($form),
			CURLOPT_HTTPHEADER => [
				$this->api_key,
				$this->authorization,
				"Content-Type:application/json", 
				"Accept:application/json"
			]
		]);
        
        $result = curl_exec($curl);
        $response = json_decode($result);
		curl_close($curl);
		$this->response['status'] = $response->response;
		$this->response['message'] =$response->message;
		echo json_encode($this->response);
	}

	public function updateDetailHasilStudy()
	{


		
		$form = [	
			'kode_jawaban' 	=> $this->input->post('kode_jawaban'),
			'kode_khs'		=> $this->input->post('kode_khs'),
			'kode_materi'	=> $this->input->post('kode_materi'),
			'user'			=> $this->session->userdata('username')
		];

		$curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'Api/Quiz/task',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'PUT',
			CURLOPT_POSTFIELDS => json_encode($form),
			CURLOPT_HTTPHEADER => [
				$this->api_key,
				$this->authorization,
				"Content-Type:application/json", 
				"Accept:application/json"
			]
		]);

        $result = curl_exec($curl);
        $response = json_decode($result);
		curl_close($curl);
		$this->response['status'] = $response->response;
		echo json_encode($this->response);
	}

	public function finishCourse()
	{

		$form = [	
			'kode_khs'		=> $this->input->post('kode_khs'),
			'user'			=> $this->session->userdata('username')
		];

		$curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'Api/Quiz/finish',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'PUT',
			CURLOPT_POSTFIELDS => json_encode($form),
			CURLOPT_HTTPHEADER => [
				$this->api_key,
				$this->authorization,
				"Content-Type:application/json", 
				"Accept:application/json"
			]
		]);

        $result = curl_exec($curl);
		curl_close($curl);
		echo $result;
	}

	public function getDetailQuiz()
	{	
		$curl = curl_init();
        curl_setopt_array( 
        	$curl, [
			CURLOPT_URL => $this->api_url.'Api/Quiz/'. str_replace('/', '', $this->input->post('kode_materi')),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => [
				$this->api_key,
				$this->authorization
			]
		]);
        $result = curl_exec($curl);
        $response = json_decode($result);
		curl_close($curl);	

		$this->response['status'] = $response->response;
		$this->response['data'] =$response->data;
		echo json_encode($this->response);
	}

	public function getAllTask()
	{

		$no_pendaftaran = str_replace('/','', $this->input->post('no_pendaftaran'));
		$curl2 = curl_init();
        curl_setopt_array( 
        	$curl2, [
			CURLOPT_URL => $this->api_url.'Api/Quiz/all_task/'.$no_pendaftaran,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => [
				$this->api_key,
				$this->authorization
			]
		]);
        $result_learnings = curl_exec($curl2);
		curl_close($curl2);
        echo $result_learnings;
	}

	public function view( $page=NULL, $act=NULL , $id=NULL )
	{	

  		$this->load->view('template/header_public');
		switch ($page) {

			case 'elearning':
				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Quiz/course_information/'.$act,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl);
		        $data_header = json_decode($result_header);
				curl_close($curl);
			
				
				
				$response = [
					"data_header"	=> $data_header->data
				];


				$this->load->view('quiz/elearning_view', $response);	
				break;

			case 'process_confirmation':
				$no_pendaftaran = str_replace('/','', $this->input->post('no_pendaftaran'));
				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Acara/calculation/'.$no_pendaftaran,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl);
		        $data_header = json_decode($result_header);
				curl_close($curl);	

				if( $data_header->response == true ){
					redirect( base_url('quiz/elearning/'.$no_pendaftaran));
				}	
				else{
					redirect( base_url('quiz/confirmation/'.$no_pendaftaran));
				}

				break;				

			case 'couching':

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Quiz/calculation/'.str_replace( '/','',$this->input->post('no_pendaftaran') ),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl);
		        $data_header = json_decode($result_header);
				curl_close($curl);	

				/*echo "<pre>";
				print_r($data_header);
				die();*/
			
				$response = [
					"data_header"	=> $data_header->data
				];

				$this->load->view('elearning/course_view', $response);	
				break;				

			case 'confirmation':

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Quiz/course_confirmation/'.$act,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl);
		        $data_header = json_decode($result_header);
				curl_close($curl);	

				$now = date('Y-m-d H:i:s');
				/*echo "<pre>";
				print_r($data_header->data);*/
				if( $now >= $data_header->data->tanggal_mulai && $now <= $data_header->data->tanggal_selesai && $data_header->data->is_start == 1 ){
					redirect( base_url('quiz/elearning/'. str_replace('/','', $data_header->data->no_pendaftaran)));
				}
				else{				
					$response = [
						"data_header"	=> $data_header->data
					];

					$this->load->view('quiz/confirmation_view', $response);	
				}
				break;

			case 'cetak_khs':

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Quiz/cetak_khs/'.str_replace("/","",$act),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl);
		        $data_header = json_decode($result_header);
				curl_close($curl);	
				/*echo "<pre>";
				print_r($data_header->data->header);*/
				$response['data_header'] = $data_header->data->header;
				$response['data_detail'] = $data_header->data->detail;
				$this->load->view('quiz/cetak_khs_view', $response);	
				break;
			
			case 'manage':

				$curl3 = curl_init();
		        curl_setopt_array( 
		        	$curl3, [
					CURLOPT_URL => $this->api_url.'Api/Kegiatan/'.str_replace('/', '', $this->input->post('kode')),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl3);
		        $data_header = json_decode($result_header);
				curl_close($curl3);	

				

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Kategori/'.$data_header->data->kode_kategori,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_kategori = curl_exec($curl);
		        $kategori = json_decode($result_kategori);
				curl_close($curl);	

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Jenis/'.$data_header->data->kode_jenis,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_jenis = curl_exec($curl);
		        $jenis = json_decode($result_jenis);
				curl_close($curl);	

				
				

				$response = [
					"data_kategori" => $kategori->data,
					"data_jenis"	=> $jenis->data,
					"data_header"	=> $data_header->data
				];

				/*echo "<pre>";
				print_r($response);
				echo str_replace('/', '', $this->input->post('kode'));
				die();*/
				$this->load->view('quiz/manage_view', $response);	
				break;
			
			default:	
				$this->load->view( 'template/global_notification');
				$this->load->view( 'quiz/list_view');
				break;
		}
		$this->load->view('template/footer');
	}
}
