<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
/*use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;*/
class Report_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Report_Model','acara',false);
		date_default_timezone_set('Asia/Jakarta');
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$this->api_url = "http://localhost/seradu_api/";
		}
		else{
			$this->api_url = "https://api.seradu.id/";	
		}
	}

	public function getMyReport()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->acara->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            //  GROUPING BUTTON
            //  ---------------------

            $row[] = $field->kode_acara;
            $row[] = $field->no_pendaftaran;
            $row[] = $field->kode_hasil;
            $row[] = $field->deskripsi_acara;
            $row[] = $field->hasil_akhir;
            $row[] = $field->jumlah_materi;
            $row[] = $field->nama_jenis;
            $row[] = $field->nama_kategori;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->acara->count_all(),
            "recordsFiltered" => $this->acara->count_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}

	public function getMyMembershipReport()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->acara->get_membership_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            //  GROUPING BUTTON
            //  ---------------------

            $row[] = $field->no_anggota;
            $row[] = $field->nik;
            $row[] = $field->nama_lengkap;
            $row[] = $field->email_google;
            $row[] = $field->tanggal_lahir;
            $row[] = $field->is_verified;
            $row[] = $field->no_hp;
            $row[] = $field->pendidikan_terakhir;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->acara->count_all_membership(),
            "recordsFiltered" => $this->acara->count_membership_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}

	


	public function view( $page=NULL, $act=NULL , $id=NULL )
	{	

  		$this->load->view('template/header_public');
		switch ($page) {

			case 'membership':

				$this->load->view( 'template/global_notification');
				$this->load->view( 'report/membership_view.php');
				break;

			case 'my_course':

				$this->load->view( 'template/global_notification');
				$this->load->view( 'report/my_course_view.php');
				break;

			default:	
				break;
		}
		$this->load->view('template/footer');
	}
}
