<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
/*use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;*/
class Tagihan_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Tagihan_Model','tagihan',false);
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getTagihan()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->tagihan->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            $edit   = "<button type='button' class='btn btn-sm btn-primary btn-edit-penggunaan' data-id='".$field->id_tagihan."' ><i class='fas fa-edit'></i></button>";

            $delete = "<button type='button' class='btn btn-sm btn-danger btn-delete-penggunaan ml-1' data-id='".$field->id_tagihan."'><i class='fas fa-trash'></i></button>";


            //  GROUPING BUTTON
            //  ---------------------
            $all_button = "";

            $all_button .= $edit.$delete;

            $row[] = "";
            $row[] = $field->id_tagihan;
            $row[] = $field->nama_pelanggan;
            $row[] = $field->nomor_kwh;
            $row[] = $field->daya;
            $row[] = $field->bulan;
            $row[] = $field->tahun;
            $row[] = $field->tarifperkwh;
            $row[] = $field->jumlah_meter;
            $row[] = $field->status;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->tagihan->count_all(),
            "recordsFiltered" => $this->tagihan->count_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}
}