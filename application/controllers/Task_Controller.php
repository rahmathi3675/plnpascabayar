<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
/*use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;*/
class Task_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Anggota_Model','anggota',false);
		date_default_timezone_set('Asia/Jakarta');
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$this->api_url = "http://localhost/seradu_api/";
		}
		else{
			$this->api_url = "https://api.seradu.id/";	
		}
	}

	function logout() {

		$company_code = $this->session->userdata('company_code');

		$this->session->unset_userdata([
			'id','Related_information_email','password','active_status','logged_in','logged_type','user_in_role'
		]);
		$this->session->sess_destroy();
		$response['auth'] = [
			'error' 	=> '1',
			'message' 	=> 'Username or Password Required',

		];
		$response['company_code'] = $company_code;
		$this->load->view('login_view',$response);
	}

	public function view( $page=NULL, $act=NULL , $id=NULL )
	{	

  		$this->load->view('template/header_public');
		switch ($page) {

				
			default:	
				$this->load->view( 'sub_task' );

				break;
		}
		$this->load->view('template/footer');
	}
}
