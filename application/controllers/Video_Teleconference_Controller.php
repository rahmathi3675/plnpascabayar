<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once "Auth.php";
require_once "vendor/autoload.php";
/*use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;*/
class Video_Teleconference_Controller extends Auth {

	public function __construct()
	{	

		parent::__construct();
		$this->authKey();
		$this->load->model('Login_Model','auth',false);
		$this->load->model('Video_Teleconference_Model','video',false);
		date_default_timezone_set('Asia/Jakarta');
		if($_SERVER['HTTP_HOST'] == "localhost"){
			$this->api_url = "http://localhost/seradu_api/";
		}
		else{
			$this->api_url = "https://api.seradu.id/";	
		}
	}

	public function getDataVideo()
	{
        

        //  BUILD DATATABLES SERVERSIDE
        //  ALL ACTION HERE
        //  VALIDATION BUTTON HERE
        

        $list = $this->video->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            
            $no++;
            $row = array();

            $edit = "
                    <form action='".base_url('vitel/edit')."' method='post' style='display: inline-block'>
                        <input type='hidden' name='kode' value='".$field->kode_kegiatan."'>
                        <button class='btn btn-sm btn-primary' type='submit'><i class='fas fa-edit'></i></button>
                    </form>";

            $delete = "<button type='button' class='btn btn-sm btn-danger btn-delete ml-1' data-id='".$field->kode_kegiatan."'><i class='fas fa-trash'></i></button>
                    ";

            $view = "
                    <form action='".base_url('vitel/view')."' method='post' style='display: inline-block'>
                        <input type='hidden' name='id' value='".$field->kode_kegiatan."'>
                        <button class='btn btn-sm btn-info' type='submit'><i class='fas fa-eye'></i></button>
                    </form>";



            //  GROUPING BUTTON
            //  ---------------------
            $all_button = "";

            if( $this->session->userdata('role') == 'adm' || $this->session->userdata('role') == 'nsr'){
            	$all_button .= $edit;
            }

            $row[] = $all_button;
            $row[] = strtoupper($field->kode_kegiatan);
            $row[] = strtoupper($field->nama_kegiatan);
            $row[] = strtoupper(substr( $field->deskripsi, 0,100));
            $row[] = $field->tanggal_kegiatan.' - '.$field->waktu_kegiatan;
            $row[] = $field->nama_kategori;
            $row[] = $field->nama_jenis;
            $data[] = $row;
        }
        
        $response = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->video->count_all(),
            "recordsFiltered" => $this->video->count_filtered(),
            "data" => $data,
        );
        echo json_encode($response);
	}

	public function uploadPhoto()
	{
        $curl = curl_init();
        curl_setopt_array( $curl, [
			CURLOPT_URL => $this->api_url.'Api/Kegiatan/upload/'.str_replace('/', '', $this->input->post('kode_kegiatan')),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => [
				'file_upload'=> new CURLFILE( 
					$_FILES['file_upload']['tmp_name'], 
					$_FILES['file_upload']['type'], 
					$_FILES['file_upload']['name'] 
				)
			],
			CURLOPT_HTTPHEADER => [
				'key: development_key',
				'Authorization: Basic YWRtaW46MTIz',
				'Content-Type: multipart/form-data'
			]
		]);

        $response = curl_exec($curl);
		curl_close($curl);
		echo $response;
	}


	public function view( $page=NULL, $act=NULL , $id=NULL )
	{	

  		$this->load->view('template/header_public');
		switch ($page) {

			case 'detail':
				$curl = curl_init();
		        $result_jenis = curl_exec($curl);
		        $jenis = json_decode($result_jenis);
				curl_close($curl);	

				$curl3 = curl_init();
		        curl_setopt_array( 
		        	$curl3, [
					CURLOPT_URL => $this->api_url.'Api/Kegiatan/'.str_replace('/', '', $act),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl3);
		        $data_header = json_decode($result_header);
				curl_close($curl3);	

				$response = [
					"data_header"	=> $data_header->data
				];

				$this->load->view('video_teleconference/detail_view', $response);	
				break;

			case 'create' :

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Kategori/all',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);

		        $result_kategori = curl_exec($curl);
		        $kategori = json_decode($result_kategori);
				curl_close($curl);	

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Jenis/all',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);

		        $result_jenis = curl_exec($curl);
		        $jenis = json_decode($result_jenis);
				curl_close($curl);	

				$response = [
					"data_kategori" => $kategori->data,
					"data_jenis"	=> $jenis->data
				];

				$this->load->view('video_teleconference/create_view', $response);
				break;

			case 'save':

				$form = $this->input->post();

				$data_form = [	
					'kategori_kegiatan' => $this->input->post('kategori_kegiatan'),
	    			'jenis_kegiatan' 	=> 'VTC',
	    			'nama_kegiatan' 	=> $this->input->post('nama_kegiatan'),
	    			'deskripsi' 		=> $this->input->post('deskripsi'),
	    			'tanggal_kegiatan' 	=> $this->input->post('tanggal_kegiatan'),
	    			'waktu_kegiatan' 	=> $this->input->post('waktu_kegiatan'),
	    			'link_meeting' 		=> $this->input->post('link_meeting'),
	    			'link_feedback' 	=> $this->input->post('link_feedback'),
	    			'create_by' 		=> $this->session->userdata('username'),
	    			'create_date' 		=> date('Y-m-d H-i-s')
	    		];

				$curl = curl_init();
		        curl_setopt_array( $curl, [
					CURLOPT_URL => $this->api_url.'Api/Kegiatan/video_teleconference',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'POST',
					CURLOPT_POSTFIELDS => json_encode($data_form),
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization,
						"Content-Type:application/json", 
						"Accept:application/json"
					]
				]);

		        $result = curl_exec($curl);
		        $response = json_decode($result);
				curl_close($curl);
				$this->response['status'] = $response->response;
				$this->response['message'] = "Activity";
				$this->response['line'][] = [ 'status' => false, 'message' => $response->message ];
				$this->session->set_flashdata('flash_messages', $this->response);
				redirect( base_url('vitel'));
				break;

			case 'edit':

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Kategori/all',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_kategori = curl_exec($curl);
		        $kategori = json_decode($result_kategori);
				curl_close($curl);	

				$curl = curl_init();
		        curl_setopt_array( 
		        	$curl, [
					CURLOPT_URL => $this->api_url.'Api/Jenis/all',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_jenis = curl_exec($curl);
		        $jenis = json_decode($result_jenis);
				curl_close($curl);	

				$curl3 = curl_init();
		        curl_setopt_array( 
		        	$curl3, [
					CURLOPT_URL => $this->api_url.'Api/Kegiatan/'.str_replace('/', '', $this->input->post('kode')),
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization
					]
				]);
		        $result_header = curl_exec($curl3);
		        $data_header = json_decode($result_header);
				curl_close($curl3);	
				/*echo "<pre>";
				print_r($data_header);
				echo str_replace('/', '', $this->input->post('kode'));
				die();*/

				$response = [
					"data_kategori" => $kategori->data,
					"data_jenis"	=> $jenis->data,
					"data_header"	=> $data_header->data
				];


				$this->load->view('video_teleconference/edit_view', $response);	
				break;
			
			case 'update':

				$form = $this->input->post();

				$data_form = [	
					'kode_kegiatan' => $this->input->post('kode_kegiatan'),
					'kategori_kegiatan' => $this->input->post('kategori_kegiatan'),
	    			'jenis_kegiatan' 	=> $this->input->post('jenis_kegiatan'),
	    			'nama_kegiatan' 	=> $this->input->post('nama_kegiatan'),
	    			'deskripsi' 		=> $this->input->post('deskripsi'),
	    			'tanggal_kegiatan' 	=> $this->input->post('tanggal_kegiatan'),
	    			'waktu_kegiatan' 	=> $this->input->post('waktu_kegiatan'),
	    			'link_meeting' 		=> $this->input->post('link_meeting'),
	    			'link_feedback' 		=> $this->input->post('link_feedback'),
	    			'update_by' 		=> $this->session->userdata('username'),
	    			'update_date' 		=> date('Y-m-d H-i-s')
	    		];

				$curl = curl_init();
		        curl_setopt_array( $curl, [
					CURLOPT_URL => $this->api_url.'Api/Kegiatan/video_teleconference',
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'PUT',
					CURLOPT_POSTFIELDS => json_encode($data_form),
					CURLOPT_HTTPHEADER => [
						$this->api_key,
						$this->authorization,
						"Content-Type:application/json", 
						"Accept:application/json"
					]
				]);

		        $result = curl_exec($curl);
		        $response = json_decode($result);
				curl_close($curl);

				$this->response['status'] = $response->response;
				$this->response['message'] = "Activity";
				$this->response['line'][] = [ 'status' => false, 'message' => $response->message ];
				$this->session->set_flashdata('flash_messages', $this->response);
				redirect( base_url('vitel'));
				break;

			default:	
				$this->load->view( 'template/global_notification');
				$this->load->view( 'video_teleconference/list_view');
				break;
		}
		$this->load->view('template/footer');
	}
}
