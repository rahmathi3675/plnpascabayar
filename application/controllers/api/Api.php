<?php

class api extends CI_Controller {

	private $ms_start;
	function __construct() {
		$this->ms_start = strtotime(date('Y:m:d H:i:s'));
		ob_start();
		ini_set('memory_limit', '1024M');
		ini_set('max_execution_time', 300);
		parent::__construct();
	}

	protected function _print($r = '') {
		ob_clean();
		if (!is_array($r)) {
			$ms_end = strtotime(date('Y:m:d H:i:s'));
			ob_end_flush();
			echo nl2br($r." Time elapsed : ".number_format(($ms_end - $this->ms_start), 2)." Seconds\n");
			ob_flush();
			sleep(.1);
			flush();
			ob_start();
			return;
		}

		$out = Array();

		foreach ((Array) $r AS $key => $val) {
			if (is_array($val)) {
				$out[(string) $key] = $this->_json_str($val);
			} else {
				$out[(string) $key] = (string) $val;
			}
		}
		echo json_encode((Array) $out);
	}

	protected function _json_err($code = '', $json = array()) {
		$err = array(
			'-10' => 'signature failed',
			'-20' => 'user not found',
			'-30' => 'invalid parameter',
			'-40' => 'parameter not complete',
			'-99' => 'db err',
			);

		$result = $err[$code];
		if (!$result) $result = 'unknown err';
		$json['code'] = $code;
		$json['description'] = $result;

		return $this->_print($json);
	}

	private function _json_str($var) {
		$out = Array();
		foreach ((Array) $var AS $key => $val) {
			if (is_array($val)) {
				$out[(string) $key] = $this->_json_str($val);
			} else {
				$out[(string) $key] = (string) $val;
			}
		}

		return $out;
	}

	protected function _rfilter($data) {
		if (!method_exists($this, '_filter')) return $data;

		foreach ((Array) $data AS $key => $val) {
			$data[$key] = $this->_filter($val);
		}

		return $data;
	}

}
?>
