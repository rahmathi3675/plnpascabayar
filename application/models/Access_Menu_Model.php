<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Access_Menu_Model extends CI_Model{

    var $table = 'pr_payment_menu_role';

    public function getMenuRole($role_id)
    {
        $this->db->select("p.id, p.menu_name, 
            ( SELECT role_id FROM pr_payment_menu_role WHERE menu_id = p.id AND role_id =  '".$role_id."' ) role_id,
            ( SELECT is_active FROM pr_payment_menu_role WHERE menu_id = p.id AND role_id =  '".$role_id."' ) is_active   
        "
        );
        $this->db->from( 'pr_payment_menu p' );
        return $this->db->get();   
    }

    public function getCountData( $condition )
    {
        $this->db->select('*');
        $this->db->from( $this->table );
        $this->db->where( $condition );
        return $this->db->get();
    }
    /*public function getDetailRole( $condition )
    {
        $this->db->select('*');
        $this->db->from( $this->table );
        $this->db->where( $condition );
        return $this->db->get();
    }*/

    public function insertAccess( $menu_id, $role_id, $next_status )
    {
        $this->db->insert( $this->table, ['menu_id' => $menu_id, 'role_id'=>$role_id, 'is_active'=>$next_status] );
        return $this->db->affected_rows(); 
    }

    public function updateAccess( $menu_id, $role_id, $next_status )
    {
        $this->db->update( $this->table, ['is_active' => $next_status] , ['role_id' => $role_id, 'menu_id' => $menu_id] );
        return $this->db->affected_rows();   
    }

    /*
    public function deleteRole( $condition )
    {
        $this->db->delete( $this->table, $condition );
        return $this->db->affected_rows();   
    }*/

    var $column_order = array(NULL,'p.role_code','p.role_name');
    var $column_search = array('p.role_code','p.role_name');
    var $order = array('p.id' => 'asc');

    public function _get_datatables_query()
    {
        $this->db->select('p.*,
            ( SELECT COUNT(0) FROM pr_payment_menu_role WHERE role_id = p.id AND is_active = 1) menu_active,
            ( SELECT COUNT(0) FROM pr_payment_menu_role WHERE role_id = p.id AND is_active = 0) menu_innactive
        ');
        $this->db->from('pr_payment_role p');
        $i = 0;

        foreach ($this->column_search as $item) 
        {
            if($_POST['search']['value']) 
            {
                if($i===0) 
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i){
                    $this->db->group_end(); 
                }
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
}

?>


