<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_Model extends CI_Model{

    var $table = 'reimburse_comment';

    public function getChat( $doc_id, $project_type )
    {
        $this->db->select('r.*');
        $this->db->from('reimburse_comment r');
        $this->db->where([
            'r.header_id'   => $doc_id,
            'r.project_type' => $project_type
        ]);
        return $this->db->get();
    }

    public function insertChat($doc_id, $my_id ,$project_type, $comment)
    {
        $data = [
            'header_id'     => $doc_id,
            'comment'       => $comment,
            'comment_by'    => $my_id,
            'comment_date'  => date('Y-m-d H:i:s'),
            'project_type'  => $project_type
        ];
        $this->db->insert($this->table, $data);
        
        $ress = $this->db->affected_rows(); 
        if($ress > 0){
            $response = [ 'status' => true, 'message' => 'Sending Success', 'date' => date('Y-m-d H:i:s'), 'name' => $this->session->userdata('user_name')];
        }
        else{
            $response = [ 'status' => false, 'message' => 'Sending Failed'];
        }
        return $response;
    }
    
    public function getAllMessage($doc_id, $project_type)
    {
     
        $this->db->select('c.*, e.nik, e.name');
        $this->db->from('reimburse_comment c');
        $this->db->join('employee e', 'c.comment_by = e.nik', 'left');
        $this->db->where([ 
            'c.header_id' => $doc_id,
            'c.project_type' => $project_type
        ]);
        $this->db->order_by('c.comment_date','asc');
        return $this->db->get();
    }
}

?>


