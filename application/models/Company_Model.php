<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_Model extends CI_Model{

    var $table = 'company';

    public function getDataCompany()
    {
        $this->db->select('company_code, company_name');
        $this->db->from($this->table);
        return $this->db->get();
    }    
}

?>


