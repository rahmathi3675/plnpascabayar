<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configure_Model extends CI_Model{

    var $table = 'configure';

    public function getData( $condition )
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where( $condition );
        return $this->db->get();
    }    

    public function getBaseUrlVendor( )
    {
        $this->db->select('config_desc');
        $this->db->from($this->table);
        $this->db->where( [
        	'code' => 'BASE_URL',
        	'config_code' => 'VENDOR_ATTACHMENT',
        	'value'		=> 1
        ] );
        return $this->db->get();
    }   

    public function getBaseUrlPr( )
    {
        $this->db->select('config_desc');
        $this->db->from($this->table);
        $this->db->where( [
            'code' => 'BASE_URL',
            'config_code' => 'PR_ATTACHMENT',
            'value'     => 1
        ] );
        return $this->db->get();
    }   
 
}

?>


