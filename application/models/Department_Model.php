<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department_Model extends CI_Model{

    var $table = 'department';

    public function getDataDepartment( $params )
    {
        $this->db->select('*');
        $this->db->from('dimension d');
        $this->db->where( $params );
        return $this->db->get();
    } 

    public function getDataDimension( $params )
    {
        $this->db->select('*');
        $this->db->from('dimension d');
        $this->db->where( $params );
        return $this->db->get();
    }   

    public function getDimensionByParams( $key, $company_code )
    {
    	$this->db->select('*');
        $this->db->from( 'dimension' );
        $this->db->where('company', $company_code);
        $this->db->like('name', $key, 'both');
        return $this->db->get(); 
    } 

    public function getSingleDimension( $id_detail, $id_header )
    {
    	$this->db->select('d.*');
        $this->db->from( 'pr_payment_detail pd');
        $this->db->join( 'pr_payment_header p','pd.pyr_code = p.pyr_code AND pd.pyr_header_id = p.id' ,'both');
        $this->db->join( 'dimension d','pd.dimension_code = d.code AND p.company_code = d.company' ,'both');
        $this->db->where('pd.pyr_header_id', $id_header);
        $this->db->where('pd.id', $id_detail);
        return $this->db->get(); 	
    }
}

?>


