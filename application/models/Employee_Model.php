<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_Model extends CI_Model{

    var $table = 'employee_view';

    public function getDataEmployee( $params )
    {
        $this->db->select('attachment_npwp, attachment_siujk');
        $this->db->from($this->table);
        $this->db->where( $params );
        return $this->db->get();
    }

    public function getListEmployee($param)
    {
    	$this->db->select('nik, name');
    	$this->db->from($this->table);
    	$this->db->like('name', $param, 'both');
    	return $this->db->get(); 
    }

    public function getSingleEmployee($nik)
    {
        $this->db->select('nik, name, email_office, job_code,job_name');
        $this->db->from($this->table);
        $this->db->where('nik', $nik);
        return $this->db->get(); 
    }

    public function getAllSimpleEmployee()
    {
        $this->db->select('nik, name, email_office,division_code,division_name,department_code,department_name');
        $this->db->from($this->table);
        $this->db->where('division_code',1);
        $this->db->order_by('name','asc');
        return $this->db->get();    
    }

    public function getAllEmployee()
    {
        $this->db->select('nik, name');
        $this->db->from($this->table);
        $this->db->order_by('name','asc');
        return $this->db->get();    
    }

    public function getMultipleMail( $niks )
    {
        $this->db->select('nik, name, email_office');
        $this->db->from($this->table);
       
        foreach ($niks as $column1) {
            $nik[] = $column1->code;
        }
        $this->db->where_in('nik', $nik);     
        return $this->db->get();    
    }

    public function getAllDataEmployee()
    {
        $this->db->select('nik, name, job_code, department_code, division_code, job_name');
        $this->db->from($this->table);
        $this->db->order_by('name','asc');
        return $this->db->get();    
    }

    public function getApprovalIr( $nik )
    {
        $this->db->select('userid, approverid_1 approver, sequence');
        $this->db->from('budget_approval_sequence');
        $this->db->order_by('sequence','asc');
        $this->db->where('userid', $nik);
        $this->db->where('approver_type','Internal Request');
        $this->db->where('approverid_1 !=', '');
        $this->db->where_not_in('approverid_1', ['BOD','BOD1']);
        return $this->db->get();    
    }
}

?>


