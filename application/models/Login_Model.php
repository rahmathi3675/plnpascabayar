<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_Model extends CI_Model{

    var $table = 'pr_ms_vendor';
    /*-----------------------------------------------------
    |
    |   FUNGSI - FUNGSI UNTUK ADMIN
    |
    |------------------------------------------------------*/ 
    public function getAnggotaByEmail( $email )
    {
        $this->db->select('a.*');
        $this->db->from('anggota a');
        $this->db->where([ 'a.email_google' => $email ]);  
        return $this->db->get();
    }


    public function insertNewAnggota( $form )
    {
        $this->db->trans_begin();
        $this->db->insert('anggota', $form);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }
    }

    public function upateToken( $form, $condition )
    {
        $this->db->trans_begin();
        $this->db->trans_strict(TRUE);

        $this->db->update('anggota', $form, $condition );
        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updatePassword($data, $condition)
    {
        $this->db->update(
            $this->table, 
            $data, 
            array('Related_information_email' => $condition)
        );
        return $this->db->affected_rows();
    }

    public function getUserTaconnect($username)
    {
        $this->db->select('a.*, e.email_office, e.division_code, e.department_code, e.name, e.job_code');
        $this->db->from('admin a');
        $this->db->join('employee_view e','a.username = e.nik','left');
        $this->db->where(['a.username' => $username, 'a.status' => '1']);  
        return $this->db->get();
    }

    public function forgotPassword($email)
    {

        $this->db_learnig->trans_begin();
        $this->db_learnig->trans_strict(TRUE);


        $this->db->query("UPDATE pr_ms_vendor SET password = '".sha1('123456')."' WHERE Related_information_email = '".$email."'");
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return false;
        }
        else
        {
            $from_email = "online@taco.co.id";
            /*$to_email = $data['email_contact'];
            if ($to_email == '') {
                $to_email = "adi.wirasta@taco.co.id";
            }*/
            $to_email = $email;
            $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => 'online@taco.co.id',
                'smtp_pass' => 'tacohpl1234',
                'mailtype'  => 'html',
                'charset'   => 'utf-8',
                'newline'   => "\r\n"
            );
            $this->email->initialize($config);
            $this->email->from($from_email, 'Payment Request Online');
            $this->email->to($to_email);
            $this->email->reply_to('online@taco.co.id', 'Halo Taco');
            $this->email->subject('Forgot Password');
            $this->email->set_mailtype("html");
            $load_data = $this->load->view('home/send_forgot_password_email','',true);
            $this->email->message($load_data);
            
            if( $this->email->send() === true){
                $this->db->trans_commit();
                return true;
            }
            else{
                $this->db->trans_rollback();
                return false;
            }
        }
    }

    public function getRoleId($id , $type)
    {
        if($type == 'vendor'){
            $role = '3';
        }
        else{
            $role = '2';
        }
        $this->db->select('*');
        $this->db->from('pr_payment_user_role');
        $this->db->where('username', $id);
        if($this->db->count_all_results() > 0){
        }
        else{
            $this->db->insert('pr_payment_user_role', [ 'username' => $id, 'role_id' => $role]);
        }
    }
}

?>


