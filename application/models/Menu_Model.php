<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_Model extends CI_Model{

    var $table = 'pr_payment_menu';

    /*public function getMenu()
    {
        $this->db->select('m.*');
        $this->db->from('pr_payment_user_role u');
        $this->db->join('pr_payment_menu_role p','u.role_id = p.role_id','join');
        $this->db->join('pr_payment_menu m', 'p.menu_id = m.id','join');
        $this->db->where('u.username', $this->session->userdata('user_in_role'));
        $this->db->where('p.is_active',1);
        return $this->db->get();
    }*/

    public function getMenuLegal()
    {
        $this->db->select('m.*');
        $this->db->from('pr_payment_user_role u');
        $this->db->join('pr_payment_menu_role p','u.role_id = p.role_id','join');
        $this->db->join('pr_payment_menu m', 'p.menu_id = m.id','join');
        $this->db->where('u.username', $this->session->userdata('user_in_role'));
        $this->db->where('p.is_active',1);
        $this->db->where('m.project_type','reimbursement');
        return $this->db->get();
    }


    public function getAllMenu()
    {
        $this->db->from($this->table);
        return $this->db->get()->result();
    }

    public function getAllIcon()
    {   
        $this->db->select('code,desc');
        $this->db->from('pr_lookup');
        $this->db->where('category','icon-awesome');
        return $this->db->get()->result();
    }

    public function getAllBackground()
    {   
        $this->db->select('code,desc');
        $this->db->from('pr_lookup');
        $this->db->where('category','background');
        return $this->db->get()->result();
    }

    public function getDetailMenu( $condition )
    {
        $this->db->select('*');
        $this->db->from( $this->table );
        $this->db->where( $condition );
        return $this->db->get();
    }

    public function insertMenu( $data )
    {
        $this->db->insert( $this->table, $data );
        return $this->db->affected_rows(); 
    }

    public function updateMenu( $data, $condition )
    {
        $this->db->update( $this->table, $data , $condition );
        return $this->db->affected_rows();   
    }

    public function deleteMenu( $condition )
    {
        $this->db->delete( $this->table, $condition );
        return $this->db->affected_rows();   
    }

    var $column_order = array(NULL,'menu_name','controller_name','menu_image','menu_background',NULL);
    var $column_search = array('id','menu_name','controller_name','menu_image','menu_background');
    var $order = array('id' => 'asc');

    public function _get_datatables_query()
    {
        $this->db->select('*');
        $this->db->from('pr_payment_menu');
        $i = 0;

        foreach ($this->column_search as $item) 
        {
            if($_POST['search']['value']) 
            {
                if($i===0) 
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i){
                    $this->db->group_end(); 
                }
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
}

?>


