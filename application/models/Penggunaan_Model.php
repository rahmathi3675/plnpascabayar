<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Penggunaan_Model extends CI_Model{

    public function createPenggunaan($form)
    {
        $this->db->trans_begin(); 
        $this->db->trans_strict(TRUE);


        $this->db->insert('penggunaan', $form);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } 
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updatePenggunaan($form, $condition)
    {
        $this->db->trans_begin(); 
        $this->db->trans_strict(TRUE);


        $this->db->update('penggunaan', $form, $condition);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } 
        else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function deletePenggunaan( $condition)
    {
        $this->db->trans_begin(); 
        $this->db->trans_strict(TRUE);
        $this->db->delete('penggunaan',$condition);
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } 
        else {
            $this->db->trans_commit();
            return true;
        }
    }


    public function getDetailPenggunaan($id)
    {
        $this->db->select('*');
        $this->db->from('penggunaan');
        $this->db->where('id_penggunaan', $id);
        return $this->db->get();

    }

    var $column_order = array(null,'a.id_penggunaan');
    var $column_search = array("a.id_penggunaan");
        
    public function _get_datatables_query()
    {   
        $this->db->select("a.id_penggunaan, a.id_pelanggan, a.username, a.nama_pelanggan, a.nomor_kwh, a.alamat, a.daya, a.bulan, a.tahun, a.meter_awal, a.meter_akhir");
        $this->db->from('v_penggunaan_listrik a');
        $i = 0;

        foreach ($this->column_search as $item) 
        {
            if($_POST['search']['value']) 
            {
                if($i===0) 
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i){
                    $this->db->group_end(); 
                }
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function count_all()
    {
        $this->db->select('a.id_penggunaan');
        $this->db->from('v_penggunaan_listrik a');
        return $this->db->count_all_results();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>