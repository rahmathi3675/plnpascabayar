<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil_Model extends CI_Model{

	public function getUser( $params )
	{
		$this->db->select('*');
		$this->db->from( 'user' );
		$this->db->where( 'username', $params );
		return $this->db->get();
	}

	public function getPelanggan( $params )
	{
		$this->db->select('*');
		$this->db->from( 'pelanggan' );
		$this->db->where( 'username', $params );
		return $this->db->get();
	}

	public function getDataPelanggan()
	{
		$this->db->select('*');
		$this->db->from( 'pelanggan' );
		return $this->db->get();
	}
}