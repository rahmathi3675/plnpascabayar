<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Quiz_Model extends CI_Model{


    

    var $column_order = array(null,'q.id_materi');
    var $column_search = array('q.id_materi');
    var $order = array('q.id_materi' => 'desc');

    public function _get_datatables_query()
    {   
        $this->db->select("q.id_materi, q.kode_materi, q.deskripsi_materi,(
            SELECT group_concat(value ,'|',deskripsi_jawaban,'|',is_key) FROM detail_materi WHERE kode_materi = q.kode_materi
        ) jawaban");
        $this->db->from('materi_kegiatan q');
        $this->db->where('q.kode_kegiatan', $this->input->post('kode_kegiatan'));
        $i = 0;

        foreach ($this->column_search as $item) 
        {
            if($_POST['search']['value']) 
            {
                if($i===0) 
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i){
                    $this->db->group_end(); 
                }
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function count_all()
    {
        $this->db->select('q.id_materi');
        $this->db->from('materi_kegiatan q');
        return $this->db->count_all_results();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }   


    #   LIST QUIZ DATATABLE

    var $column_order_list = array(
        null,'k.kode_kegiatan','k.nama_kegiatan','k.deskripsi','k.tanggal_kegiatan','k.waktu_kegiatan','kg.nama_kategori','jk.nama_jenis'
    );
    var $column_search_list = array('k.kode_kegiatan');
    var $order_list = array('k.kode_kegiatan' => 'desc');

    public function _get_datatables_query_list()
    {   
        $this->db->select("k.*,kg.nama_kategori, jk.nama_jenis, (
            SELECT COUNT(0) FROM materi_kegiatan WHERE kode_kegiatan = k.kode_kegiatan
        ) count_detail ");
        $this->db->from('kegiatan k');
        $this->db->join('kategori_kegiatan kg','k.kode_kategori = kg.kode_kategori','both');
        $this->db->join('jenis_kegiatan jk','k.kode_jenis = jk.kode_jenis','both');
        $i = 0;

        foreach ($this->column_search_list as $item) 
        {
            if($_POST['search']['value']) 
            {
                if($i===0) 
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_list) - 1 == $i){
                    $this->db->group_end(); 
                }
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order_list[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables_list()
    {
        $this->_get_datatables_query_list();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function count_all_list()
    {
        $this->db->select('k.id');
        $this->db->from('kegiatan k');
        return $this->db->count_all_results();
    }

    function count_filtered_list()
    {
        $this->_get_datatables_query_list();
        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>