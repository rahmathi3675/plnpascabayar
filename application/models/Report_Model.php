<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Report_Model extends CI_Model{


    var $column_order = array('a.kode_acara','da.no_pendaftaran','h.kode_hasil','a.deskripsi_acara','h.hasil_akhir','h.jumlah_materi','jk.nama_jenis','kk.nama_kategori');
    var $column_search = array("a.kode_acara");
        
    public function _get_datatables_query()
    {   
        $this->db->select("
            a.id_acara, a.kode_acara,
            a.deskripsi_acara,
            a.tanggal_mulai, a.tanggal_selesai, 
            k.kode_kegiatan,
            da.no_pendaftaran,
            h.kode_hasil,
            h.hasil_akhir,
            h.jumlah_materi,
            jk.nama_jenis,
            kk.nama_kategori
        ");
        $this->db->from('acara a');
        $this->db->join('kegiatan k','a.kode_kegiatan = k.kode_kegiatan','both');
        $this->db->join('jenis_kegiatan jk','k.kode_jenis = jk.kode_jenis','both');
        $this->db->join('kategori_kegiatan kk','k.kode_kategori = kk.kode_kategori','both');
        $this->db->join('detail_acara da','a.kode_acara = da.kode_acara','both');
        $this->db->join('hasil_kegiatan h','da.no_pendaftaran = h.no_pendaftaran','both');

        $i = 0;

        foreach ($this->column_search as $item) 
        {
            if($_POST['search']['value']) 
            {
                if($i===0) 
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i){
                    $this->db->group_end(); 
                }
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function count_all()
    {
        $this->db->select('a.id_acara');
        $this->db->from('acara a');
        return $this->db->count_all_results();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }







    var $column_order_membership = array('a.no_anggota','a.nik','a.nama_lengkap','a.email_google','a.tanggal_lahir','a.is_verified','a.no_hp','a.pendidikan_terakhir');
    var $column_search_membership = array("a.no_anggota");
        
    public function _get_datatables_membership_query()
    {   
        $this->db->select("a.no_anggota,a.nik,a.nama_lengkap, a.email_google,a.tanggal_lahir, a.is_verified, a.no_hp,a.pendidikan_terakhir
            
        ");
        $this->db->from('anggota a');

        $i = 0;

        foreach ($this->column_search_membership as $item) 
        {
            if($_POST['search']['value']) 
            {
                if($i===0) 
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i){
                    $this->db->group_end(); 
                }
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order_membership[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }


    function get_membership_datatables()
    {
        $this->_get_datatables_membership_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function count_all_membership()
    {
        $this->db->select('a.id_acara');
        $this->db->from('acara a');
        return $this->db->count_all_results();
    }

    function count_membership_filtered()
    {
        $this->_get_datatables_membership_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

}

?>