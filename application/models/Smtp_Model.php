<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Smtp_Model extends CI_Model
{	

	var $table = 'smtp';

	public function getEmailConfig()
	{
		$this->db->select( '*' );
		$this->db->from( $this->table );
		$this->db->where( [
			'type' 	=> "admin",
			'status'	=> 1
		] );
		return $this->db->get();	
	}
}