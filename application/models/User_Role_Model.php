<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Role_Model extends CI_Model{

    private function cekUserRole( $nik )
    {
        $this->db->select('*');
        $this->db->from("pr_payment_user_role");
        $this->db->where(['username' => $nik] );
        return $this->db->count_all_results();
    }

    public function UpdateUserRole( $nik, $role_id )
    {   
        $cek = $this->cekUserRole($nik);
        if( $cek > 0 ){
            $result = $this->db->update('pr_payment_user_role', ['role_id' =>$role_id], ['username' => $nik] );
            if( $result ){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            $result = $this->db->insert("pr_payment_user_role", ['username' => $nik, 'role_id' => $role_id, 'create_by' => $this->session->userdata('id'), 'create_date' => date("Y-m-d H;i:s") ]);
            if( $result ){
                return true;
            }
            else{
                return false;
            }
        }
    }

    var $table = 'employee_view';

    var $column_order = array(null,'e.nik','e.name','e.company_code','e.division_name','department_name','e.description','e.job_name','loc_name');
    var $column_search = array('e.nik','e.name','e.company_code','e.division_name','department_name','e.description','e.job_name','loc_name');
    var $order = array('e.nik' => 'asc');

    public function _get_datatables_query()
    {
        $user = $this->session->all_userdata();

        $this->db->select('e.*, u.role_id');
        $this->db->from('employee_view e');
        $this->db->join('pr_payment_user_role u','e.nik = u.username','left');

        $i = 0;

        foreach ($this->column_search as $item) 
        {
            if($_POST['search']['value']) 
            {
                if($i===0) 
                {
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i){
                    $this->db->group_end(); 
                }
            }
            $i++;
        }
         
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1){
            $this->db->limit($_POST['length'], $_POST['start']);
            $query = $this->db->get();
            return $query->result();
        }
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
}

?>


