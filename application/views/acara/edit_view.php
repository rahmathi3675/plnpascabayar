<?php echo form_open_multipart('acara/update'); ?>
<form method="post" id="form-create-acara" name="form_create_acara">

<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 pt-5 pl-5 pr-5 pb-0">
		<nav aria-label="breadcrumb">
		  	<ol class="breadcrumb bg-nu">
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url() ?>"><i class="fas fa-home"></i> Home</a></li>
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url('acara') ?>"><i class="fas fa-list"></i> List Acara</a></li>
		    	<li class="breadcrumb-item  text-white active" aria-current="page"><i class="fas fa-solar-panel"></i> Update</li>
		  	</ol>
		</nav>
	</div>
	<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 p-5 " >
		<input readonly type="hidden" style="display: none;" id="id-acara" name="id_acara" value="<?= rand(666666666666666,888888888888888).$data_header->id_acara.rand(666666666666666,999999999999999) ?>">
		<input readonly type="hidden" name="kode_acara" id="kode-acara" readonly maxlength="100" value="<?= $data_header->kode_acara ?>" style="display: none;">
	    <!-- <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-0 ">
	    	<div class="input-group input-group-md">
	        	<input type="file" id="file-upload" name="file_upload">
	        </div>
	    </div> -->
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 mt-4 bg-dark p-3">
	    	<div class="input-group input-group-md justify-content-center">
	        	<img id="photo-kegiatan" class="" src="<?= $this->api_url.'document/activity/'.$data_header->gambar ?>" width="100%" height="100%" alt="img">
	      	</div>
	    </div>
	</div>
	<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 
		pr-xl-5 pt-xl-5 pl-xl-0 
		pr-lg-5 pt-lg-5 pl-lg-0 
		pr-md-5 pt-md-5 pl-md-0
		pr-sm-4 pt-sm-0 pl-sm-4
		">
		<div class="form-group col-lg-6 col-md-8 col-sm-12 mb-3  mt-0 mt-md-5 mt-lg-2">
	    	<div class="input-group input-group-md">
	        	<input type="text" class="form-control form-biodata bg-white" placeholder="KODE ACARA" maxlength="200" disabled value="<?= $data_header->kode_acara ?>">
	      	</div>
	    </div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<select class="form-control form-biodata bg-white" id="kode-kegiatan" name="kode_kegiatan"  required>
	        		<option value="">- PILIH KATGEORI -</option>
	        		<?php
	        		foreach ($data_kegiatan as $index => $column) {
	        			$selected_kegiatan =  ( $column->kode_kegiatan == $data_header->kode_kegiatan ) ? " selected ": "";
	        			?>
	        			<option <?= $selected_kegiatan ?> value="<?= $column->kode_kegiatan ?>"><?= strtoupper($column->nama_kegiatan) ?></option>
	        			<?php 
	        		}
	        		?>
	        	</select>
	      	</div>
	    </div>	

	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<textarea class="form-control form-biodata bg-white" name="deskripsi" placeholder="DESKRIPSI" rows="3" required><?= $data_header->deskripsi_acara ?></textarea>
	      	</div>
	    </div>
	    <div class="row">
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-3 ml-3">
		    	<div class="input-group input-group-md">
		    		<input type="text" readonly class="form-control form-biodata datetimepicker bg-white" name="tanggal_mulai" placeholder="TANGGAL MULAI"  required value="<?= date('Y-m-d', strtotime($data_header->tanggal_mulai)) ?>">
		      	</div>
		    </div>
		    <div class="col-lg-4 col-md-4 col-sm-6 mb-3 ml-auto mr-3">
		    	<div class="input-group input-group-md">
		    		<input type="time"  class="form-control form-biodata bg-white" name="waktu_mulai" placeholder="" value="10:00" required value="<?= date('H:i', strtotime($data_header->tanggal_mulai) ) ?>">
		      	</div>
		    </div>
		</div>
		<div class="row">
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-3 ml-3">
		    	<div class="input-group input-group-md">
		    		<input type="text" readonly class="form-control form-biodata datetimepicker bg-white" name="tanggal_selesai" placeholder="TANGGAL SELESAI"  required value="<?= date('Y-m-d', strtotime($data_header->tanggal_selesai)) ?>">
		      	</div>
		    </div>
		    <div class="col-lg-4 col-md-4 col-sm-6 mb-3 ml-auto mr-3">
		    	<div class="input-group input-group-md">
		    		<input type="time"  class="form-control form-biodata bg-white" name="waktu_selesai" placeholder="" value="10:00" required value="<?= date('H:i:s', strtotime($data_header->tanggal_selesai) ) ?>">
		      	</div>
		    </div>
		</div>
	    <div class="form-group col-lg-4 col-md-6 col-sm-12 mb-3 mt-5">
	    	<button class="btn btn-md btn-success btn-block"><i class="fas fa-save"></i> UPDATE</button>
	    </div>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 p-5">
		
	</div>
</div>
	</form>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/kegiatan.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/kegiatan.js');?>"></script>
