
<form method="post" id="form-verifikasi" name="form_verifikasi" enctype="multipart/form-data">
<div class="content-main row bg-white ml-xl-0 mr-xl-0 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 text-center bg-nu">
		<div class="row align-items-center">
			<div class="col-xl-2 col-lg-3 col-md-3 col-sm-5 p-2">
				<img  src="<?= base_url('assets/images/logo_nu_no_bg.png') ?>" height="100" width="150">
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-7 p-2">
				<h4 class="text-center text-white">DATA KEANGGOTAAN</h4>
				<h6 class="text-center text-white">LIST ANGGOTA</h6>
			</div>
			<div class="col-md-4 col-lg-3 col-md-3 col-sm-12 d-none d-sm-none d-md-none d-lg-block d-xl-block pr-5 text-right text-white  ml-auto">
				<h5 class="text-left text-white text-right">NU FATAYAT</h5>
				<h6 class="text-left text-white text-right">JAWA BARAT</h6>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 bg-white">

		<div class="table-responsive mt-5 mb-5">
			<table id="table-list-anggota" class="table table-xs table-bordered">
				<thead>
					<tr>
						<td>ACTION</td>
						<td>PHOTO PROFIL</td>
						<td>NIK</td>
						<td>NO ANGGOTA</td>
						<td>NAMA LENGKAP</td>
						<td>TEMPAT LAHIR</td>
						<td>TGL LAHIR</td>
						<td>ALAMAT RUMAH</td>
						<td>RT</td>
						<td>RW</td>
						<td>KODE POS</td>
						<td>NO HP</td>
						<td>PENDIDIKAN</td>
						<td>EMAIL</td>
						<td>IS VERIFIED</td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/anggota.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/list_anggota.js');?>"></script>
