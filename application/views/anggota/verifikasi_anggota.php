
<form method="post" id="form-verifikasi" name="form_verifikasi" enctype="multipart/form-data">
<div class="content-main row bg-dark ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 text-center bg-nu">
		<div class="row align-items-center">
			<div class="col-xl-2 col-lg-3 col-md-3 col-sm-5 p-2">
				<img  src="<?= base_url('assets/images/logo_nu_no_bg.png') ?>" height="100" width="150">
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-7 p-2">
				<h4 class="text-center text-white">DATA KEANGGOTAAN</h4>
				<h6 class="text-center text-white">VERIFIKASI ANGGOTA</h6>
			</div>
			<div class="col-md-4 col-lg-3 col-md-3 col-sm-12 d-none d-sm-none d-md-none d-lg-block d-xl-block pr-5 text-right text-white  ml-auto">
				<h5 class="text-left text-white text-right">NU FATAYAT</h5>
				<h6 class="text-left text-white text-right">JAWA BARAT</h6>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 bg-white">
		<div class="row">
			<div class="col-md-5 col-lg-5 p-4 pb-0">
				<input readonly type="hidden" style="display: none;" id="id-anggota" name="id_anggota" value="<?= rand(666666666666666,888888888888888).$profil->id.rand(666666666666666,999999999999999) ?>">
				<textarea readonly name="token" id="token" style="display: none;"><?= $this->session->userdata('token') ?></textarea>
				<input readonly type="hidden" name="email" maxlength="100" value="<?= $profil->email_google ?>" style="display: none;">
				<input readonly type="text" name="username"  maxlength="100" value="<?= $profil->username ?>" style="display: none;">
			    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 mt-4">
			    	<div class="input-group input-group-md">
			        	<img id="photo-profile" class="bg-light" src="<?= $api_url.'document/users/'.$profil->local_directory.'/'.$profil->photo_profil ?>" width="300" height="400" alt="img">
			      	</div>
			    </div>
			</div>
			<div class="col-md-7 col-lg-7 p-4 pb-0">

				<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
			    	<div class="input-group input-group-md">
			        	<input readonly type="text" class="form-control form-biodata bg-white" placeholder="NO ANGGOTA" value="<?= $profil->no_anggota ?>">
			      	</div>
			    </div>
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 row  pr-0 mr-0">
				    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-2 pr-md-0 pr-sm-0">
				    	<div class="input-group input-group-md">
				        	<input readonly type="text" class="form-control form-biodata bg-white" placeholder="PCNU" value="<?= $profil->pcnu ?>">
				      	</div>
				    </div>
				    <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-2 pl-md-1 pr-md-0 pr-sm-0">
				    	<div class="input-group input-group-md">
				        	<input readonly type="text" class="form-control form-biodata bg-white" placeholder="MWCNU" value="<?= $profil->mwcnu ?>">
				      	</div>
				    </div>
				</div>
			 	<div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 ">
			    	<div class="input-group input-group-md">
			        	<input type="email" readonly class="form-control form-biodata bg-white" maxlength="100" placeholder="EMAIL" value="<?= strtoupper($profil->email_google) ?>">
			      	</div>
			    </div>
		    	<div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3">
			    	<div class="input-group input-group-md">
			        	<input readonly type="text" class="form-control form-biodata bg-white" placeholder="NAMA LENGKAP" value="<?= $profil->nama_lengkap ?>">
			      	</div>
			    </div>
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3">
				    <div class="form-group col-xl-8 col-lg-8 col-md-12 col-sm-12 mb-2 pl-0 pr-md-1 pr-sm-0 ml-auto">
				    	<div class="input-group input-group-md">
				        	<input type="text" readonly class="form-control form-biodata bg-white" placeholder="NIK" value="<?= $profil->nik ?>">
				      	</div>
				    </div>
				</div>
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 row  pr-0 mr-0">
			    	<div class="form-group col-xl-8 col-lg-8 col-md-12 col-sm-12 mb-2 pr-md-0 pr-sm-0 ml-auto">
				    	<div class="input-group input-group-md">
			        		<input type="text" readonly class="form-control form-biodata bg-white"  placeholder="TEMPAT LAHIR" value="<?= $profil->tempat_lahir ?>">
				      	</div>
				    </div>
				</div>
				<div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 row  pr-0 mr-0">
				    <div class="form-group col-xl-8 col-lg-8 col-md-12 col-sm-12 mb-2 pr-md-0 pr-sm-0 ml-auto">
				    	<div class="input-group input-group-md">
				        	<input type="text" readonly class="form-control form-biodata datetimepicker bg-white" placeholder="TANGGAL LAHIR" value="<?= $profil->tanggal_lahir ?>">
				      	</div>
				    </div>
			    </div>
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3">
				    <div class="form-group col-xl-8 col-lg-8 col-md-12 col-sm-12 mb-2 pl-0 pr-1 ml-auto">
				    	<div class="input-group input-group-md">
				        	<select class="form-control form-biodata bg-white">
				        		<?php 
				        		$selected_pendidikan = '';
				        		foreach ($pendidikan as $index => $column) {
				        			if( $profil->pendidikan_terakhir == $column->kode ){
				        				?><option selected value="<?= $column->kode ?>"><?= strtoupper( $column->deskripsi ) ?></option><?php 
				        			}
				        		}
				        		?>
				        	</select>
				      	</div>
				    </div>
				</div>
				<div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-0">
				    <div class="form-group col-xl-8 col-lg-8 col-md-12 col-sm-12 mb-2 pl-0 pr-1 ml-auto">
				    	<div class="input-group input-group-md">
				        	<select class="form-control form-biodata bg-white" readonly>
				        		<option value="">- JABATAN -</option>
				        		<?php 
				        		foreach ($jabatan as $index => $column) {
				        			$selected = ( $column->kode_jabatan == $profil->jabatan) ? ' selected ': '';
				        			?><option <?= $selected ?> value="<?= $column->kode_jabatan ?>"><?= strtoupper( $column->nama_jabatan ) ?></option><?php 
				        		}
				        		?>
				        	</select>
				      	</div>
				    </div>
				</div>
				<div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-0">
				    <div class="form-group col-xl-8 col-lg-8 col-md-12 col-sm-12 mb-2 pl-0 pr-1 ml-auto">
				    	<div class="input-group input-group-md">
				        	<select class="form-control form-biodata bg-white" name="kode_role">
				        		<option value="">- ROLE -</option>
				        		<?php 
				        		foreach ($role as $index => $column) {
				        			$selected = ( $column->kode_role == $profil->role) ? ' selected ': '';
				        			?><option <?= $selected ?> value="<?= $column->kode_role ?>"><?= strtoupper( $column->deskripsi_role ) ?></option><?php 
				        		}
				        		?>
				        	</select>
				      	</div>
				    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 bg-white pb-5">
  			
		<div class="row">
			<div class="col-md-5 col-lg-5 p-4">
				<div class="form-group col-lg-12 col-md-12 col-sm-12">
			    	<div class="input-group input-group-md">
			        	<select class="form-control form-biodata" id="postal-code" name="kode_pos">
			        		<?php 
			        		if( $posts != false ){ ?>
			        			<option value="<?= $posts->postal_code.','.$posts->id_province.','.$posts->id_city.','.$posts->kecamatan.','.$posts->kelurahan ?>" selected><?= strtoupper( $posts->postal_code.', '.$posts->province.', '.$posts->city.', '.$posts->kecamatan.', '.$posts->kelurahan ) ?></option><?php 
			        		}
			        		?>
			        	</select>
			      	</div>
			    </div>
			    <div class="form-group col-lg-12 col-md-12 col-sm-12">
			    	<div class="input-group input-group-md">
			        	<textarea readonly class="form-control form-biodata bg-white" id="alamat-rumah" placeholder="ALAMAT"><?= $profil->alamat_rumah ?></textarea>
			      	</div>
			    </div>
			    <div class="form-group col-lg-12 col-md-12 col-sm-12 row mb-3">
			    	<div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-3 mb-2">
				    	<div class="input-group input-group-md">
			        		<input readonly type="text" class="form-control form-biodata bg-white" id="rt" placeholder="RT" value="<?= $profil->rt ?>">
				      	</div>
				    </div>
				    <div class="form-group col-xl-1 col-lg-1 col-md-1 col-sm-1 mb-2">/</div>
				    <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-3  mb-2 ml-2">
				    	<div class="input-group input-group-md">
				        	<input readonly type="text" class="form-control form-biodata bg-white" id="rw" placeholder="RW" value="<?= $profil->rw ?>">
				      	</div>
				    </div>
			    </div>
			</div>
			<div class="col-md-7 col-lg-7 p-4">
	    		<div class="form-group col-lg-12 col-md-12 col-sm-12 row mb-3 pr-0">
			    	<div class="input-group input-group-md">
			      	</div>
			    </div>
	    		
			    <div class="form-group col-lg-8 col-md-8 col-sm-12 row mb-3 pr-0">
			    	<div class="input-group input-group-md">
			        	<input type="text" readonly class="form-control form-biodata bg-white" id="provinsi"  placeholder="PROVINSI" value="<?= strtoupper(@$posts->province) ?>">
			      	</div>
			    </div>
			    <div class="form-group col-lg-8 col-md-8 col-sm-12 row mb-3 pr-0">
			    	<div class="input-group input-group-md">
			        	<input type="text" readonly class="form-control form-biodata bg-white" id="kabupaten-kota"  placeholder="KABUPATEN KOTA" value="<?= strtoupper(@$posts->city) ?>">
			      	</div>
			    </div>
			    <div class="form-group col-lg-8 col-md-8 col-sm-12 row mb-3 pr-0">
			    	<div class="input-group input-group-md">
			        	<input type="text" readonly class="form-control form-biodata bg-white" id="kecamatan" n placeholder="KECAMATAN" value="<?= strtoupper(@$posts->kecamatan) ?>">
			      	</div>
			    </div>
			    <div class="form-group col-lg-8 col-md-8 col-sm-6 row mb-3 pr-0">
			    	<div class="input-group input-group-md">
			        	<input type="text" readonly class="form-control form-biodata bg-white" id="kelurahan"  placeholder="KELURAHAN" value="<?= strtoupper(@$posts->kelurahan) ?>">
			      	</div>
			    </div>
		    </div>

		    
		</div>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 bg-white pb-5 mb-5">
		<div class="row align-items-center">
			<div class="col"></div>
			<div class="col-xl-6 col-lg-6 col-md-6">
				<button id="btn-verifikasi" type="button" class="form-control btn bg-nu text-white p-3"><i class="fas fa-save fa-1x"></i> UPDATE PROFIL </button>
			</div>
			<div class="col"></div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/anggota.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/verifikasi_anggota.js');?>"></script>
