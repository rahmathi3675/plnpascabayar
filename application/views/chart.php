<main id="content-main">
	<div class="row">
		<div class="col-md-12 col-lg-12 col-xl-12 text-center mb-0" > 
			<div class="col-md-12">
				<h6 class="text-left">KALENDER KEGIATAN TERPOPULER</h6>
				<hr>
				
			</div>
		</div>
	</div>
	
	 <div class="row">
 		<div class="col-lg-6 mt-5">
 			<canvas id="myChart" width="400" height="250"></canvas>
		<script>
			const ctx = document.getElementById('myChart').getContext('2d');
			const myChart = new Chart(ctx, {
			    type: 'line',
			    data: {
			        labels: ['Januari', 'Febuari', 'Maret', 'April', 'Mei', 'Juni'],
			        datasets: [{
			            label: '# of Anggota',
			            data: [12, 19, 3, 5, 2, 3],
			            backgroundColor: [
			                'rgba(255, 99, 132, 0.2)',
			                'rgba(54, 162, 235, 0.2)',
			                'rgba(255, 206, 86, 0.2)',
			                'rgba(75, 192, 192, 0.2)',
			                'rgba(153, 102, 255, 0.2)',
			                'rgba(255, 159, 64, 0.2)'
			            ],
			            borderColor: [
			                'rgba(255, 99, 132, 1)',
			                'rgba(54, 162, 235, 1)',
			                'rgba(255, 206, 86, 1)',
			                'rgba(75, 192, 192, 1)',
			                'rgba(153, 102, 255, 1)',
			                'rgba(255, 159, 64, 1)'
			            ],
			            borderWidth: 1
			        }]
			    },
			    options: {
			    	plugins: {
			            legend: {
			            	display: false,
			                labels: {
			                    font: {
			                        size: 10,
			                        family: 'Helvetica Neue',
			                        style: 	'normal',
			                        weight: "italic",
			                        lineHeight: 1
			                    }
			                }
			            },
			            tooltips: {
				            enabled: false
				        }
			        },
			    	layout: {
			            padding: {
			            	left: 10
			            }
			        },
			        scales: {
			        	x: {
			        		grid: {
						        display: false
					      	},
		        		 	title: {
						        display: true,
						        text: 'Keanggotaan 2021'
					      	},	
					      	ticks: {
						        major: {
						          	enabled: false
						        },
					        	color: (context) => context.tick && context.tick.major && '#FFF', font: function(context) {
						          	if (context.tick && context.tick.major) {
						            	return {
						              	weight: 'bold'
						            	};
					          		}
					        	},
					        	backdropPadding: {
				                  	x: 1,
				                  	y: 0
				              	},
				              	
					        },
					        
			        	},
			            y: {
			            	grid: {
						        display: true,
						        // Y Boder Axis
						        borderColor: 'rgba(0,152,70,1)',
						        // Y Width Axix
					        	borderWidth: 0.5,
					        	// Line Width
					        	lineWidth: 0,
					        	// Line Colour
					        	color: '#FB2C00',
					        	//drawBorder: true,
					        	//drawOnChartArea: true,
					        	//drawTicks: false,
					        	//offset: true,
					        	//tickBorderDash: true,
					        	//tickBorderDashOffset: true,
					        	//tickColor: '#000',
					        	//tickLength: 1,
					        	//tickWidth: 1
					      	},
			                beginAtZero: true,
			                type: 'linear',
			                title: {
						        display: true,
						        text: 'Jumlah Anggota',
						        // Text Color
						        color: '#FB2C00',

						        backgroundColor: '#000',
					      	}	
			            }
			        }
			    }
			});
		</script>
 		</div>
 		<div class="col-lg-6 mt-5">
 			<canvas id="multi-chart" width="400" height="250"></canvas>
		<script>
			  
			const ctx2 = document.getElementById('multi-chart').getContext('2d');
			const mixedChart = new Chart(ctx2, {
			    data: {
			        datasets: [
			        	{
				            type: 'bar',
				            label: 'Quiz',
				            data: [10, 20, 30, 40, 0 ,5],
				            backgroundColor: 'rgba(0,152,70,1)',
				        }, {
				            type: 'bar',
				            label: 'Video Confrence',
				            data: [10, 30, 15, 50,5,10],
				            backgroundColor: '#007bff'
				        },
				        {
				            type: 'bar',
				            label: 'Video Tutorial',
				            data: [5, 10, 15, 20,15,10],
				            backgroundColor: '#17a2b8'
				        }
			        ],
			        labels: ['January', 'February', 'March', 'April','Mei','Juni']
			    },
			    options: {
			    	plugins: {
			            legend: {
			            	display: true,
			                labels: {
			                    font: {
			                        size: 12,
			                        family: 'Helvetica Neue',
			                        style: 	'normal',
			                        weight: "bold",
			                        lineHeight: 1
			                    }
			                }
			            },
			            tooltips: {
				            enabled: false
				        }
			        },
			    	layout: {
			            padding: {
			            	left: 10
			            }
			        },
			        scales: {
			        	x: {
			        		grid: {
						        display: false
					      	},
		        		 	title: {
						        display: true,
						        text: 'Kegiatan Learning Managemen System'
					      	},	
					      	ticks: {
						        major: {
						          	enabled: false
						        },
					        	color: (context) => context.tick && context.tick.major && '#FFF', font: function(context) {
						          	if (context.tick && context.tick.major) {
						            	return {
						              	weight: 'bold'
						            	};
					          		}
					        	},
					        	backdropPadding: {
				                  	x: 1,
				                  	y: 0
				              	},
				              	
					        },
					        
			        	},
			            y: {
			            	grid: {
						        display: true,
						        // Y Boder Axis
						        borderColor: 'rgba(0,152,70,1)',
						        // Y Width Axix
					        	borderWidth: 0.5,
					        	// Line Width
					        	lineWidth: 0,
					        	// Line Colour
					        	color: '#FB2C00',
					        	//drawBorder: true,
					        	//drawOnChartArea: true,
					        	//drawTicks: false,
					        	//offset: true,
					        	//tickBorderDash: true,
					        	//tickBorderDashOffset: true,
					        	//tickColor: '#000',
					        	//tickLength: 1,
					        	//tickWidth: 1
					      	},
			                beginAtZero: true,
			                type: 'linear',
			                title: {
						        display: true,
						        text: 'Jumlah Pelatihan / Acara',
						        // Text Color
						        color: '#FB2C00',

						        backgroundColor: '#000',
					      	}	
			            }
			        }
			    }
			});
		</script>
 		</div>
	</div> 
</div>