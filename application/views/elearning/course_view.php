<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">

	<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3"></div>
	<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 pb-5 
		pr-xl-5 pt-xl-5 pl-xl-0 
		pr-lg-5 pt-lg-5 pl-lg-0 
		pr-md-5 pt-md-5 pl-md-0
		pr-sm-4 pt-sm-0 pl-sm-4
		
		">
		<hr class="bg-nu">
		<h5 class="font-bold text-center bg-info pt-2 ">COOURSE CONFIRMATION</h5>
		<hr class="bg-nu">

		<div class="row mt-3">
			<div class="col-lg-6 col-md-6 col-sm-12 mb-3  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-right font-light mb-0">No Pendaftaran</p>
	        	<p class="text-right font-regular mb-0"><b><?= $data_header->no_pendaftaran ?></b></p>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-2  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-left font-light mb-0">Kode Acara</p>
	        	<p class="text-left font-regular mb-0"><b><?= $data_header->kode_acara ?></b></p>
		    </div>
		</div>
		<div class="row mt-1">
			<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-1 ">
	    		<p class="text-center mb-0 font-light">Nama Kegiatan</p>
	        	<p class="text-center font-regular"><b><?= $data_header->nama_kegiatan ?></b></p>
	      	</div>

		    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-1">
	    		<p class="text-center mb-0 font-light">Deksripsi Acara</p>
	        	<p class="text-center font-regular mb-0"><b><?= $data_header->deskripsi_acara ?></b></p>
	      	</div>
		</div>
		<div class="row mt-1">
			<div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-right font-light mb-0">JENIS</p>
	        	<p class="text-right font-regular mb-0"><b><?= $data_header->nama_jenis ?></b></p>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-left font-light mb-0">KATEGORI</p>
	        	<p class="text-left font-regular mb-0"><b><?= $data_header->nama_kategori ?></b></p>
		    </div>
		</div>
		<div class="row mt-1">
			<div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-right font-light mb-0">TGL MULAI</p>
	        	<p class="text-right font-regular"><b><?= $data_header->tanggal_mulai ?></b></p>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-left font-light mb-0">TGL SELESAI</p>
	        	<p class="text-left font-regular"><b><?= $data_header->tanggal_selesai ?></b></p>
		    </div>
		</div>
		<div class="row mt-1" style="padding-bottom: 200px;">
	   	    <div class="col-lg-12 col-md-12 col-sm-12 mb-3  mt-0 mt-md-5 mt-lg-2 text-center">
	   	    	<?php echo form_open_multipart('quiz/couching'); ?>
				<form method="post" id="form-detail-acara" name="form_detail_acara">
					<input type="hidden" name="no_pendaftaran" value="<?= $data_header->no_pendaftaran ?>">
		    		<button type="submit" class="btn btn-md btn-success pl-4 pr-4"><i class="fas fa-save"></i> MULAI</button>
		    	</form>
		    </div>
		</div>
	</div>
	<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3"></div>
</div>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/kegiatan.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/kegiatan.js');?>"></script>
