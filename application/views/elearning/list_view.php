
<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 pt-5 pl-5 pr-5 pb-0">
		<nav aria-label="breadcrumb">
		  	<ol class="breadcrumb bg-nu">
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url() ?>"><i class="fas fa-home"></i> Home</a></li>
		    	<li class="breadcrumb-item  text-white active" aria-current="page"><i class="fas fa-list"></i> List Elearning</li>
		  	</ol>
		</nav>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 bg-white pl-5 pr-5 mt-1">
		<div class="table-responsive mt-1 mb-5">
			<table id="table-list-acara" class="table table-sm table-bordered">
				<thead class="bg-light">
					<tr>
						<td>#</td>
						<td>KODE ACARA</td>
						<td>KODE KEGIATAN</td>
						<td>DESKRIPSI</td>
						<td>TANGGAL MULAI</td>
						<td>TANGGAL SELESAI</td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/elearning.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/list_elearning.js');?>"></script>
