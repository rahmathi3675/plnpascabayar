<!--
<nav class="navbar navbar-dark bg-dark">
	<a class="navbar-brand" href="#">Never expand</a>
	<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

		<div class="navbar-collapse collapse" id="navbarsExample01" style="">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#">Disabled</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
				</ul>
				<form class="form-inline my-2 my-md-0">
					<input class="form-control" type="text" placeholder="Search" aria-label="Search">
				</form>
		</div>
</nav>
<nav class="navbar navbar-expand navbar-dark bg-dark">
			<a class="navbar-brand" href="#">Always expand</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

	<div class="collapse navbar-collapse" id="navbarsExample02">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Link</a>
			</li>
		</ul>
		<form class="form-inline my-2 my-md-0">
				<input class="form-control" type="text" placeholder="Search">
		</form>
	</div>
</nav>

<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Expand at sm</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample03" aria-controls="navbarsExample03" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExample03">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#">Disabled</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
						<div class="dropdown-menu" aria-labelledby="dropdown03">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
				</ul>
				<form class="form-inline my-2 my-md-0">
					<input class="form-control" type="text" placeholder="Search">
				</form>
		</div>
</nav>

<nav class="navbar navbar-expand-md navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Expand at md</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExample04">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#">Disabled</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
						<div class="dropdown-menu" aria-labelledby="dropdown04">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
				</ul>
				<form class="form-inline my-2 my-md-0">
					<input class="form-control" type="text" placeholder="Search">
				</form>
		</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Expand at lg</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExample05">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#">Disabled</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown05" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
						<div class="dropdown-menu" aria-labelledby="dropdown05">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
				</ul>
				<form class="form-inline my-2 my-md-0">
					<input class="form-control" type="text" placeholder="Search">
				</form>
		</div>
</nav>

<nav class="navbar navbar-expand-xl navbar-dark bg-dark">
	<a class="navbar-brand" href="#">Expand at xl</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample06" aria-controls="navbarsExample06" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExample06">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#">Disabled</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown06" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
						<div class="dropdown-menu" aria-labelledby="dropdown06">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
				</ul>
				<form class="form-inline my-2 my-md-0">
					<input class="form-control" type="text" placeholder="Search">
				</form>
		</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="container">
				<a class="navbar-brand" href="#">Container</a>
				<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="navbar-collapse collapse" id="navbarsExample07" style="">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item active">
							<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Link</a>
						</li>
						<li class="nav-item">
							<a class="nav-link disabled" href="#">Disabled</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown07" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
							<div class="dropdown-menu" aria-labelledby="dropdown07">
								<a class="dropdown-item" href="#">Action</a>
								<a class="dropdown-item" href="#">Another action</a>
								<a class="dropdown-item" href="#">Something else here</a>
							</div>
						</li>
					</ul>
					<form class="form-inline my-2 my-md-0">
						<input class="form-control" type="text" placeholder="Search" aria-label="Search">
					</form>
				</div>
		</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="navbar-collapse justify-content-md-center collapse" id="navbarsExample08" style="">
				<ul class="navbar-nav">
					<li class="nav-item active">
						<a class="nav-link" href="#">Centered nav only <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#">Disabled</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown08" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
						<div class="dropdown-menu" aria-labelledby="dropdown08">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
				</ul>
		</div>
</nav>
-->

<!-- 
<div class="container">
	<nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
			<a class="navbar-brand" href="#">Navbar-</a>
			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="navbar-collapse collapse" id="navbarsExample09" style="">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#">Disabled</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
						<div class="dropdown-menu" aria-labelledby="dropdown09">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
				</ul>
				<form class="form-inline my-2 my-md-0">
					<input class="form-control" type="text" placeholder="Search" aria-label="Search">
				</form>
			</div>
	</nav>

	<nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
			<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample10" aria-controls="navbarsExample10" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="navbar-collapse justify-content-md-center collapse" id="navbarsExample10" style="">
				<ul class="navbar-nav">
					<li class="nav-item active">
						<a class="nav-link" href="#">Centered nav only <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">Link</a>
					</li>
					<li class="nav-item">
						<a class="nav-link disabled" href="#">Disabled</a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown10" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
						<div class="dropdown-menu" aria-labelledby="dropdown10">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
						</div>
					</li>
				</ul>
			</div>
	</nav>

	<main role="main">
		<div class="jumbotron">
			<div class="col-sm-8 mx-auto">
				<h1>Navbar examples</h1>
				<p>This example is a quick exercise to illustrate how the navbar and its contents work. Some navbars extend the width of the viewport, others are confined within a <code>.container</code>. For positioning of navbars, checkout the <a href="../navbar-top/">top</a> and <a href="../navbar-top-fixed/">fixed top</a> examples.</p>
				<p>At the smallest breakpoint, the collapse plugin is used to hide the links and show a menu button to toggle the collapsed content.</p>
				<p>
					<a class="btn btn-primary" href="../../components/navbar/" role="button">View navbar docs »</a>
				</p>
			</div>
		</div>
	</main>
</div>
-->

<!-- navbar-dark -->
<nav class="navbar fixed-top bg-nu flex-md-nowrap p-0 shadow">
 	<a class="navbar-brand bg-nu col-sm-4 col-md-3 col-lg-2 mr-0 pt-2 custom-sidebar text-white" href="#">
 		<img src="<?= base_url('assets/images/logo_nu_no_bg.png') ?>" style="max-width: 300px; height:  20px;">
 		NU FATAYAT
    </a>

	<!-- <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search"> -->
	<i id="btn-nav-navigation" class="mr-auto ml-3 m-0 fas fa-bars text-white bg-nu"></i>
	<a class="text-white ml-auto" href="#"><i id="btn-nav-navigation" class="m-0 fas fa-home text-white bg-nu"></i>&nbsp;Home</a>
	<a class="text-white ml-4" href="#"><i id="btn-nav-navigation" class="m-0 fas fa-user text-white bg-nu"></i>&nbsp;Profil</a>
	<ul class="navbar-nav px-3 bg-nu">
		<li class="nav-item text-nowrap">
			<a class="nav-link text-white" href="#"><i class="fas fa-sign-out-alt"></i> Sign out</a>
		</li>
	</ul>
</nav>

<div class="container-fluid">
	<div class="row">
		<!-- 
			d-none 		: cara pintas untuk memblock / hidden
			d-md-block	: 
		-->
		<nav class="col-sm-4 col-md-3 col-lg-2 bg-nu sidebar custom-sidebar" >
			<div class="sidebar-sticky" style="">
				<div class="" id="collapseExample">
					<ul class="nav flex-column">
						<li class="nav-item text-center">
							<img src="<?= base_url('assets/images/logo_nu_with_bg.png') ?>" style="max-width: 300px; height:  100px;">
							<hr class="m-2 bg-white">
						</li>
					 	<li class="nav-item">
							<a class="nav-link" href="#">
								<i class="fas fa-tachometer-alt text-white"></i>
								</svg>
								<label class="text-white">&nbsp;Dashboard</label>
								<span class="sr-only">(current)</span>
							</a>
						</li>
						<li class="nav-item custome-collapse collapse-lms" data-toggle="collapse" href="#collapse-lms" role="button" aria-expanded="false">
							<span>
							<a class="nav-link" href="#" style="">
								<i class="fas fa-chalkboard-teacher text-white"></i>
								<label class="text-white">&nbsp;Learning</label>
								<i class="fas fa-angle-left m-2 text-white icon-collapse-lms" style="height: 10px; float: right;"></i>
							</a>
							</span>
							<ul class="collapse" id="collapse-lms" style="list-style-type: none;">
								<li class="nav-item">
									<a class="nav-link pl-0" href="#">
										<i class="fas fa-chalkboard text-white"></i>
										<label class="text-white">&nbsp;Quiz</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0" href="#">
										<i class="fas fa-video text-white"></i>
										<label class="text-white">&nbsp;Video Tutorial</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0" href="#">
										<i class="fas fa-chalkboard-teacher text-white"></i>
										<label class="text-white">&nbsp;Video Teleconference</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0" href="#">
										<i class="fas fa-book text-white"></i>
										<label class="text-white">&nbsp;&nbsp;E-book</label>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item collapse-lms" data-toggle="collapse" href="#collapse-management-anggota" role="button" aria-expanded="false">
							<span>
							<a class="nav-link" href="#" style="">
								<i class="fas fa-users text-white"></i>
								<label class="text-white">&nbsp;Managemen Anggota</label>
								<i class="fas fa-angle-left m-2 text-white icon-collapse-lms" style="height: 10px; float: right;"></i>
							</a>
							</span>
							<ul class="collapse" id="collapse-management-anggota" style="list-style-type: none;">
								<li class="nav-item">
									<a class="nav-link pl-0" href="#">
										<i class="fas fa-check text-white"></i>
										<label class="text-white">&nbsp;Verifikasi</label>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item collapse-lms" data-toggle="collapse" href="#collapse-management-event" role="button" aria-expanded="false">
							<span>
							<a class="nav-link" href="#" style="">
								<i class="fas fa-calendar-alt text-white"></i>
								<label class="text-white">&nbsp;Kalendar</label>
								<i class="fas fa-angle-left m-2 text-white icon-collapse-lms" style="height: 10px; float: right;"></i>
							</a>
							</span>
							<ul class="collapse" id="collapse-management-event" style="list-style-type: none;">
								<li class="nav-item">
									<a class="nav-link pl-0" href="#">
										<i class="fas fa-calendar-plus text-white"></i>
										<label class="text-white">&nbsp;Entry Kegiatan</label>
									</a>
								</li>
							</ul>
						</li>
					</ul>

					<li class="nav-item">
						<a class="nav-link" href="#">
							<!-- <i class="fas fa-tachometer-alt text-white"></i> -->
							<label class="text-white">&nbsp;Laporan</label>
							<span class="sr-only">(current)</span>
						</a>
					</li>
					<ul class="nav flex-column mb-2">
						<li class="nav-item">
							<a class="nav-link" href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text text-white"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
								<label class="text-white">Laroran Keanggotaan</label>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text text-white"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
								<label class="text-white">Laroran LMS</label>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		<main role="main" class="col col-sm-8 col-md-9 col-lg-10 ml-sm-auto ml-md-auto ml-lg-auto content-custom-sidebar bg-white">

			<div class="row">
				<div class="col-lg-12 mt-5">
					<div class="row p-3">
				        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 bg-white p-1" style="border: 1px solid transparent;">
				          	<div class="custom-card bg-white" style="border: 2px solid lightgreen; border-radius: 5%;">
					            <div class="custom-card-header p-3">
					              	<div class="shadow-dark text-center bg-success" style="position: absolute; border: 3px solid white; top: -20%; border-radius: 10%; padding: 10px;">
					                	<i class="fas fa-users fa-2x text-white"></i>
					              	</div>
					              	<div class="text-end pt-3">
					                	<p class="text-sm mb-0 text-right" style="float: left; font-size: 1.5em;">MEMBER</p>
					                	<h4 class="mb-0 text-right">2,120</h4>
					              	</div>
				            	</div>
					            <hr class="dark horizontal my-0 ml-2 mr-2">
					            <div class="custom-card-footer p-3 pb-3">
					              	<p class="mb-0 text-right"><span class="text-success text-sm font-weight-bolder">120 </span>Baru mendaftar</p>
					            </div>
				          	</div>
				        </div>
				       	<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 bg-white p-1" style="border: 1px solid transparent;">
				          	<div class="custom-card bg-white" style="border: 2px solid lightgreen; border-radius: 5%;">
					            <div class="custom-card-header p-3 pt-2">
					              	<div class="shadow-dark text-center bg-primary" style="position: absolute; border: 3px solid white; top: -20%; border-radius: 10%; padding: 10px;">
					                	<i class="fas fa-graduation-cap fa-2x text-white"></i>
					              	</div>
					              	<div class="text-end pt-3">
					                	<p class="text-sm mb-0 text-right" style="float: left; font-size: 1.5em;">QUIZ</p>
					                	<h4 class="mb-0 text-right">280</h4>
					              	</div>
				            	</div>
					            <hr class="dark horizontal my-0 ml-2 mr-2">
					            <div class="custom-card-footer p-3 pb-3">
					              	<p class="mb-0 text-right"><span class="text-success text-sm font-weight-bolder">10 </span>Pelatihan Tercapai</p>
					            </div>
				          	</div>
				        </div>
				        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 bg-white p-1" style="border: 1px solid transparent;">
				          	<div class="custom-card bg-white" style="border: 2px solid lightgreen; border-radius: 5%;">
					            <div class="custom-card-header p-3 pt-2">
					              	<div class="shadow-dark text-center bg-info" style="position: absolute; border: 3px solid white; top: -20%; border-radius: 10%; padding: 10px;">
					                	<i class="fas fa-video fa-2x text-white"></i>
					              	</div>
					              	<div class="text-end pt-3">
					                	<p class="text-sm mb-0 text-right" style="float: left; font-size: 1.5em;">VIDEO</p>
					                	<h4 class="mb-0 text-right">65</h4>
					              	</div>
				            	</div>
					            <hr class="dark horizontal my-0 ml-2 mr-2">
					             <div class="custom-card-footer p-3 pb-3">
					              	<p class="mb-0 text-right"><span class="text-success text-sm font-weight-bolder">3 </span>Video Tercapai</p>
					            </div>
				          	</div>
				        </div>
				        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 bg-white p-1" style="border: 1px solid transparent;">
				          	<div class="custom-card bg-white" style="border: 2px solid lightgreen; border-radius: 5%;">
					            <div class="custom-card-header p-3 pt-2">
					              	<div class="shadow-dark text-center bg-success" style="position: absolute; border: 3px solid white; top: -20%; border-radius: 10%; padding: 10px;">
					                	<i class="fas fa-book-open fa-2x text-white"></i>
					              	</div>
					              	<div class="text-end pt-3">
					                	<p class="text-sm mb-0 text-right" style="float: left; font-size: 1.5em;">E-BOOK</p>
					                	<h4 class="mb-0 text-right">2,100</h4>
					              	</div>
				            	</div>
					            <hr class="dark horizontal my-0 ml-2 mr-2">
					             <div class="custom-card-footer p-3 pb-3">
					              	<p class="mb-0 text-right"><span class="text-success text-sm font-weight-bolder">15 </span>Telah diunduh</p>
					            </div>
				          	</div>
				        </div>
     	 			</div>
     	 		</div>
     	 		<div class="col-lg-6 mt-5">
     	 			<canvas id="myChart" width="400" height="250"></canvas>
					<script>
						const ctx = document.getElementById('myChart').getContext('2d');
						const myChart = new Chart(ctx, {
						    type: 'line',
						    data: {
						        labels: ['Januari', 'Febuari', 'Maret', 'April', 'Mei', 'Juni'],
						        datasets: [{
						            label: '# of Anggota',
						            data: [12, 19, 3, 5, 2, 3],
						            backgroundColor: [
						                'rgba(255, 99, 132, 0.2)',
						                'rgba(54, 162, 235, 0.2)',
						                'rgba(255, 206, 86, 0.2)',
						                'rgba(75, 192, 192, 0.2)',
						                'rgba(153, 102, 255, 0.2)',
						                'rgba(255, 159, 64, 0.2)'
						            ],
						            borderColor: [
						                'rgba(255, 99, 132, 1)',
						                'rgba(54, 162, 235, 1)',
						                'rgba(255, 206, 86, 1)',
						                'rgba(75, 192, 192, 1)',
						                'rgba(153, 102, 255, 1)',
						                'rgba(255, 159, 64, 1)'
						            ],
						            borderWidth: 1
						        }]
						    },
						    options: {
						    	plugins: {
						            legend: {
						            	display: false,
						                labels: {
						                    font: {
						                        size: 10,
						                        family: 'Helvetica Neue',
						                        style: 	'normal',
						                        weight: "italic",
						                        lineHeight: 1
						                    }
						                }
						            },
						            tooltips: {
							            enabled: false
							        }
						        },
						    	layout: {
						            padding: {
						            	left: 10
						            }
						        },
						        scales: {
						        	x: {
						        		grid: {
									        display: false
								      	},
					        		 	title: {
									        display: true,
									        text: 'Keanggotaan 2021'
								      	},	
								      	ticks: {
									        major: {
									          	enabled: false
									        },
								        	color: (context) => context.tick && context.tick.major && '#FFF', font: function(context) {
									          	if (context.tick && context.tick.major) {
									            	return {
									              	weight: 'bold'
									            	};
								          		}
								        	},
								        	backdropPadding: {
							                  	x: 1,
							                  	y: 0
							              	},
							              	
								        },
								        
						        	},
						            y: {
						            	grid: {
									        display: true,
									        // Y Boder Axis
									        borderColor: 'rgba(0,152,70,1)',
									        // Y Width Axix
								        	borderWidth: 0.5,
								        	// Line Width
								        	lineWidth: 0,
								        	// Line Colour
								        	color: '#FB2C00',
								        	//drawBorder: true,
								        	//drawOnChartArea: true,
								        	//drawTicks: false,
								        	//offset: true,
								        	//tickBorderDash: true,
								        	//tickBorderDashOffset: true,
								        	//tickColor: '#000',
								        	//tickLength: 1,
								        	//tickWidth: 1
								      	},
						                beginAtZero: true,
						                type: 'linear',
						                title: {
									        display: true,
									        text: 'Jumlah Anggota',
									        // Text Color
									        color: '#FB2C00',

									        backgroundColor: '#000',
								      	}	
						            }
						        }
						    }
						});
					</script>
     	 		</div>
     	 		<div class="col-lg-6 mt-5">
     	 			<canvas id="multi-chart" width="400" height="250"></canvas>
					<script>
						  
						const ctx2 = document.getElementById('multi-chart').getContext('2d');
						const mixedChart = new Chart(ctx2, {
						    data: {
						        datasets: [
						        	{
							            type: 'bar',
							            label: 'Quiz',
							            data: [10, 20, 30, 40, 0 ,5],
							            backgroundColor: 'rgba(0,152,70,1)',
							        }, {
							            type: 'bar',
							            label: 'Video Confrence',
							            data: [10, 30, 15, 50,5,10],
							            backgroundColor: '#007bff'
							        },
							        {
							            type: 'bar',
							            label: 'Video Tutorial',
							            data: [5, 10, 15, 20,15,10],
							            backgroundColor: '#17a2b8'
							        }
						        ],
						        labels: ['January', 'February', 'March', 'April','Mei','Juni']
						    },
						    options: {
						    	plugins: {
						            legend: {
						            	display: true,
						                labels: {
						                    font: {
						                        size: 12,
						                        family: 'Helvetica Neue',
						                        style: 	'normal',
						                        weight: "bold",
						                        lineHeight: 1
						                    }
						                }
						            },
						            tooltips: {
							            enabled: false
							        }
						        },
						    	layout: {
						            padding: {
						            	left: 10
						            }
						        },
						        scales: {
						        	x: {
						        		grid: {
									        display: false
								      	},
					        		 	title: {
									        display: true,
									        text: 'Kegiatan Learning Managemen System'
								      	},	
								      	ticks: {
									        major: {
									          	enabled: false
									        },
								        	color: (context) => context.tick && context.tick.major && '#FFF', font: function(context) {
									          	if (context.tick && context.tick.major) {
									            	return {
									              	weight: 'bold'
									            	};
								          		}
								        	},
								        	backdropPadding: {
							                  	x: 1,
							                  	y: 0
							              	},
							              	
								        },
								        
						        	},
						            y: {
						            	grid: {
									        display: true,
									        // Y Boder Axis
									        borderColor: 'rgba(0,152,70,1)',
									        // Y Width Axix
								        	borderWidth: 0.5,
								        	// Line Width
								        	lineWidth: 0,
								        	// Line Colour
								        	color: '#FB2C00',
								        	//drawBorder: true,
								        	//drawOnChartArea: true,
								        	//drawTicks: false,
								        	//offset: true,
								        	//tickBorderDash: true,
								        	//tickBorderDashOffset: true,
								        	//tickColor: '#000',
								        	//tickLength: 1,
								        	//tickWidth: 1
								      	},
						                beginAtZero: true,
						                type: 'linear',
						                title: {
									        display: true,
									        text: 'Jumlah Pelatihan / Acara',
									        // Text Color
									        color: '#FB2C00',

									        backgroundColor: '#000',
								      	}	
						            }
						        }
						    }
						});
					</script>
     	 		</div>
			</div>


			<!-- 
			<div style="display: none;" class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
				<h4>Dashboard</h4>
				<div class="btn-toolbar mb-2 mb-md-0">
					<div class="btn-group mr-2">
						<button class="btn btn-sm btn-outline-secondary">Share</button>
						<button class="btn btn-sm btn-outline-secondary">Export</button>
					</div>
					<button class="btn btn-sm btn-outline-secondary dropdown-toggle">
						<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>
						This week
					</button>
				</div> 
			</div>
			-->

			<!-- <div  class="table-responsive">
				<table  class="table table-bordered table-sm">
					<thead>
						<tr>
							<th>#</th>
							<th>Header</th>
							<th>Header</th>
							<th>Header</th>
							<th>Header</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1,001</td>
							<td>Lorem</td>
							<td>ipsum</td>
							<td>dolor</td>
							<td>sit</td>
						</tr>
						<tr>
							<td>1,002</td>
							<td>amet</td>
							<td>consectetur</td>
							<td>adipiscing</td>
							<td>elit</td>
						</tr>
						<tr>
							<td>1,003</td>
							<td>Integer</td>
							<td>nec</td>
							<td>odio</td>
							<td>Praesent</td>
						</tr>
						<tr>
							<td>1,003</td>
							<td>libero</td>
							<td>Sed</td>
							<td>cursus</td>
							<td>ante</td>
						</tr>
						<tr>
							<td>1,004</td>
							<td>dapibus</td>
							<td>diam</td>
							<td>Sed</td>
							<td>nisi</td>
						</tr>
						<tr>
							<td>1,005</td>
							<td>Nulla</td>
							<td>quis</td>
							<td>sem</td>
							<td>at</td>
						</tr>
						<tr>
							<td>1,006</td>
							<td>nibh</td>
							<td>elementum</td>
							<td>imperdiet</td>
							<td>Duis</td>
						</tr>
						<tr>
							<td>1,007</td>
							<td>sagittis</td>
							<td>ipsum</td>
							<td>Praesent</td>
							<td>mauris</td>
						</tr>
						<tr>
							<td>1,008</td>
							<td>Fusce</td>
							<td>nec</td>
							<td>tellus</td>
							<td>sed</td>
						</tr>
						<tr>
							<td>1,009</td>
							<td>augue</td>
							<td>semper</td>
							<td>porta</td>
							<td>Mauris</td>
						</tr>
						<tr>
							<td>1,010</td>
							<td>massa</td>
							<td>Vestibulum</td>
							<td>lacinia</td>
							<td>arcu</td>
						</tr>
						<tr>
							<td>1,011</td>
							<td>eget</td>
							<td>nulla</td>
							<td>Class</td>
							<td>aptent</td>
						</tr>
						<tr>
							<td>1,012</td>
							<td>taciti</td>
							<td>sociosqu</td>
							<td>ad</td>
							<td>litora</td>
						</tr>
						<tr>
							<td>1,013</td>
							<td>torquent</td>
							<td>per</td>
							<td>conubia</td>
							<td>nostra</td>
						</tr>
						<tr>
							<td>1,014</td>
							<td>per</td>
							<td>inceptos</td>
							<td>himenaeos</td>
							<td>Curabitur</td>
						</tr>
						<tr>
							<td>1,015</td>
							<td>sodales</td>
							<td>ligula</td>
							<td>in</td>
							<td>libero</td>
						</tr>
					</tbody>
				</table>
			</div> -->
		</main>
	</div>
</div>
<link href="<?php echo base_url('css_app/dashboard.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/dashboard.js');?>"></script>

