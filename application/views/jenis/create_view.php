
<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 pt-5 pl-5 pr-5 pb-0">
		<nav aria-label="breadcrumb">
		  	<ol class="breadcrumb bg-nu">
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url() ?>"><i class="fas fa-home"></i> Home</a></li>
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url('jenis_kegiatan') ?>"><i class="fas fa-list"></i> List Type</a></li>
		    	<li class="breadcrumb-item  text-white active" aria-current="page">Type</li>
		  	</ol>
		</nav>
	</div>
	<div class="col-sm-12 col-md-8 col-lg-6 col-xl-6 p-5" style="height: 600px" >
		<?php echo form_open_multipart('jenis_kegiatan/save'); ?>
		<form method="post" id="form-create-kategori" name="form_create_kategori">
			
			<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
		    	<div class="input-group input-group-md">
		        	<input type="text" class="form-control form-biodata bg-white" name="kode" placeholder="KODE JENIS" value="" maxlength="3" required>
		      	</div>
		    </div>
		    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
		    	<div class="input-group input-group-md">
		        	<input type="text" class="form-control form-biodata bg-white" name="nama_jenis" placeholder="NAMA JENIS" value="" required>
		      	</div>
		    </div>
		    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 mt-5">
		    	<button class="btn btn-md btn-success"><i class="fas fa-save"></i> SAVE</button>
		    </div>
		</form>
	</div>
</div>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/jenis_kegiatan.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/jenis_kegiatan.js');?>"></script>
