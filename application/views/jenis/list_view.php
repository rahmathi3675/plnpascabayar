
<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 pt-5 pl-5 pr-5 pb-0">
		<nav aria-label="breadcrumb">
		  	<ol class="breadcrumb bg-nu">
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url() ?>"><i class="fas fa-home"></i> Home</a></li>
		  	</ol>
		</nav>
		<a href="<?= base_url('jenis_kegiatan/create') ?>" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i> CREATE TYPE</a>	
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 bg-white pl-5 pr-5 mt-1">
		<div class="table-responsive mt-1 mb-5">
			<table id="table-list-kategori-kegiatan" class="table table-xs table-bordered">
				<thead class="bg-light">
					<tr>
						<td>ACTION</td>
						<td>CODE</td>
						<td>TYPE</td>
						<td>CREATE BY</td>
						<td>CREATE DATE</td>
						<td>UPDATE BY</td>
						<td>UPDATE DATE</td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<form method="post" id="form-verifikasi" name="form_kategori" enctype="multipart/form-data">
</form>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/kategori_kegiatan.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/list_jenis_kegiatan.js');?>"></script>
