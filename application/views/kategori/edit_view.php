
<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 pt-5 pl-5 pr-5 pb-0">
		<nav aria-label="breadcrumb">
		  	<ol class="breadcrumb bg-nu">
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url() ?>"><i class="fas fa-home"></i> Home</a></li>
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url('kategori_kegiatan') ?>"><i class="fas fa-list"></i> List Category</a></li>
		    	<li class="breadcrumb-item  text-white active" aria-current="page">Edit</li>
		  	</ol>
		</nav>
	</div>
	<div class="col-sm-12 col-md-8 col-lg-6 col-xl-6 p-5" style="height: 600px" >
		<?php echo form_open_multipart('kategori_kegiatan/update'); ?>
		<form method="post" id="form-create-kategori" name="form_create_kategori">
			<input type="hidden" name="kode" value="<?= $data->kode_kategori ?>">
			<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
		    	<div class="input-group input-group-md">
		        	<input type="text" class="form-control form-biodata bg-white" placeholder="KODE KATEGORI" value="<?= $data->kode_kategori ?>" maxlength="3" readonly>
		      	</div>
		    </div>
		    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
		    	<div class="input-group input-group-md">
		        	<input type="text" class="form-control form-biodata bg-white" name="nama_kategori" placeholder="NAMA KATEGORI" value="<?= $data->nama_kategori ?>" required>
		      	</div>
		    </div>
		    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 mt-5">
		    	<button class="btn btn-md btn-success"><i class="fas fa-save"></i> UPDATE</button>
		    </div>
		</form>
	</div>
</div>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/kategori_kegiatan.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/kategori_kegiatan.js');?>"></script>
