<link href="<?= base_url('assets/vendors/fullcalendar-5/lib/main.css') ?>" rel='stylesheet' />
<script src="<?= base_url('assets/vendors/fullcalendar-5/lib/main.js') ?>"></script>

<?php 
if( $this->session->userdata('username') != ''){

	?>
	<div class="bg-white pt-5 p-3 mb-0 px-5">
		<div class="row">
			
			<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 px-4 mt-2"> 
				<div class="bg-primary">
					<div class="row">
						<div class="col-6 col-sm-8 col-md-8 py-4 pl-5 bg-primary" style="border-radius: 10px 0 0 10px;">
							<h6 class="text-white font-light">ROLE</h6>
							<h4 class="text-white font-bold"><?= strtoupper($this->session->userdata('role')) ?></h4>
					 	</div>
					 	<div class="col-6 col-sm-4 col-md-4  bg-white py-auto text-center py-4 px-0" style="border-radius: 0 10px 10px 0; border: 1px solid #007bff">
					 		<i class="fas fa-user fa-3x text-primary mt-2"></i>
					 	</div>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 px-4 mt-2"> 
				<div class="bg-primary">
					<div class="row">
						<div class="col-6 col-sm-8 col-md-8 py-4 pl-5 bg-success" style="border-radius: 10px 0 0 10px;">
							<h6 class="text-white font-light">DAYA</h6>
							<h4 class="text-white font-bold"><?= ( empty($this->session->userdata('id_tarif')) ? '#' : strtoupper($this->session->userdata('id_tarif'))  ) ?></h4>
					 	</div>
					 	<div class="col-6 col-sm-4 col-md-4  bg-white py-auto text-center py-4 px-0" style="border-radius: 0 10px 10px 0; border: 1px solid #28a745">
					 		<i class="fas fa-bolt fa-3x text-success mt-2"></i>
					 	</div>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 px-4 mt-2"> 
				<div class="bg-primary">
					<div class="row">
						<div class="col-6 col-sm-8 col-md-8 py-4 pl-5 bg-info" style="border-radius: 10px 0 0 10px;">
							<h6 class="text-white font-light">USER</h6>
							<h4 class="text-white font-bold"><?= $this->session->userdata('username') ?></h4>
					 	</div>
					 	<div class="col-6 col-sm-4 col-md-4  bg-white py-auto text-center py-4 px-0" style="border-radius: 0 10px 10px 0; border: 1px solid #17a2b8">
					 		<i class="fas fa-people-carry fa-3x text-info mt-2"></i>
					 	</div>
					</div>
				</div>
			</div>

			<div class="col-sm-6 col-md-6 col-lg-3 col-xl-3 px-4 mt-2"> 
				<div class="bg-primary">
					<div class="row">
						<div class="col-6 col-sm-8 col-md-8 py-4 pl-5 bg-secondary" style="border-radius: 10px 0 0 10px;">
							<h6 class="text-white font-light">NO KWH</h6>
							<h4 class="text-white font-bold"><?= ( empty($this->session->userdata('nomor_kwh')) ? '#' : strtoupper($this->session->userdata('nomor_kwh'))  ) ?></h4>
					 	</div>
					 	<div class="col-6 col-sm-4 col-md-4  bg-white py-auto text-center py-4 px-0" style="border-radius: 0 10px 10px 0; border: 1px solid #6c757d">
					 		<i class="fas fa-file-invoice fa-3x text-secondary mt-2"></i>
					 	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="bg-white pt-2 p-2 mb-0 pt-5">
		<div class="row" id="event">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 pr-4 pl-5 mt-4"> 
				<nav>
				  	<div class="nav nav-tabs" id="nav-tab" role="tablist">
					    <?php 
						if( $this->session->userdata('role') == 'admin' ){ ?>
					    	<a class="nav-item nav-link active" id="nav-penggunaan-tab" data-toggle="tab" href="#nav-penggunaan" role="tab" aria-controls="nav-penggunaan" aria-selected="true"><i class="fas fa-user"></i> Penggunaan</a>
					    	<?php 
					    }
					    if( $this->session->userdata('role') == 'admin' || $this->session->userdata('role') == 'pelanggan' ){ ?>
					    	<a class="nav-item nav-link " id="nav-tagihan-tab" data-toggle="tab" href="#nav-tagihan" role="tab" aria-controls="nav-tagihan" aria-selected="false"><i class="fas fa-file-invoice"></i> Tagihan</a>
					    	<?php 
					    }
					    if( $this->session->userdata('role') == 'admin' ){ ?>
					    	<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-pembayaran" role="tab" aria-controls="nav-pembayaran" aria-selected="false"><i class="fas fa-money-bill-wave"></i> Pembayaran</a>
					    	<?php 
					    }
					    ?>
				  	</div>
				</nav>
				<div class="tab-content" id="nav-tabContent">
				  	<div class="tab-pane fade  mt-5" id="nav-penggunaan" role="tabpanel" aria-labelledby="nav-penggunaan-tab">
				  		
				  		<div class="row">
				  			<div class="col-md-4 ml-auto mb-5">
				  				<form method="post" id="form-penggunaan" name="form_penggunaan">
					  				<table  width="100%">
					  					<tr>
					  						<input type="hidden" name="id_penggunaan" id="id-penggunaan">
					  						<td width="30%" class="font-light">Nama Pelanggan</td>
					  						<td>
					  							<select class="form-control form-control-sm select2" name="id_pelanggan" id="id-pelanggan-penggunaan" required>
					  								<option value="">- Pilih Pelanggan -</option>
					  								<?php 
					  								foreach ($data_pelanggan as $index => $column) {
					  									?><option value="<?= $column->id_pelanggan ?>"><?= $column->nama_pelanggan ?></option><?php	
					  								}
					  								?>
					  							</select>
					  						</td>
					  					</tr>
					  					<tr>
					  						<td width="30%" class="font-light">Bulan</td>
					  						<td>
					  							<select class="form-control col-6 form-control-sm select2" name="bulan" id="bulan-penggunaan" required>
					  								<option value="">- Pilih Bulan -</option>
					  								<option value="01">Januari</option>
					  								<option value="02">Febuari</option>
					  								<option value="03">Maret</option>
					  								<option value="04">April</option>
					  								<option value="05">Mei</option>
					  								<option value="06">Juni</option>
					  								<option value="07">Juli</option>
					  								<option value="08">Agustus</option>
					  								<option value="09">September</option>
					  								<option value="10">Oktober</option>
					  								<option value="11">November</option>
					  								<option value="12">Desember</option>
					  								<option></option>
					  							</select>
					  						</td>
					  					</tr>
					  					<tr>
					  						<td width="30%" class="font-light">Tahun</td>
					  						<td>
					  							<select class="form-control col-6 form-control-sm select2" name="tahun" id="tahun-penggunaan" required>
					  								<option value="">- Pilih Tahun -</option>
					  								<option value="2021">2021</option>
					  								<option value="2022">2022</option>
					  								<option value="2023">2023</option>
					  							</select>
					  						</td>
					  					</tr>
					  					<tr>
					  						<td width="30%" class="font-light">Kwh Awal</td>
					  						<td>
					  							<input type="text" class="form-control col-6 form-control-sm" name="kwh_awal" id="kwh-awal-penggunaan" required>
					  						</td>
					  					</tr>
					  					<tr>
					  						<td width="30%" class="font-light">Kwh Akhir</td>
					  						<td>
					  							<input type="text" class="form-control col-6 form-control-sm" name="kwh_akhir" id="kwh-akhir-penggunaan" required>
					  						</td>
					  					</tr>
					  					<tr>
					  						<td width="30%" class="font-light"></td>
					  						<td>
					  							<button type="submit" class="btn btn-sm btn-success">Simpan</button>
					  						</td>
					  					</tr>
					  				</table>
					  			</form>
				  			</div>
				  		</div>
				  		<div class="row">
				  			<div class="col-md-12">
						  		<table class="table table-sm table-bordered mt-5" id="table-penggunaan">
						  			<thead>
						  				<tr class="bg-primary">
						  					<td class="text-white">#</td>
						  					<td class="text-white">Id Penggunaan</td>
						  					<td class="text-white">Id Pelanggan</td>
						  					<td class="text-white">Nama Pelanggan</td>
						  					<td class="text-white">Nomor Kwh</td>
						  					<td class="text-white">Alamat</td>
						  					<td class="text-white">Daya</td>
						  					<td class="text-white">Bulan</td>
						  					<td class="text-white">Tahun</td>
						  					<td class="text-white">Meter Awal</td>
						  					<td class="text-white">Meter Akhir</td>
						  				</tr>
						  			</thead>
						  		</table>
				  			</div>
				  		</div>
				  	</div>
				  	<div class="tab-pane fade show active" id="nav-tagihan" role="tabpanel" aria-labelledby="nav-tagihan-tab">
				  		<div class="row">
				  			<div class="col-md-12 pt-5">
						  		<table class="table table-sm table-bordered mt-5" id="table-tagihan">
						  			<thead>
						  				<tr class="bg-primary">
						  					<td class="text-white" style="width: 80px !important;">#</td>
						  					<td class="text-white" style="width: 100px !important;">Id Tagihan</td>
						  					<td class="text-white" style="width: 200px !important;">Nama Pelanggan</td>
						  					<td class="text-white" style="width: 100px !important;">Nomor Kwh</td>
						  					<td class="text-white" style="width: 100px !important;">Daya</td>
						  					<td class="text-white" style="width: 100px !important;">Bulan</td>
						  					<td class="text-white" style="width: 100px !important;">Tahun</td>
						  					<td class="text-white" style="width: 100px !important;">Tarif/Kwh</td>
						  					<td class="text-white" style="width: 100px !important;">Jumlah Meter</td>
						  					<td class="text-white" style="width: 150px !important;">Status</td>
						  				</tr>
						  			</thead>
						  		</table>
				  			</div>
				  		</div>
				  	</div>
				  	<div class="tab-pane fade" id="nav-pembayaran" role="tabpanel" aria-labelledby="nav-contact-tab">
				  		<div class="row">
				  			<div class="col-md-4 ml-auto mt-5">
				  				<form method="post" id="form-pembayaran" name="form_pembayaran">
					  				<table  width="100%">
					  					<tr>
					  						<input type="hidden" name="id_penggunaan_pembayaran" id="id-penggunaan-pembayaran">
					  						<input type="hidden" name="id_tagihan_penggunaan" id="id-tagihan-penggunaan">
					  						<input type="hidden" name="bulan_penggunaan_pembayaran" id="bulan-penggunaan-pembayaran">
					  						<input type="hidden" name="id_pelanggan_pembayaran" id="id-pelanggan-pembayaran">
					  					</tr>
					  					<!-- <tr>
					  						<td width="30%" class="font-light">Tahun</td>
					  						<td>
					  							<input type="text" readonly class="form-control col-6 form-control-sm" name="tahun-pembayaran" id="tahun-pembayaran" required>
					  						</td>
					  					</tr> -->
					  					<tr>
					  						<td width="30%" class="font-light">Biaya Admin</td>
					  						<td>
					  							<input type="number" class="form-control col-6 form-control-sm" name="total_biaya_admin" id="total-biaya-admin" required>
					  						</td>
					  					</tr>
					  					<tr>
					  						<td width="30%" class="font-light">Total</td>
					  						<td>
					  							<input type="number" class="form-control col-6 form-control-sm" name="total_pembayaran" id="total_pembayaran" required>
					  						</td>
					  					</tr>
					  					<tr>
					  						<td width="30%" class="font-light"></td>
					  						<td>
					  							<button type="submit" class="btn btn-sm btn-success">Simpan</button>
					  						</td>
					  					</tr>
					  				</table>
					  			</form>
				  			</div>
				  		</div>
				  		<div class="row">
				  			<div class="col-md-12">
						  		<table class="table table-sm table-bordered mt-5" id="table-pembayaran">
						  			<thead>
						  				<tr class="bg-primary">
						  					<td class="text-white">#</td>
						  					<td class="text-white">Id Penggunaan</td>
						  					<td class="text-white">Id Pelanggan</td>
						  					<td class="text-white">Nama Pelanggan</td>
						  					<td class="text-white">Nomor Kwh</td>
						  					<td class="text-white">Alamat</td>
						  					<td class="text-white">Daya</td>
						  					<td class="text-white">Bulan</td>
						  					<td class="text-white">Tahun</td>
						  					<td class="text-white">Jumlah Meter</td>
						  					<td class="text-white">Total Bayar</td>
						  					<td class="text-white">Tanggal Bayar</td>
						  					<td class="text-white">status</td>
						  				</tr>
						  			</thead>
						  		</table>
				  			</div>
				  		</div>
				  	</div>
				</div>
			</div>
		</div>
	</div>
	<?php 
}
else{
	?>

		<form method="post" id="form-verifikasi" name="form_verifikasi" enctype="multipart/form-data">
			<div class="content-main row ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2 mt-5">
				<div class="col-xl-3 col-lg-5 col-md-6 col-sm-8 ml-auto mr-auto text-center bg-primary">
					<div class="row align-items-center">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-2 pt-4">
							<i class="fas fa-bolt fa-4x text-white"></i>
						</div>
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-2">
							<h6 class="text-center text-white font-bold">PLN PASCABAYAR</h6>
						</div>
					</div>
				</div>
			</div>
			<div class="content-main bg-dark row ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
				<div class="col-xl-3 col-lg-5 col-md-6 col-sm-8 ml-auto mr-auto bg-white">
					<div class="row">
						<div class="col-md-12 col-lg-12 p-2 pt-5 pt-1 ml-auto mr-auto">
						    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12">
						    	<div class="input-group input-group-md">
						        	<input type="text" class="form-control form-biodata" id="username" name="username" maxlength="20" placeholder="USERNAME" value="">
						      	</div>
						    </div>
						    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 ">
						    	<div class="input-group input-group-md">
						        	<input type="password" readonly class="form-control form-biodata" id="password" name="password" maxlength="15" placeholder="PASSWORD" value="" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" value="">
						      	</div>
						    </div>
						</div>
					
					</div>
					<div class="col-sm-12 col-md-12 col-lg-12 bg-white pb-5 mb-5 mt-5">
						<div class="row align-items-center">
							<div class="col"></div>
							<div class="col-xl-12 col-lg-12 col-md-12 text-center">
								<button id="btn-login" type="button" class="btn bg-primary text-white p-2 w-100"><i class="fas fa-key fa-1x"></i> LOGIN </button>
							</div>
							<div class="col"></div>
						</div>
					</div>
				</div>
			</div>
		</form>
	<?php 
}
?>

<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<script src="<?php echo base_url('js_app/landing_page.js');?>"></script>
<link href="<?php echo base_url('css_app/landing_page.css');?>" rel="stylesheet" type="text/css">


