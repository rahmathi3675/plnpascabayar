<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PAYMENT REQUEST PT. TACO</title>
	<link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="<?php echo base_url('/assets/images/icon.png');?>" type="image/x-icon"/>

	<!-- Global stylesheets -->
	<link href="<?php echo base_url('/assets/template/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('/assets/vendors/css/icons/icomoon/styles.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('/assets/template/css/core.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('/assets/template/css/components.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('/assets/template/css/colors.min.css');?>" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url('/assets/vendors/js/plugins/loaders/pace.min.js');?>"></script>
	<script src="<?php echo base_url('/assets/vendors/js/core/libraries/jquery.min.js');?>"></script>
	<script src="<?php echo base_url('/assets/vendors/js/plugins/loaders/blockui.min.js');?>"></script>
	<script src="<?php echo base_url('/assets/vendors/js/core/libraries/bootstrap.min.js');?>"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?php echo base_url('/assets/template/js/app.js');?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js');?>"></script>
</head>

<body class="login-container" style="background-image:url(<?php echo base_url('assets/images/ar3.jpg')?>); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size:cover; background-position:bottom;">

	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content">
					<div class="panel panel-body login-form" style="opacity: 0.9;filter: alpha(opacity=50);">
						<form method="post" id="form_login_a" action="<?php echo base_url('Login');?>">
							<input type="hidden" name="company_code" value="<?= $company_code ?>">
							<div class="text-center">
								<img src="<?php echo base_url('assets/images/'.$company_code.'.png'); ?>" style="max-width: 100px; width: 100px; max-height: 100px; height: 50px;">
								<h3 class="content-group">Login Page</h3>
							</div>
							<?php 
							if(isset($auth['error'])){ 
								if ( $auth['error'] == 1 ){ ?>
									<div class="alert alert-danger no-border">
										<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
										<span class="text-semibold"><?php echo $auth['message'];?></span>
									</div>
									<?php 
								}
							} ?>

							<?php if(isset($auth['success'])){ ?>
							<div class="alert alert-success no-border">
								<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
								<span class="text-semibold"><?php echo $auth['message'];?></span>
							</div>
							<?php } ?>

							<div class="form-group has-feedback has-feedback-left">
								<input type="text" class="form-control" name="username" id="username"  placeholder="Enter Username">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" name="password" id="password" placeholder="Enter Password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<div class="checkbox">
									<input id="remember" name="remember" type="checkbox">
									<label for="remember"> Remember me</label>
								</div>
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Login <i class="icon-circle-right2 position-right"></i></button>
							</div>
							<div class="form-group">
								<a style="padding-top: 20px !important" class="mt-sm" href="<?= base_url('login/forgot_password') ?>">Forgot Password</a>
							</div>

							<!-- <a href="#"><img src="<?= base_url('assets/images/smm.png') ?>" alt="Smm" style="max-width: 50px; max-height: 50px;"></a> -->
						</form>
						<!-- <form method="post" action="<?php echo base_url('login');?>" style="display: inline" class="form-company">
							<input type="hidden" name="company_code" value="fio">
							<a href="#" class="form-company-button">
								<img src="<?= base_url('assets/images/fio.png') ?>" alt="Fio" style="max-width: 50px; max-height: 50px;">
							</a>
						</form>
						<form method="post" action="<?php echo base_url('login');?>" style="display: inline" class="form-company">
							<input type="hidden" name="company_code" value="bic">
							<a href="#" class="form-company-button">
								<img src="<?= base_url('assets/images/bic.png') ?>" alt="Bic" style="max-width: 50px; max-height: 50px;">
							</a>
						</form>
						<form method="post" action="<?php echo base_url('login');?>" style="display: inline" class="form-company">
							<input type="hidden" name="company_code" value="tac">
							<a href="#" class="form-company-button">
								<img src="<?= base_url('assets/images/tac.png') ?>" alt="Tac" style="max-width: 50px; max-height: 50px;">
							</a>
						</form>
						<form method="post" action="<?php echo base_url('login');?>" style="display: inline" class="form-company">
							<input type="hidden" name="company_code" value="taco">
							<a href="#" class="form-company-button">
								<img src="<?= base_url('assets/images/taco.png') ?>" alt="Taco" style="max-width: 50px; max-height: 50px;">
							</a>
						</form> -->
					</div>

					
									
					<div class="footer text-muted text-center">
						&copy; 2018. <a href="https://www.taco.co.id/" target="_blank">PT. TACO</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">

$(document).ready(function(){

	$('.form-company-button').on('click', function(){
		$(this).closest('.form-company').submit();
	});
})
</script>
