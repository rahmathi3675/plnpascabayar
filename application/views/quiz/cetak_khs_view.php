<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">

	<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3"></div>
	<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 pb-5 
		pr-xl-5 pt-xl-5 pl-xl-0 
		pr-lg-5 pt-lg-5 pl-lg-0 
		pr-md-5 pt-md-5 pl-md-0
		pr-sm-4 pt-sm-0 pl-sm-4
		
		">
		<hr class="bg-nu">
		<h5 class="font-bold text-center bg-info pt-2 ">KARTU HASIL STUDY</h5>
		<hr class="bg-nu">
		<div class="row mt-3">
			<div class="col-lg-6 col-md-6 col-sm-12 mb-3  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-right font-light mb-0">KODE KHS</p>
	        	<p class="text-right font-regular mb-0"><b><?= $data_header->kode_hasil ?></b></p>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-2  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-left font-light mb-0">NO PENDAFTARAN</p>
	        	<p class="text-left font-regular mb-0"><b><?= $data_header->no_pendaftaran ?></b></p>
		    </div>
		</div>
		<div class="row mt-1">
			<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-1 ">
	    		<p class="text-center mb-0 font-light">DESKRIPSI</p>
	        	<p class="text-center font-regular"><b><?= $data_header->deskripsi_acara ?></b></p>
	      	</div>
		</div>
		<div class="row mt-1">
			<div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-right font-light mb-0">HASIL</p>
	        	<p class="text-right font-regular mb-0"><b><?= $data_header->hasil_akhir ?></b></p>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-left font-light mb-0">SOAL</p>
	        	<p class="text-left font-regular mb-0"><b><?= $data_header->jumlah_materi ?></b></p>
		    </div>
		</div>
		
	</div>
	<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3"></div>
	<div class="col-md-1"></div>
	<div class="col-md-10 p-5">
		<table class="table table-md table-bordered" width="100%" border="1">
			<thead class="bg-light">
				<tr >
					<td>Kode Materi</td>
					<td>Soal</td>
					<td>Jawaban</td>
					<td>Hasil</td>
				</tr>
			</thead>
				
				<?php
				foreach ($data_detail as $index => $column) {
					?>
					<tr>
						<td><?= $column->kode_materi ?></td>
						<td><?= $column->deskripsi_materi ?></td>
						<td><?= '('.$column->value.') '.$column->deskripsi_jawaban ?></td>
						<td><?= $column->hasil ?></td>
					</tr>
					<?php
				}
				?>
		</table>
		<button id="btn-cetak" class="btn btn-primary"><i class="fas fa-print"></i> Cetak</button>
	</div>
	<div class="col-md-1"></div>
</div>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/kegiatan.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/kegiatan.js');?>"></script>
