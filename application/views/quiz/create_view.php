<?php echo form_open_multipart('kegiatan/save'); ?>
<form method="post" id="form-create-kegiatan" name="form_create_kegiatan">

<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 pt-5 pl-5 pr-5 pb-0">
		<nav aria-label="breadcrumb">
		  	<ol class="breadcrumb bg-nu">
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url() ?>"><i class="fas fa-home"></i> Home</a></li>
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url('kegiatan') ?>"><i class="fas fa-list"></i> List Kegiatan</a></li>
		    	<li class="breadcrumb-item  text-white active" aria-current="page"><i class="fas fa-solar-panel"></i> Create</li>
		  	</ol>
		</nav>
	</div>
	<div class="col-sm-12 col-md-8 col-lg-6 col-xl-6 p-5" >
			
		<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<select class="form-control form-biodata bg-white" id="kategori-kegiatan" name="kategori_kegiatan"  required>
	        		<option value="">- PILIH KATGEORI -</option>
	        		<?php
	        		foreach ($data_kategori as $index => $column) {
	        			?>
	        			<option value="<?= $column->kode ?>"><?= strtoupper($column->nama_kategori) ?></option>
	        			<?php 
	        		}
	        		?>
	        	</select>
	      	</div>
	    </div>
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<select class="form-control form-biodata bg-white" id="jenis-kegiatan" name="jenis_kegiatan" required>
	        		<option value="">- PILIH JENIS -</option>
	        		<?php
	        		foreach ($data_jenis as $index => $column) {
	        			?>
	        			<option value="<?= $column->kode ?>"><?= strtoupper($column->nama_jenis) ?></option>
	        			<?php 
	        		}
	        		?>
	        	</select>
	      	</div>
	    </div>		
	</div>
	<div class="col-sm-12 col-md-4 col-lg-6 col-xl-6 p-5">
		<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<input type="text" class="form-control form-biodata bg-white" name="nama_kegiatan" placeholder="NAMA KEGIATAN" value="TEST KEGIATAN" maxlength="200" required>
	      	</div>
	    </div>
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<textarea class="form-control form-biodata bg-white" name="deskripsi" placeholder="DESKRIPSI" rows="3" value="" required>SAMPLE</textarea>
	      	</div>
	    </div>
	    <div class="form-group col-lg-6 col-md-6 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	    		<input type="text" readonly class="form-control form-biodata datetimepicker bg-white" name="tanggal_kegiatan" placeholder="TANGGAL KEGIATAN" value="2021-12-29" required>
	      	</div>
	    </div>
	    <div class="form-group col-lg-4 col-md-4 col-sm-6 mb-3 ">
	    	<div class="input-group input-group-md">
	    		<input type="time"  class="form-control form-biodata bg-white" name="waktu_kegiatan" placeholder="" value="10:00" required>
	      	</div>
	    </div>

	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 p-5">
		<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 mt-5">
	    	<button class="btn btn-md btn-success"><i class="fas fa-save"></i> SAVE</button>
	    </div>
	</div>
</div>
	</form>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/kegiatan.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/kegiatan.js');?>"></script>
