<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">


	<div class="col-sm-12 col-md-8 col-lg-8 col-xl-8 
		pb-5 
		pr-xl-3 pt-xl-5 pl-xl-3 
		pr-lg-3 pt-lg-5 pl-lg-3 
		pr-md-3 pt-md-5 pl-md-3
		pr-sm-3 pt-sm-0 pl-sm-3
		">
		<h5 class="font-bold text-center bg-info pt-2 pb-2 text-white" style="border-radius: 5px;">LEARNING</h5>

		<form>
			<input type="hidden" readonly name="no_pendaftaran" id="no-pendaftaran" value="<?= $data_header->no_pendaftaran ?>">
			<input type="hidden" readonly name="kode_materi" id="kode-materi" value="">
			<input type="hidden" readonly name="kode_acara" id="kode-acara" value="<?= $data_header->kode_acara ?>">
			<input type="hidden" readonly name="kode_khs" id="kode-khs" value="<?= $data_header->kode_hasil ?>">
			<input type="hidden" readonly  id="kode-khs-no" value="<?= str_replace('/','',$data_header->kode_hasil ) ?>">
			<input type="hidden" readonly id="tanggal-selesai" value="<?= $data_header->tanggal_selesai ?>">

			<div class="row">
				<div class="col-md-12 p-4" id="current-task">
					<div class="col-lg-12 col-md-12 col-sm-12 mt-0 mb-3" id="task-header">
						<!-- <p>
							<span>( 2 )</span> 
					        <span class="bg-white font-light ml-3" rows="3" placeholder="DESKRIPSI" name="deskripsi_materi">Sample Soal () Dengan Jawaban E</span>
				        </p> -->
				    </div>
				    <div class="col-lg-12 col-md-12 col-sm-12 mt-0 mb-5">
		  				<div class="row" id="task-body">
		  					<!-- <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ml-5">
				  				<label for="id-jawaban">
						    		<input id="id-jawaban" type="radio" name="is_key" value="0" required> 
						    		<span class="ml-3">Sample Jawaban A</span>
						    	</label>
						    </div> -->
						</div>
						
				    </div>
				</div>
				<div class="col-md-12 p-4 text-center" id="task-footer">
					<button type="button" style="display: none;" id="btn-prev" class="btn btn-primary font-light"><i class="fas fa-angle-left"></i> Prev</button>
					<button type="button" style="display: none;" id="btn-next" class="btn btn-primary font-light">Next <i class="fas fa-angle-right"></i></button>
					<button type="button" style="display: none;" id="btn-selesai" class="btn btn-success font-light"><i class="fas fa-save"></i> Save And Review</button>
					<button type="button" style="display: none;" id="btn-submit" class="btn btn-success font-light"><i class="fas fa-save"></i> Finish Quiz</button>
				</div>
			</div>
		</form>
	</div>
	<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4
		pb-5 
		pr-xl-3 pt-xl-5 pl-xl-3 
		pr-lg-3 pt-lg-5 pl-lg-3 
		pr-md-3 pt-md-5 pl-md-3
		pr-sm-3 pt-sm-0 pl-sm-3
		">
		<h5 class="font-bold text-center bg-info pt-2 pb-2 text-white" style="border-radius: 5px;">PANEL </h5>

		
		<div class="row">
			<div class="col-md-12 p-3 text-center">
				<p id="demo" class="bg-primary mx-5" style="border-radius: 2px"></p>
			</div>
			<div class="col-md-12 p-3 text-center">
				<div class="row justify-content-xl-center" id="panel-jawaban">
					<?php 
					/*for ($i=1; $i <= 40; $i++) { 
						?>
							<div class="div-task-number col-xl-1 col-lg-1 col-md-1 col-sm-1 text-center
								pr-xl-1 pt-xl-1 pl-xl-1 pb-xl-2 m-1
								pr-lg-3 pt-lg-5 pl-lg-3 
								pr-md-3 pt-md-5 pl-md-3
								pr-sm-3 pt-sm-0 pl-sm-3
								div-task-incomplete task-incomplete
								">
								<form class="form-task-number" method="post">
									<input type="hidden" class="task-code">
									<input type="hidden" class="selected_jawaban">
									<input type="hidden" class="is_current_position">
									<span class="font-light"><?= $i ?></span>
				    			</form>
					    	</div>
				    	<?php 
				    }*/
				    ?>
				</div>
			</div>
		</div>

		<h5 class="font-bold text-center bg-info pt-2 pb-2 text-white" style="border-radius: 5px;">INFORMATION </h5>

		<div class="row mt-3">
			<div class="col-lg-6 col-md-6 col-sm-12 mb-3  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-right font-light mb-0">No Pendaftaran</p>
	        	<p class="text-right font-regular mb-0"><b><?= $data_header->no_pendaftaran ?></b></p>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-2  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-left font-light mb-0">Kode Acara</p>
	        	<p class="text-left font-regular mb-0"><b><?= $data_header->kode_acara ?></b></p>
		    </div>
		</div>
		<div class="row mt-1">
			<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-1 ">
	    		<p class="text-center mb-0 font-light">Nama Kegiatan</p>
	        	<p class="text-center font-regular"><b><?= $data_header->nama_kegiatan ?></b></p>
	      	</div>

		    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-1">
	    		<p class="text-center mb-0 font-light">Deksripsi Acara</p>
	        	<p class="text-center font-regular mb-0"><b><?= $data_header->deskripsi_acara ?></b></p>
	      	</div>
		</div>
		<div class="row mt-1">
			<div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-right font-light mb-0">JENIS</p>
	        	<p class="text-right font-regular mb-0"><b><?= $data_header->nama_jenis ?></b></p>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-left font-light mb-0">KATEGORI</p>
	        	<p class="text-left font-regular mb-0"><b><?= $data_header->nama_kategori ?></b></p>
		    </div>
		</div>
		<!-- <div class="row mt-1">
			<div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-right font-light mb-0">TGL MULAI</p>
	        	<p class="text-right font-regular"><b><?= $data_header->tanggal_mulai ?></b></p>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-left font-light mb-0">TGL SELESAI</p>
	        	<p class="text-left font-regular"><b><?= $data_header->tanggal_selesai ?></b></p>
		    </div>
		</div> -->
	</div>

</div>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/elearning.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/elearning.js');?>"></script>
