<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 pt-5 pl-5 pr-5 pb-0">
		<nav aria-label="breadcrumb">
		  	<ol class="breadcrumb bg-nu">
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url() ?>"><i class="fas fa-home"></i> Home</a></li>
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url('quiz') ?>"><i class="fas fa-list"></i> List Quiz</a></li>
		    	<li class="breadcrumb-item  text-white active" aria-current="page"><i class="fas fa-solar-panel"></i> Update</li>
		  	</ol>
		</nav>
	</div>
	<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 pl-5 pr-5 pt-1" >


		<div class="form-group col-lg-6 col-md-8 col-sm-12 mb-3  mt-0 mt-md-2 mt-lg-2">
	    	<div class="input-group input-group-md">
	        	<input type="text" class="form-control form-biodata bg-white" placeholder="NAMA KEGIATAN" maxlength="200" disabled value="<?= $data_header->kode_kegiatan ?>">
	      	</div>
	    </div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<input type="text" class="form-control form-biodata bg-white" value="<?= strtoupper($data_kategori->nama_kategori) ?>" disabled>
	        	</select>
	      	</div>
	    </div>
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<input type="text" class="form-control form-biodata bg-white" value="<?= strtoupper($data_jenis->nama_jenis) ?>" disabled>
	        	</select>
	      	</div>
	    </div>		
		<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<input type="text" class="form-control form-biodata bg-white"  maxlength="200" required value="<?= $data_header->nama_kegiatan ?>" disabled>
	      	</div>
	    </div>
	</div>
	<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 
		pr-xl-5 pt-xl-2 pl-xl-0 
		pr-lg-5 pt-lg-2 pl-lg-0 
		pr-md-5 pt-md-2 pl-md-0
		pr-sm-4 pt-sm-0 pl-sm-4
		">
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 mt-2 ">
	    	<div class="input-group input-group-md">
	        	<textarea class="form-control form-biodata bg-white" placeholder="DESKRIPSI"  required><?= $data_header->deskripsi ?></textarea>
	      	</div>
	    </div>
	    <div class="form-group col-lg-6 col-md-6 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	    		<input type="text" readonly class="form-control form-biodata datetimepicker bg-white" name="tanggal_kegiatan" placeholder="TANGGAL KEGIATAN" required value="<?= $data_header->tanggal_kegiatan ?>">
	      	</div>
	    </div>
	    <div class="form-group col-lg-4 col-md-4 col-sm-6 mb-3 ">
	    	<div class="input-group input-group-md">
	    		<input type="time"  class="form-control form-biodata bg-white" name="waktu_kegiatan" placeholder="" required value="<?= $data_header->waktu_kegiatan ?>">
	      	</div>
	    </div>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 pl-5 pr-5 mt-5">
		<h6 class="bg-nu p-1 text-white text-center">MATERI</h6>	
	</div>
	<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 pl-5 pr-5 mt-3">
		<div class="table-responsive mt-1 mb-5">
			<table id="table-materi-quiz" class="table table-sm table-bordered">
				<thead class="bg-light">
					<tr>
						<td>KODE</td>
						<td>DESKRIPSI</td>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 pl-5 pr-5 mt-3 mb-5">
		<form method="post" id="form-create-quiz" name="form_create_quiz">
			<input readonly type="hidden" style="display: none;" id="id-anggota" name="id_anggota" value="<?= rand(666666666666666,888888888888888).$data_header->id.rand(666666666666666,999999999999999) ?>">
			<input readonly type="hidden" name="kode_kegiatan" id="kode-kegiatan" readonly maxlength="100" value="<?= $data_header->kode_kegiatan ?>" style="display: none;">
			<div class="form-group form-group-add col-lg-12 col-md-12 col-sm-12">
		    	<div class="input-group input-group-md">
		        	<textarea class="form-control form-biodata bg-white" rows="3" placeholder="DESKRIPSI" name="deskripsi_materi"  required>Sample Soal () Dengan Jawaban E</textarea>
		      	</div>
		    </div>
		    <div class="form-group form-group-add col-lg-12 col-md-12 col-sm-12 mt-5 mb-5">
		    	<div class="input-group input-group-md mb-3">
				  	<div class="input-group-prepend">
				    	<input type="radio" name="is_key" value="0" style="display: -webkit-box; margin-top: 15px !important;" required>
				  	</div>
				  	<div class="input-group-prepend">
				    	<i class="p-2">( A )</i> &nbsp; 
				  	</div>
				  	<input type="text" class="form-control form-biodata" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban[]" required value="Sample Jawaban A">
				</div>
				<div class="input-group input-group-md mb-3">
					<div class="input-group-prepend">
				    	<input type="radio" name="is_key" value="1" style="display: -webkit-box; margin-top: 15px !important;" required>
				  	</div>
				  	<div class="input-group-prepend">
				    	<i class="p-2">( B ) </i>&nbsp; 
				  	</div>
				  	<input type="text" class="form-control form-biodata" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban[]" required value="Sample Jawaban B">
				</div>
				<div class="input-group input-group-md mb-3">
					<div class="input-group-prepend">
				    	<input type="radio" name="is_key" value="2" style="display: -webkit-box; margin-top: 15px !important;" required>
				  	</div>
				  	<div class="input-group-prepend">
				    	<i class="p-2">( C ) </i>&nbsp; 
				  	</div>
				  	<input type="text" class="form-control form-biodata" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban[]" required value="Sample Jawaban C">
				</div>
				<div class="input-group input-group-md mb-3">
					<div class="input-group-prepend">
				    	<input type="radio" name="is_key" value="3" style="display: -webkit-box; margin-top: 15px !important;" required>
				  	</div>
				  	<div class="input-group-prepend">
				    	<i class="p-2">( D ) </i>&nbsp; 
				  	</div>
				  	<input type="text" class="form-control form-biodata" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban[]" required value="Sample Jawaban D">
				</div>
				<div class="input-group input-group-md mb-3">
					<div class="input-group-prepend">
				    	<input type="radio" name="is_key" value="4" style="display: -webkit-box; margin-top: 15px !important;" required>
				  	</div>
				  	<div class="input-group-prepend">
				    	<i class="p-2">( E ) </i>&nbsp; 
				  	</div>
				  	<input type="text" class="form-control form-biodata" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban[]" required value="Sample Jawaban E">
				</div>
				<div class="input-group input-group-md mb-3 mt-5">
					<button type="submit" class="btn btn-primary btn-block"><i class="fas fa-chevron-left"></i> &nbsp; &nbsp; TAMBAHKAN</button>
				</div>
		    </div>
		</form>
		<form method="post" id="form-edit-quiz" name="form_update_quiz" style="display: none;">
		    <div class="form-group form-group-edit col-lg-12 col-md-12 col-sm-12">
		    	<div class="input-group input-group-md">
		    		<input readonly type="hidden" name="kode_kegiatan" id="kode-kegiatan" readonly maxlength="100" value="<?= $data_header->kode_kegiatan ?>" style="display: none;">
		    		<input type="hidden" name="kode_materi" id="kode-materi">
		        	<textarea class="form-control form-biodata bg-white" rows="3" placeholder="DESKRIPSI" name="deskripsi_materi" id="deskripsi-materi" required>Sample Soal () Dengan Jawaban E</textarea>
		      	</div>
		    </div>
		    <div class="form-group form-group-edit col-lg-12 col-md-12 col-sm-12 mt-5 mb-5" id="form-jawaban">
		    	<div class="input-group input-group-md mb-3">
				  	<div class="input-group-prepend">
				    	<input type="radio" name="is_key" value="0" style="display: -webkit-box; margin-top: 15px !important;" required>
				  	</div>
				  	<div class="input-group-prepend">
				    	<i class="p-2">( A )</i> &nbsp; 
				  	</div>
				  	<input type="text" class="form-control form-biodata jawaban_edit" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban_edit[]" required value="Sample Jawaban A">
				</div>
				<div class="input-group input-group-md mb-3">
					<div class="input-group-prepend">
				    	<input type="radio" name="is_key" value="1" style="display: -webkit-box; margin-top: 15px !important;" required>
				  	</div>
				  	<div class="input-group-prepend">
				    	<i class="p-2">( B ) </i>&nbsp; 
				  	</div>
				  	<input type="text" class="form-control form-biodata jawaban_edit" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban_edit[]" required value="Sample Jawaban B">
				</div>
				<div class="input-group input-group-md mb-3">
					<div class="input-group-prepend">
				    	<input type="radio" name="is_key" value="2" style="display: -webkit-box; margin-top: 15px !important;" required>
				  	</div>
				  	<div class="input-group-prepend">
				    	<i class="p-2">( C ) </i>&nbsp; 
				  	</div>
				  	<input type="text" class="form-control form-biodata jawaban_edit" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban_edit[]" required value="Sample Jawaban C">
				</div>
				<div class="input-group input-group-md mb-3">
					<div class="input-group-prepend">
				    	<input type="radio" name="is_key" value="3" style="display: -webkit-box; margin-top: 15px !important;" required>
				  	</div>
				  	<div class="input-group-prepend">
				    	<i class="p-2">( D ) </i>&nbsp; 
				  	</div>
				  	<input type="text" class="form-control form-biodata jawaban_edit" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban_edit[]" required value="Sample Jawaban D">
				</div>
				<div class="input-group input-group-md mb-3">
					<div class="input-group-prepend">
				    	<input type="radio" name="is_key" value="4" style="display: -webkit-box; margin-top: 15px !important;" required>
				  	</div>
				  	<div class="input-group-prepend">
				    	<i class="p-2">( E ) </i>&nbsp; 
				  	</div>
				  	<input type="text" class="form-control form-biodata jawaban_edit" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban_edit[]" required value="Sample Jawaban E">
				</div>
			</div>
			<div class="form-group form-group-edit col-lg-12 col-md-12 col-sm-12 mt-5 mb-5">
				<div class="input-group input-group-md mb-3 mt-5">
					<button type="submit" class="btn btn-primary btn-block"><i class="fas fa-chevron-left"></i> &nbsp; &nbsp; PERBAHARUI</button>
				</div>
		    </div>
		</form>
	</div>
</div>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/quiz.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/quiz.js');?>"></script>
