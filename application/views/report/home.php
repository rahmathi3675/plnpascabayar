<section >
	<div class="container-fluid mt-4">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fas fa-home"></i> Menu</a></li>
				<li class="breadcrumb-item active" aria-current="page"><span><i class="fas fa-list"></i> Report</span></li>
			</ol>
		</nav>
	</div>
</section>
<section>
	<div class="container-fluid mt-4">
	  	<div class="row">
	  		<div class="col-sm-12 col-md-12 col-lg-12">
	  			<iframe style="width: 100%; min-height: 850px; padding: 0px" src="https://crm.taco.co.id/crm/Generic_Taconnect" frameborder="yes">
                  <p>Your browser does not support iframes.</p>
                </iframe>
	  		</div>
	  	</div>
	</div>
</section>
<section class="mt-5"></section>
<script type="text/javascript">
	var role = '<?php echo base_url('Roles/getAllRoles') ?>';
</script>
<link href="<?= base_url('css_app/list_role.css');?>" rel="stylesheet" type="text/css">
<script src="<?= base_url('js_app/list_role.js');?>"></script>