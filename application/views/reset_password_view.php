<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PAYMENT REQUEST PT. TACO</title>
	<link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="<?php echo base_url('/assets/images/icon.png');?>" type="image/x-icon"/>

	<!-- Global stylesheets -->
	<link href="<?php echo base_url('/assets/template/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('/assets/vendors/css/icons/icomoon/styles.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('/assets/template/css/core.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('/assets/template/css/components.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('/assets/template/css/colors.min.css');?>" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?php echo base_url('/assets/vendors/js/plugins/loaders/pace.min.js');?>"></script>
	<script src="<?php echo base_url('/assets/vendors/js/core/libraries/jquery.min.js');?>"></script>
	<script src="<?php echo base_url('/assets/vendors/js/plugins/loaders/blockui.min.js');?>"></script>
	<script src="<?php echo base_url('/assets/vendors/js/core/libraries/bootstrap.min.js');?>"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?php echo base_url('/assets/template/js/app.js');?>"></script>

</head>

<body class="login-container" style="background-image:url(<?php echo base_url('assets/images/ar3.jpg')?>); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size:cover; background-position:bottom;">

	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content">
					<form method="post" id="form_login_a" action="<?php echo base_url('login/reset_password');?>" autocomplete="off">
						<div class="panel panel-body login-form" style="opacity: 0.9;filter: alpha(opacity=50);">

							<div class="text-center">
								<img src="<?php echo base_url('assets/images/logo.png'); ?>">
								<h3 class="content-group">Reset Password</h3>
							</div>
							<?php if(isset($auth['error'])){ ?>
							<div class="alert alert-danger no-border">
								<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
								<span class="text-semibold"><?php echo $auth['message'];?></span>
							</div>
							<?php } ?>

							<?php if(isset($auth['success'])){ ?>
							<div class="alert alert-success no-border">
								<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
								<span class="text-semibold"><?php echo $auth['message'];?></span>
							</div>
							<?php } ?>

							<div class="form-group has-feedback has-feedback-left">
								<input type="email" class="form-control" name="email" readonly value="<?= $auth['Related_information_email'] ?>">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>
							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" required>
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" name="re_password" id="re-password" placeholder="Confirm Password" required>
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							

							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Confirm Reset Password <i class="icon-circle-right2 position-right"></i></button>
							</div>
							<div class="form-group">
								<a style="padding-top: 20px !important" class="mt-sm" href="<?= base_url('login') ?>">Login Page</a>
							</div>
						</div>
					</form>
					<div class="footer text-muted text-center">
						&copy; 2018. <a href="https://www.taco.co.id/" target="_blank">PT. TACO</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
