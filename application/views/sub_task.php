<link href="<?= base_url('assets/vendors/fullcalendar-5/lib/main.css') ?>" rel='stylesheet' />
<script src="<?= base_url('assets/vendors/fullcalendar-5/lib/main.js') ?>"></script>

<?php 
if( $this->session->userdata('username') != ''){

	?>
	<div class="bg-white pt-5 p-3 mb-0 px-5">
		<div class="row">
			
			<div class="col-md-12" id="div-menu">

				<h3 class="font-regular">MENU PILIHAN</h3>
				<hr>
				<p>
					<h6 class="font-light">1. 	Input Angka</h6>
					<h6 class="font-light">2. 	Sorting</h6>
					<h6 class="font-light">3. 	Searching</h6>
					<h6 class="font-light">4. 	Selesai</h6>
					<br>
					<div class="font-light">Masukan Pilihan [1/2/3/4] : <input type="number" name="input_menu" id="input-menu">
					</div>

				</p>
			</div>

			<div class="col-md-12" id="div-menu-searching" style="display: none;">

				<h3 class="font-regular">TAMPILAN HASIL SEARCHING</h3>
				<hr>
				<p>
					<div class="font-light">Masukan Angka Yang Dicari : 
						<input type="number" id="input-nilai-search">
					</div>
					<br>
					<div id="div-note-searching">
					</div>
					<div id="div-hasil-searching" style="display: none;">
						<h6 class="font-bold" id="hasil-searching"></h6>
					</div>

				</p>
			</div>

			<div class="col-md-12" id="div-menu-input" style="display: none;">

				<h3 class="font-regular">INPUT ANGKA</h3>
				<hr>
				<p>
					<div class="font-light">Masukan Angka Secara Acak : 
						<input type="number" id="input-nilai-angka">
					</div>
					<br>
					<div id="div-note-input">
						<h6 class="font-light">Input Angka Secara Acak</h6>
						<hr>
						<div id="list-array">
							<!-- <h6 class="font-light">Angka 1 : 70</h6>
							<h6 class="font-light">Angka 2 : 50</h6>
							<h6 class="font-light">Angka 3 : 90</h6> -->
						</div>
						<br>
					</div>
					<div id="div-hasil-searching" style="display: none;">
						<h6 class="font-bold" id="hasil-searching"></h6>
					</div>

				</p>
			</div>
			<div class="col-md-12" id="div-menu-sortir" style="display: none;">

				<h3 class="font-regular">TAMPILAN HASIL SORTIR</h3>
				<hr>
				<p>
					<div class="font-light">Hasil Sortir : 
						<h5 class="font-light" id="list-hasil-sortir"></h5>
					</div>
					<br>
				</p>
			</div>
		</div>
	</div>
	<?php 
}
else{
	?>

		<form method="post" id="form-verifikasi" name="form_verifikasi" enctype="multipart/form-data">
			<div class="content-main row ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2 mt-5">
				<div class="col-xl-3 col-lg-5 col-md-6 col-sm-8 ml-auto mr-auto text-center bg-primary">
					<div class="row align-items-center">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-2 pt-4">
							<i class="fas fa-bolt fa-4x text-white"></i>
						</div>
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-2">
							<h6 class="text-center text-white font-bold">PLN PASCABAYAR</h6>
						</div>
					</div>
				</div>
			</div>
			<div class="content-main bg-dark row ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
				<div class="col-xl-3 col-lg-5 col-md-6 col-sm-8 ml-auto mr-auto bg-white">
					<div class="row">
						<div class="col-md-12 col-lg-12 p-2 pt-5 pt-1 ml-auto mr-auto">
						    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12">
						    	<div class="input-group input-group-md">
						        	<input type="text" class="form-control form-biodata" id="username" name="username" maxlength="20" placeholder="USERNAME" value="">
						      	</div>
						    </div>
						    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 ">
						    	<div class="input-group input-group-md">
						        	<input type="password" readonly class="form-control form-biodata" id="password" name="password" maxlength="15" placeholder="PASSWORD" value="" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" value="">
						      	</div>
						    </div>
						</div>
					
					</div>
					<div class="col-sm-12 col-md-12 col-lg-12 bg-white pb-5 mb-5 mt-5">
						<div class="row align-items-center">
							<div class="col"></div>
							<div class="col-xl-12 col-lg-12 col-md-12 text-center">
								<button id="btn-login" type="button" class="btn bg-primary text-white p-2 w-100"><i class="fas fa-key fa-1x"></i> LOGIN </button>
							</div>
							<div class="col"></div>
						</div>
					</div>
				</div>
			</div>
		</form>
	<?php 
}
?>

<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<script src="<?php echo base_url('js_app/sub_task.js');?>"></script>
<link href="<?php echo base_url('css_app/landing_page.css');?>" rel="stylesheet" type="text/css">


