    </main>
    <div id="loading-page" class="cover-edit no-gutters justify-content-center" style="height: 100%; top: 0%; bottom: 0%; left: 0%; right: 0%; position: fixed; width: 100%; margin-left: 0%; margin-right: 0%; background-color: #fff;  z-index: 3; display: none;">
        <h3 class="text-center" style="position: fixed; top: 50%; bottom: 50%; margin-left: 20%; margin-right: 20%; width: 60%;">
            <i class="fa fa-hourglass-start fa-1x text-info"> Please Wait ...</i><br> 
            <div class="progress" style="margin-top: 5px;">
                <div id="loading" class="progress-bar progress-bar-striped progress-bar-animated bg-success myprogress" role="progressbar" style="width:100%;"></div>
            </div>
        </h3>
    </div>
</body>
</html>
<link href="<?php echo base_url('css_app/footer.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/footer.js');?>"></script>
