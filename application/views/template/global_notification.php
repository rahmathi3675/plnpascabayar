<!-- NOTIFICATION -->
<?php 
if($this->session->flashdata('flash_messages') != ""){
	$response = $this->session->flashdata('flash_messages');

	if($response['status'] == true){
		$status = 'primary  ';
	}
	else{
		$status = 'danger ';
	}
	?>
	<div class="global-notification">
		
		<div class="alert alert-<?= $status; ?> alert-dismissible fade show" role="alert">
			<h4 class="alert-heading">Information</h4> 
	  		<hr>
	  		<?php 
	  			echo "<b>".strtoupper($response['message'])."</b><br>";
				$i  = 1;
				if( isset($response['line']) ){
					if( !empty($response['line'])){
						foreach ($response['line'] as $value) {

							if(!empty($value)){
								echo $i.'. '.$value['message']."<br>";
								$i++;
							}
						}
					}
				}
			?>
	  		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    		<span aria-hidden="true">&times;</span>
	  		</button>
		</div>
	</div> 
	<?php
}
elseif(isset($flash_messages) ){

	$response = $flash_messages;
	if($response['status'] == true){
		$status = 'primary ';
	}
	else{
		$status = 'danger ';
	}
	?>
	<div class="global-notification">
		
		<div class="alert alert-<?= $status ?> alert-dismissible fade show" role="alert" >
			<h4 class="alert-heading">Information</h4> 
	  		<hr>
	  		<?php 
	  			echo "<b>".strtoupper($response['message'])."</b><br>";
				$i  = 1;
				if( isset($response['line']) ){
					if( !empty($response['line'])){
						foreach ($response['line'] as $value) {

							if(!empty($value)){
								echo $i.'. '.$value['message']."<br>";
								$i++;
							}
						}
					}
				}
			?>
	  		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    		<span aria-hidden="true">&times;</span>
	  		</button>
		</div>
	</div> 
	<?php
}	
?>