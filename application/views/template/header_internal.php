<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>DILUS</title>
	<link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="<?php echo base_url('/assets/images/icon.png');?>" type="image/x-icon"/>

	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/datepicker-jquery/jquery-ui.css'); ?>">
	
	<!-- FONT AWOSOME -->
	<link href="<?php echo base_url('assets/vendors/fontawesome/css/fontawesome.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/vendors/fontawesome/css/all.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/vendors/DataTables/datatables.min.css');?>" rel="stylesheet" type="text/css">
	
	<!-- Global stylesheets -->
	<link href="<?php echo base_url('assets/vendors/select2-fixed/select2.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/vendors/select2-fixed/select2-bootstrap4.css');?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('css_app/home_page.css');?>" rel="stylesheet" type="text/css">
	
	<?php 
	if($_SERVER['HTTP_HOST'] == "localhost"){ ?>

		<link href="<?= base_url('assets/vendors/bootstrap-4.0.0/dist/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
		<?php
	}
	else{ ?>
		<link href="<?= base_url('assets/vendors/bootstrap-4.0.0/dist/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
		<?php 
	}
	?>
	<script src="<?php echo base_url('assets/vendors/chartjs-master/package/dist/chart.min.js'); ?>"></script>

	<!-- JS -->
	<!-- Jquery -->		
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js');?>"></script>
	<!-- Font-Awosome -->
	<script src="<?php echo base_url('assets/vendors/fontawesome/js/fontawesome.js');?>"></script>
	<!-- DataTables -->
	<script src="<?php echo base_url('assets/vendors/DataTables/datatables.min.js');?>"></script>
	<!-- SweetAlert -->
	<script src="<?php echo base_url('assets/js/sweetalert2.all.js');?>"></script>
	<!-- Poppever -->
	<!-- <script src="<?php //echo base_url('assets/js/popper.min.js');?>"></script> -->
	<!-- Bootstrap 4 -->
	<?php 
	if($_SERVER['HTTP_HOST'] == "localhost"){ ?>
  		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
  		<!-- <script src="http://localhost/payment_request/assets/js/popper.min.js"></script> -->
		<script src="<?php echo base_url('assets/vendors/bootstrap-4.0.0/dist/js/bootstrap.js') ?>"></script>
		<script src="<?php echo base_url('assets/vendors/select2-fixed/select2.min.js'); ?>"></script>
		<?php 
	}
	else{	?>
		<!-- <script src="https://crm.taco.co.id/payment_request/assets/js/popper.min.js"></script> -->
		<script src="<?php echo base_url('assets/vendors/bootstrap-4.0.0/dist/js/bootstrap.js') ?>"></script>
		<script src="<?php echo base_url('assets/vendors/select2-fixed/select2.min.js'); ?>"></script>
		<?php 
	}
	?>
	<!-- Select2 -->

	<!-- <script src="<?php //echo base_url('assets/vendors/datetimepicker/build/jquery.datetimepicker.full.min.js'); ?>"></script> -->
	<script src="<?php echo base_url('assets/vendors/datepicker-jquery/jquery-ui.js'); ?>"></script>


	<!-- JqueryPlugin Number Format -->
	<script src="<?php echo base_url('assets/vendors/jquery-number-master/jquery.number.min.js') ?>"></script>

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<style type="text/css">
		.box-shadow-menu{
			box-shadow: 1px 5px 5px 3px #888888;
			width: 230px; min-width: 160px;
		}

		.select2-bootstrap4.css {
			padding-left: 15px;
		}
	</style>
</head>
<body id="body-frame" class="bg-dark" style="margin-left: 20px; margin-right: 20px; margin-top: 0px;">
