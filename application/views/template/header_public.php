<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title id="title">DILUS</title>
	<link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="<?php echo base_url('/assets/images/icon.png');?>" type="image/x-icon"/>

	<link rel="stylesheet" href="<?php echo base_url('assets/vendors/datepicker-jquery/jquery-ui.css'); ?>">
	
	<!-- FONT AWOSOME -->
	<link href="<?php echo base_url('assets/vendors/fontawesome/css/fontawesome.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/vendors/fontawesome/css/all.css');?>" rel="stylesheet" type="text/css">
	
	<link href="<?php echo base_url('assets/vendors/DataTables/datatables.min.css');?>" rel="stylesheet" type="text/css">
	<!-- <link href="<?php //echo base_url('assets/vendors/DataTables/Buttons-1.6.1/css/buttons.dataTables.min.css');?>" rel="stylesheet" type="text/css"> -->
	
	<!-- Global stylesheets -->
	<link href="<?php echo base_url('assets/vendors/select2-fixed/select2.min.css');?>" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url('assets/vendors/select2-fixed/select2-bootstrap4.css');?>" rel="stylesheet" type="text/css">
	<link href="<?= base_url('css_app/home_page.css');?>" rel="stylesheet" type="text/css">
	
	<?php 
	if($_SERVER['HTTP_HOST'] == "localhost"){ ?>

		<link href="<?= base_url('assets/vendors/bootstrap-4.0.0/dist/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
		<?php
	}
	else{ ?>
		<link href="<?= base_url('assets/vendors/bootstrap-4.0.0/dist/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
		<?php 
	}
	?>
	<script src="<?php echo base_url('assets/vendors/chartjs-master/package/dist/chart.min.js'); ?>"></script>

	<!-- JS -->
	<!-- Jquery -->		
	<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js');?>"></script>
	<!-- Font-Awosome -->
	<script src="<?php echo base_url('assets/vendors/fontawesome/js/fontawesome.js');?>"></script>
	<!-- DataTables -->
	<script src="<?php echo base_url('assets/vendors/DataTables/datatables.min.js');?>"></script>
	<!-- <script src="<?php //echo base_url('assets/vendors/DataTables/Buttons-1.6.1/js/dataTables.buttons.min.js');?>"></script>
	<script src="<?php //echo base_url('assets/vendors/DataTables/Buttons-1.6.1/js/buttons.html5.min.js');?>"></script>
	<script src="<?php //echo base_url('assets/vendors/DataTables/Buttons-1.6.1/js/buttons.print.min.js');?>"></script> -->
	<!-- SweetAlert -->
	<script src="<?php echo base_url('assets/js/sweetalert2.all.js');?>"></script>
	<!-- Poppever -->
	<!-- <script src="<?php //echo base_url('assets/js/popper.min.js');?>"></script> -->
	<!-- Bootstrap 4 -->
	<?php 
	if($_SERVER['HTTP_HOST'] == "localhost"){ ?>
  		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
  		<!-- <script src="http://localhost/payment_request/assets/js/popper.min.js"></script> -->
		<script src="<?php echo base_url('assets/vendors/bootstrap-4.0.0/dist/js/bootstrap.js') ?>"></script>
		<script src="<?php echo base_url('assets/vendors/select2-fixed/select2.min.js'); ?>"></script>
		<?php 
	}
	else{	?>
		<!-- <script src="https://crm.taco.co.id/payment_request/assets/js/popper.min.js"></script> -->
		<script src="<?php echo base_url('assets/vendors/bootstrap-4.0.0/dist/js/bootstrap.js') ?>"></script>
		<script src="<?php echo base_url('assets/vendors/select2-fixed/select2.min.js'); ?>"></script>
		<?php 
	}
	?>
	<!-- Select2 -->

	<!-- <script src="<?php //echo base_url('assets/vendors/datetimepicker/build/jquery.datetimepicker.full.min.js'); ?>"></script> -->
	<script src="<?php echo base_url('assets/vendors/datepicker-jquery/jquery-ui.js'); ?>"></script>





	<!-- JqueryPlugin Number Format -->
	<script src="<?php echo base_url('assets/vendors/jquery-number-master/jquery.number.min.js') ?>"></script>

	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<style type="text/css">
		.box-shadow-menu{
			box-shadow: 1px 5px 5px 3px #888888;
			width: 230px; min-width: 160px;
		}

		.select2-bootstrap4.css {
			padding-left: 15px;
		}
	</style>
</head>
<body id="body-frame" class="bg-dark" style="margin-left: 20px; margin-right: 20px; margin-top: 0px;">

	<nav class="navbar fixed-top">
	 	<!-- LOGO NU -->
	 	<a id="custom-navbar-logo" class="
	 		mt-2 
	 		mb-2 
	 		ml-2 
	 		mr-4 
	 		d-none d-sm-block 
	 		label-landing-page"
	 		href="<?= base_url('landing_page') ?>">
	 		<i class="fas fa-bolt" style="font-size: 1.6em;"></i> PLN PASCABAYAR</a>

	 	<!-- BURGER MENU Hidden in sm xs md -->
		<i class="
			mt-2 
			mb-2 
			ml-2 
			mr-2 
			d-xs-block d-sm-block d-md-block 
			d-lg-none d-xl-none 
			fas fa-bars bg-white text-primary 
			btn-nav-navigation" style="font-size: 2em;">
		</i>

	 	<!-- EXLORE Hidden hidden in sm xs md -->
	    <?php 
	    if( $this->session->userdata('role') == 'adm') { ?>
		    <button class="
		    	mt-2 
		    	mb-2 
		    	ml-2
		    	pl-3 pr-3
		    	d-none d-sm-none d-md-none 
		    	d-lg-block d-xl-block 
		    	rounded-0 
		    	btn btn-primary btn-nav-navigation btn-explore-landing-page">
		    	<i class="fas fa-bars"></i>
			</button>
			<?php 
		}
		?>
	   
	    <!-- SEARCH Hidden hidden in sm -->
	    <span class="
	    	mt-2 
	    	mb-2 
	    	mr-auto 
	    	d-none d-sm-none d-md-block d-lg-block d-xl-block
	    	">
	    	<!-- <input class="search-landing-page" style="width: 80%" type="text" placeholder="Search">
	    	<button  class="btn-search-landing-page"><i class="fas fa-search"></i></button> -->
		</span>
		
		

		
		<?php 

		if( $this->session->userdata('logged_in') == false ){ ?>

			<!-- LOGIN GOOGLE hidden in sm -->
			<!-- <a class="
				mt-2 
				mb-2 
				ml-auto ml-sm-auto ml-md-auto ml-lg-auto ml-xl-auto 
				mr-2 mr-sm-0 mr-lg-0 mr-xl-0 
				d-block d-sm-block d-md-block d-lg-block d-xl-block
				" href="<?= $google_auth_url ?>">
				<img src="<?= base_url('assets/images/google-logo-1.ico') ?>" height="35">
			</a> -->

			<!-- LOGIN NORMAL hidden in sm -->
			<!-- <a 
				class="
					mt-2 
					mb-2 
					ml-sm-2 
					d-block d-sm-block d-md-block d-lg-block d-xl-block 
					rounded-0 btn btn-primary text-white"
				href="<?= base_url('landing_page/login') ?>">
				<i class="fas fa-sign-in-alt"></i>
				Masuk
			</a> -->
			
			<!-- DAFTAR Hidden in sm xs md -->
			<!-- <a
				href="<?= base_url('landing_page/daftar') ?>"  
				class="
					mt-2 
					mb-2 
					mr-lg-4 mr-xl-4 
					ml-sm-2 
					d-none d-sm-none d-md-none d-lg-block d-xl-block  
					rounded-0 btn btn-success btn-explore-landing-page">
					<i class="fas fa-users"></i>
					Daftar
			</a> -->

			<!-- DAFTAR Hidden in lg xl -->
			<!-- <a 
				href="<?= base_url('landing_page/daftar') ?>" 
				class="
					mt-2 
					mb-2 
					ml-2 ml-sm-2 ml-md-2 ml-lg-2
					mr-2 mr-sm-2 mr-md-3 mr-lg-3 
	 				d-block d-sm-block d-md-block d-lg-none d-xl-none 
					rounded-0 btn btn-success btn-explore-landing-page">
					<i class="fas fa-users"></i>
					Daftar
			</a> -->
			<?php 
		}
		else{ 


			if ( $this->session->userdata('role') == 'nsr' || $this->session->userdata('role') == 'agt') { 

			}
			?>
			
			<!-- DAFTAR Hidden in sm xs md -->
			<a class="
				mt-2 
				mb-2 
				mr-2 mr-sm-2 mr-md-3 mr-lg-3 mr-xl-3
				ml-1 ml-sm-1 
				d-block d-sm-block d-md-block d-lg-block d-xl-block 
				rounded-0 btn btn-success btn-explore-landing-page text-white"
				href="<?= base_url('Auth/logOut') ?>">
				<i class="fas fa-sign-out-alt"></i>
				Keluar
			</a>
			<?php 
		}
		?>
	</nav>

	<nav  class="collapse col-sm-4 col-md-3 col-lg-2 mt-3 bg-nu sidebar custom-sidebar">
		<div class="sidebar-sticky">
			<div class="" id="collapseExample">
				<ul class="nav flex-column">
					<li class="nav-item text-center">
						<img src="<?= base_url('assets/images/logo_nu_with_bg.png') ?>" style="max-width: 300px; height:  100px;">
						<p class="text-center text-white">NU FATAYAT</p>
						<hr class="m-2 bg-white">
					</li>
				 	<li class="nav-item">
						<a class="nav-link" href="#">
							<i class="fas fa-tachometer-alt text-white"></i>
							</svg>
							<label class="text-white">&nbsp;Dashboard</label>
							<span class="sr-only">(current)</span>
						</a>
					</li>
					
					<?php
					if( $this->session->userdata('role') == 'adm') {

						?>
						<!-- <li class="nav-item custome-collapse collapse-learning" data-toggle="collapse" href="#collapse-learning" role="button" aria-expanded="false">
							<span>
							<a class="nav-link" href="#" style="">
								<i class="fas fa-chalkboard-teacher text-white"></i>
								<label class="text-white">&nbsp;ELearning</label>
								<i class="fas fa-angle-left m-2 text-white icon-collapse-lms float-right"></i>
							</a>
							</span>
							<ul class="collapse" id="collapse-learning" style="list-style-type: none;">
								<li class="nav-item">
									<a class="nav-link pl-0" href="<?= base_url('elearning') ?>">
										<i class="fas fa-chalkboard text-white"></i>
										<label class="text-white">&nbsp;Quiz</label>
									</a>
								</li>
							</ul>
						</li> -->
						<li class="nav-item custome-collapse collapse-lms" data-toggle="collapse" href="#collapse-managemen" role="button" aria-expanded="false">
							<span>
							<a class="nav-link" href="#" style="">
								<i class="fas fa-chalkboard-teacher text-white"></i>
								<label class="text-white">&nbsp;Managemen</label>
								<i class="fas fa-angle-left m-2 text-white icon-collapse-lms float-right"></i>
							</a>
							</span>
							<ul class="collapse" id="collapse-managemen" style="list-style-type: none;">
								<li class="nav-item">
									<a class="nav-link pl-0" href="<?= base_url('kegiatan') ?>">
										<i class="fas fa-chalkboard text-white"></i>
										<label class="text-white">&nbsp;Kegiatan</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0" href="<?= base_url('acara') ?>">
										<i class="fas fa-chalkboard text-white"></i>
										<label class="text-white">&nbsp;Acara</label>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item custome-collapse collapse-lms" data-toggle="collapse" href="#collapse-design" role="button" aria-expanded="false">
							<span>
							<a class="nav-link" href="#" style="">
								<i class="fas fa-chalkboard-teacher text-white"></i>
								<label class="text-white">&nbsp;Materi</label>
								<i class="fas fa-angle-left m-2 text-white icon-collapse-design float-right"></i>
							</a>
							</span>
							<ul class="collapse" id="collapse-design" style="list-style-type: none;">
								<li class="nav-item">
									<a class="nav-link pl-0" href="<?= base_url('quiz') ?>">
										<i class="fas fa-chalkboard text-white"></i>
										<label class="text-white">&nbsp;Quiz</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0" href="<?= base_url('vitud') ?>">
										<i class="fas fa-video text-white"></i>
										<label class="text-white">&nbsp;Video Tutorial</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0" href="<?= base_url('vitel') ?>">
										<i class="fas fa-chalkboard-teacher text-white"></i>
										<label class="text-white">&nbsp;Video Teleconference</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0" href="<?= base_url('modul') ?>">
										<i class="fas fa-book text-white"></i>
										<label class="text-white">&nbsp;&nbsp;Modul</label>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item custome-collapse collapse-lms" data-toggle="collapse" href="#collapse-lms" role="button" aria-expanded="false">
							<span>
							<a class="nav-link" href="#" style="">
								<i class="fas fa-chalkboard-teacher text-white"></i>
								<label class="text-white">&nbsp;Master</label>
								<i class="fas fa-angle-left m-2 text-white icon-collapse-lms float-right"></i>
							</a>
							</span>
							<ul class="collapse" id="collapse-lms" style="list-style-type: none;">
								
								<li class="nav-item">
									<a class="nav-link pl-0"
										href="<?= base_url('kategori_kegiatan') ?>">
										<i class="fas fa-cogs text-white"></i>
										<label class="text-white">&nbsp;&nbsp;Kategori Kegiatan</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0"
										href="<?= base_url('jenis_kegiatan') ?>">
										<i class="fas fa-cogs text-white"></i>
										<label class="text-white">&nbsp;&nbsp;Jenis Kegiatan</label>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item collapse-lms" data-toggle="collapse" href="#collapse-management-anggota" role="button" aria-expanded="false">
							<span>
							<a class="nav-link" href="#" style="">
								<i class="fas fa-users text-white"></i>
								<label class="text-white">&nbsp;Anggota</label>
								<i class="fas fa-angle-left m-2 text-white icon-collapse-lms" style="height: 10px; float: right;"></i>
							</a>
							</span>
							<ul class="collapse" id="collapse-management-anggota" style="list-style-type: none;">
								<li class="nav-item">
									<a class="nav-link pl-0" href="<?= base_url('anggota/verifikasi') ?>">
										<i class="fas fa-check text-white"></i>
										<label class="text-white">&nbsp;Verifikasi</label>
									</a>
								</li>
							</ul>
						</li>
						<?php 
					}
					else{
						?>
						
						<li class="nav-item custome-collapse collapse-lms" data-toggle="collapse" href="#collapse-lms" role="button" aria-expanded="false">
							<span>
							<a class="nav-link" href="#" style="">
								<i class="fas fa-chalkboard-teacher text-white"></i>
								<label class="text-white">&nbsp;Learning</label>
								<i class="fas fa-angle-left m-2 text-white icon-collapse-lms float-right"></i>
							</a>
							</span>
							<ul class="collapse" id="collapse-lms" style="list-style-type: none;">
								<li class="nav-item">
									<a class="nav-link pl-0" href="<?= base_url('elearning') ?>">
										<i class="fas fa-chalkboard text-white"></i>
										<label class="text-white">&nbsp;Quiz</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0" href="#">
										<i class="fas fa-video text-white"></i>
										<label class="text-white">&nbsp;Video Tutorial</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0" href="#">
										<i class="fas fa-chalkboard-teacher text-white"></i>
										<label class="text-white">&nbsp;Video Teleconference</label>
									</a>
								</li>
								<li class="nav-item">
									<a class="nav-link pl-0" href="#">
										<i class="fas fa-book text-white"></i>
										<label class="text-white">&nbsp;&nbsp;E-book</label>
									</a>
								</li>
							</ul>
						</li>
						<?php
					}
					?>
					<!-- <li class="nav-item collapse-lms" data-toggle="collapse" href="#collapse-management-event" role="button" aria-expanded="false">
						<span>
						<a class="nav-link" href="#" style="">
							<i class="fas fa-calendar-alt text-white"></i>
							<label class="text-white">&nbsp;Kalendar</label>
							<i class="fas fa-angle-left m-2 text-white icon-collapse-lms" style="height: 10px; float: right;"></i>
						</a>
						</span>
						<ul class="collapse" id="collapse-management-event" style="list-style-type: none;">
							<li class="nav-item">
								<a class="nav-link pl-0" href="">
									<i class="fas fa-calendar-plus text-white"></i>
									<label class="text-white">&nbsp;Entry Kegiatan</label>
								</a>
							</li>
						</ul>
					</li> -->
				</ul>

				<li class="nav-item" style="list-style-type: none;">
					<a class="nav-link" href="#">
						<!-- <i class="fas fa-tachometer-alt text-white"></i> -->
						<label class="text-white">&nbsp;Laporan</label>
						<span class="sr-only">(current)</span>
					</a>
				</li>
				<ul class="nav flex-column mb-2" style="list-style-type: none;">
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('report/membership') ?>">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text text-white"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
							<label class="text-white">Laporan Keanggotaan</label>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('report/my_course') ?>">
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text text-white"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
							<label class="text-white">Laporan LMS</label>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<main id="main" class="col col-sm-12 col-md-12 col-lg-12 pt-1 ml-sm-auto ml-md-auto ml-lg-auto ml-xl-auto content-custom-sidebar">
