<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2" style="min-height: 750px;">

	<div class="col-sm-12 col-md-8 col-lg-8 col-xl-8
		pr-xl-5 pt-xl-5 pl-xl-5 
		pr-lg-5 pt-lg-5 pl-lg-5 
		pr-md-5 pt-md-5 pl-md-5
		pr-sm-4 pt-sm-0 pl-sm-4">	
		<?php 
		$link =  $data_header->link_video;
		$arr_link = explode('/', $link);
		
		if ( $link == null || $link == ''){
			?>
			<div class="row">
				<div class="col-sm-12 text-white p-5 col-xl-12 bg-dark" style="min-height: 500px;">
					<i class="fas fa-desktop"></i> Video Belum diupload
				</div>
			</div>
			<?php
		}
		else{
			?>
			<iframe width="100%" height="550" src="https://www.youtube.com/embed/<?= $arr_link[3] ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" alt="A" allowfullscreen></iframe>
			<?php 
		}
		?>
	</div>
	<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 pb-4 
		pr-xl-5 pt-xl-4 pl-xl-0 
		pr-lg-5 pt-lg-4 pl-lg-0 
		pr-md-5 pt-md-4 pl-md-0
		pr-sm-4 pt-sm-0 pl-sm-4
		
		">
		<hr class="bg-nu">
		<h5 class="font-bold text-center bg-primary pt-2 pb-2 text-white">VIDEO TELECONFERENCE</h5>
		<hr class="bg-nu">

		<div class="row mt-3">
			<div class="col-lg-6 col-md-6 col-sm-12 mb-3  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-right font-light mb-0">Kode Kegiatan</p>
	        	<p class="text-right font-regular mb-0"><b><?= $data_header->kode_kegiatan ?></b></p>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-2  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-left font-light mb-0">Jenis Kegiatan</p>
	        	<p class="text-left font-regular mb-0"><b>Video Tutorial</b></p>
		    </div>
		</div>
		<div class="row mt-1">
			<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-1 ">
	    		<p class="text-center mb-0 font-light">Nama Kegiatan</p>
	        	<p class="text-center font-regular"><b><?= $data_header->nama_kegiatan ?></b></p>
	      	</div>

		    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-1">
	    		<p class="text-center mb-0 font-light">Deksripsi Kegiatan</p>
	        	<p class="text-center font-regular mb-0"><b><?= $data_header->deskripsi ?></b></p>
	      	</div>
		</div>
		<div class="row mt-1">
			<div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-right font-light mb-0">TGL TERBIT</p>
	        	<p class="text-right font-regular"><b><?= $data_header->tanggal_terbit ?></b></p>
		    </div>
		    <div class="col-lg-6 col-md-6 col-sm-12 mb-1  mt-0 mt-md-5 mt-lg-2">
		    	<p class="text-left font-light mb-0">WAKTU</p>
	        	<p class="text-left font-regular"><b><?= date('H:i', strtotime($data_header->tanggal_terbit)) ?></b></p>
		    </div>
		</div>
		<div class="row mt-1">
			<?php 
			if( $data_header->link_meeting != null ){ ?>
				<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-1 ">
		    		<p class="text-center mb-0 font-light">Link Meeting</p>
		        	<p class="text-center font-regular"><b><a target="blank" href="<?= $data_header->link_meeting ?>"><i class="fas fa-video"></i> Go To Online Meeting</a></b></p>
		      	</div>
		      	<?php 
		    }

	      	if( $data_header->link_feedback != null ){ ?>
			    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-1">
		    		<p class="text-center mb-0 font-light">Feedback</p>
		        	<p class="text-center font-regular mb-0"><b><a target="blank" href="<?= $data_header->link_feedback ?>"><i class="fas fa-file-invoice"></i> Go To Feedback Form</a></b></p>
		      	</div>
		      	<?php 
		   	}
		   	?>
		</div>
	</div>
	<div class="col-sm-12 col-md-3 col-lg-3 col-xl-3"></div>
</div>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/kegiatan.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/kegiatan.js');?>"></script>
