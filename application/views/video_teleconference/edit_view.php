<?php echo form_open_multipart('vitel/update'); ?>
<form method="post" id="form-create-kegiatan" name="form_create_kegiatan">

<div class="content-main row bg-white ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 pt-5 pl-5 pr-5 pb-0">
		<nav aria-label="breadcrumb">
		  	<ol class="breadcrumb bg-nu">
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url() ?>"><i class="fas fa-home"></i> Home</a></li>
		    	<li class="breadcrumb-item text-white"><a class="text-white" href="<?= base_url('vitel') ?>"><i class="fas fa-list"></i> List Video Teleconference</a></li>
		    	<li class="breadcrumb-item  text-white active" aria-current="page"><i class="fas fa-solar-panel"></i> Edit</li>
		  	</ol>
		</nav>
	</div>
	<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 p-5 " >
		<input readonly type="hidden" style="display: none;" id="id-anggota" name="id_anggota" value="<?= rand(666666666666666,888888888888888).$data_header->id.rand(666666666666666,999999999999999) ?>">
		<input readonly type="hidden" name="kode_kegiatan" id="kode-kegiatan" readonly maxlength="100" value="<?= $data_header->kode_kegiatan ?>" style="display: none;">
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-0 ">
	    	<div class="input-group input-group-md">
	        	<input type="file" id="file-upload" name="file_upload">
	        </div>
	    </div>
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 mt-4 bg-dark p-3">
	    	<div class="input-group input-group-md justify-content-center">
	        	<img id="photo-kegiatan" class="" src="<?= $this->api_url.'document/activity/'.$data_header->gambar ?>" width="100%" height="100%" alt="img">
	      	</div>
	    </div>
	</div>
	<div class="col-sm-12 col-md-6 col-lg-6 col-xl-6 
		pr-xl-5 pt-xl-5 pl-xl-0 
		pr-lg-5 pt-lg-5 pl-lg-0 
		pr-md-5 pt-md-5 pl-md-0
		pr-sm-4 pt-sm-0 pl-sm-4
		">
		<div class="form-group col-lg-6 col-md-8 col-sm-12 mb-3  mt-0 mt-md-5 mt-lg-5">
	    	<div class="input-group input-group-md">
	        	<input type="text" class="form-control form-biodata bg-white" placeholder="NAMA KEGIATAN" maxlength="200" disabled value="<?= $data_header->kode_kegiatan ?>">
	      	</div>
	    </div>
		<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<select class="form-control form-biodata bg-white" id="kategori-kegiatan" name="kategori_kegiatan"  required>
	        		<option value="">- PILIH KATGEORI -</option>
	        		<?php
	        		foreach ($data_kategori as $index => $column) {
	        			$selected_kategori =  ( $column->kode_kategori == $data_header->kode_kategori ) ? " selected ": "";
	        			?>
	        			<option <?= $selected_kategori ?> value="<?= $column->kode_kategori ?>"><?= strtoupper($column->nama_kategori) ?></option>
	        			<?php 
	        		}
	        		?>
	        	</select>
	      	</div>
	    </div>
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<select class="form-control form-biodata bg-white" id="jenis-kegiatan" name="jenis_kegiatan" required>
	        		<?php
	        		foreach ($data_jenis as $index => $column) {
	        			if ( $column->kode_jenis == $data_header->kode_jenis ){
	        				?>
	        				<option selected value="<?= $column->kode_jenis ?>"><?= strtoupper($column->nama_jenis) ?></option>
	        				<?php 
	        			}
	        		}
	        		?>
	        	</select>
	      	</div>
	    </div>		
		<div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<input type="text" class="form-control form-biodata bg-white" name="nama_kegiatan" placeholder="NAMA KEGIATAN / VIDEO" maxlength="200" required value="<?= $data_header->nama_kegiatan ?>">
	      	</div>
	    </div>
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	        	<textarea class="form-control form-biodata bg-white" name="deskripsi" placeholder="DESKRIPSI" rows="3" required><?= $data_header->deskripsi ?></textarea>
	      	</div>
	    </div>
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	    		<input type="text" class="form-control form-biodata bg-white" name="link_meeting" placeholder="LINK MEETING" required value="<?= $data_header->link_meeting ?>">
	      	</div>
	    </div>
	    <div class="form-group col-lg-12 col-md-12 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	    		<input type="text" class="form-control form-biodata bg-white" name="link_feedback" placeholder="LINK FEEDBACK" required value="<?= $data_header->link_feedback ?>">
	      	</div>
	    </div>
	    <div class="form-group col-lg-6 col-md-6 col-sm-12 mb-3 ">
	    	<div class="input-group input-group-md">
	    		<input type="text" readonly class="form-control form-biodata datetimepicker bg-white" name="tanggal_kegiatan" placeholder="TANGGAL KEGIATAN" required value="<?= $data_header->tanggal_kegiatan ?>">
	      	</div>
	    </div>
	    <div class="form-group col-lg-4 col-md-4 col-sm-6 mb-3 ">
	    	<div class="input-group input-group-md">
	    		<input type="time"  class="form-control form-biodata bg-white" name="waktu_kegiatan" placeholder="" value="<?= $data_header->waktu_kegiatan ?>" required>
	      	</div>
	    </div>
	    <div class="form-group col-lg-4 col-md-6 col-sm-12 mb-3 mt-5">
	    	<button class="btn btn-md btn-success btn-block"><i class="fas fa-save"></i> UPDATE</button>
	    </div>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 p-5">
		
	</div>
</div>
	</form>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/video-teleconference.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/video_teleconference.js');?>"></script>
