
<form method="post" id="form-verifikasi" name="form_verifikasi" enctype="multipart/form-data">
<div class="content-main row bg-dark ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-xl-3 col-lg-5 col-md-6 col-sm-8 ml-auto mr-auto text-center bg-nu">
		<div class="row align-items-center">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-2">
				<img  src="<?= base_url('assets/images/logo_nu_no_bg.png') ?>" height="100" width="150">
			</div>
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 p-2">
				<h6 class="text-center text-white">NU FATAYAT LOGIN</h6>
			</div>
		</div>
	</div>
</div>
<div class="content-main row bg-dark ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-xl-3 col-lg-5 col-md-6 col-sm-8 ml-auto mr-auto bg-white">
		<div class="row">
			<div class="col-md-12 col-lg-12 p-2 pt-5 pt-1 ml-auto mr-auto">
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12">
			    	<div class="input-group input-group-md">
			        	<input type="text" class="form-control form-biodata" id="username" name="username" maxlength="20" placeholder="USERNAME" value="">
			      	</div>
			    </div>
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 ">
			    	<div class="input-group input-group-md">
			        	<input type="password" readonly class="form-control form-biodata" id="password" name="password" maxlength="15" placeholder="PASSWORD" value="" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" value="">
			      	</div>
			    </div>
			</div>
		
		</div>
		<div class="col-sm-12 col-md-12 col-lg-12 bg-white pb-5 mb-5 mt-5">
			<div class="row align-items-center">
				<div class="col"></div>
				<div class="col-xl-12 col-lg-12 col-md-12 text-center">
					<button id="btn-login" type="button" class="btn bg-nu text-white p-3 w-100"><i class="fas fa-save fa-1x"></i> LOGIN </button>
				</div>
				<div class="col"></div>
			</div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/login.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/login.js');?>"></script>
