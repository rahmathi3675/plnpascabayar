
<form method="post" id="form-verifikasi" name="form_verifikasi" enctype="multipart/form-data">
<div class="content-main row bg-dark ml-xl-5 mr-xl-5 ml-lg-6 mr-lg-5 ml-md-5 mr-md-5 ml-sm-2 mr-sm-2">
	<div class="col-md-12 col-lg-12 col-xl-12 text-center bg-nu">
		<div class="row align-items-center">
			<div class="col-xl-2 col-lg-3 col-md-3 col-sm-5 p-2">
				<img  src="<?= base_url('assets/images/logo_nu_no_bg.png') ?>" height="100" width="150">
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-7 p-2">
				<h4 class="text-center text-white">DAFTAR KEANGGOTAAN</h4>
				<h6 class="text-center text-white">Form Registrasi</h6>
			</div>
			<div class="col-md-4 col-lg-3 col-md-3 col-sm-12 d-none d-sm-none d-md-none d-lg-block d-xl-block pr-5 text-right text-white  ml-auto">
				<h5 class="text-left text-white text-right">NU FATAYAT</h5>
				<h6 class="text-left text-white text-right">JAWA BARAT</h6>
			</div>
		</div>
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 bg-white">
		
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 bg-white">
		<div class="row">
			<div class="col-md-6 col-lg-6 p-2 pt-5 pt-1 ml-auto mr-auto">
			    <div class="form-group col-xl-12 col-lg-12 col-md-8 col-sm-12">
			    	<div class="input-group input-group-md">
			        	<input type="text" class="form-control form-biodata" id="username" name="username" maxlength="20" placeholder="USERNAME" value="rahmathi111">
			      	</div>
			    </div>
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 ">
			    	<div class="input-group input-group-md">
			        	<input type="password" readonly class="form-control form-biodata" id="password" name="password" maxlength="15" placeholder="PASSWORD" value="" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" value="123">
			      	</div>
			    </div>
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 ">
			    	<div class="input-group input-group-md">
			        	<input type="password" class="form-control form-biodata" id="confirm-password" name="confirm_password" maxlength="15" placeholder="CONFORM PASSWORD" autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" value="123">
			      	</div>
			    </div>
			    <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 mb-3 ">
			    	<div class="input-group input-group-md">
			        	<input type="email" name="email" id="email" class="form-control form-biodata" maxlength="150" placeholder="EMAIL" value="test@gmail.com">
			      	</div>
			    </div>
			</div>
		
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 bg-white pb-5">
  			
		
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 bg-white pb-5 mb-5">
		<div class="row align-items-center">
			<div class="col"></div>
			<div class="col-xl-6 col-lg-6 col-md-6 text-center">
				<button id="btn-update-biodata" type="button" class="btn bg-nu text-white p-3 w-100"><i class="fas fa-save fa-1x"></i> UPDATE PROFIL </button>
			</div>
			<div class="col"></div>
		</div>
	</div>
</div>
</form>
<script type="text/javascript">
	var base_url = "<?= base_url() ?>";
</script>
<link href="<?php echo base_url('css_app/anggota.css');?>" rel="stylesheet" type="text/css">
<script src="<?php echo base_url('js_app/daftar.js');?>"></script>
