

$(document).ready( function(){

    $('.datetimepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('.select2').select2({
        theme: 'bootstrap'
    });

    $('#postal-code').select2({
        allowClear: true,
        /*theme: 'bootstrap',*/
        minimumInputLength: 3,
        tags: false,
        ajax: {
            url: base_url+ '/Anggota_Controller/getPostalCode',
            dataType: 'json',
            type: "POST",
            data: function (keywords) {
                return {
                    postal_code: keywords
                };
            },
            processResults: function (ParseDataJson) {
                return {
                    results: $.map(ParseDataJson.data, function (item) {
                        return {text: item.postal_code.toUpperCase()+', '+ item.province.toUpperCase() +', '+ item.city.toUpperCase() +', '+item.kecamatan.toUpperCase()+', '+item.kelurahan.toUpperCase(), 
                        id: item.postal_code+','+item.id_province+','+item.id_city+','+item.kecamatan+','+item.kelurahan+','}
            
                    })
                };
            }
        }
    });

    $('#postal-code').on('change', function(){
        let address = $(this).text();
        let rincian = address.split(',');
        console.log(rincian);
        $("#provinsi").val(rincian[1]);
        $("#kabupaten-kota").val(rincian[2]);
        $("#kecamatan").val(rincian[3]);
        $("#kelurahan").val(rincian[4]);
    });

    var persen = 0;
    
    function make_base_auth(user, password) {
        var tok = user + ':' + password;
        var hash = btoa(tok);
        return 'Basic ' + hash;
    }

    $('#btn-update-biodata').on('click',function(){
        
        let username = $('#username').val();
        let password = $('#password').val();
        let confirm_password = $('#confirm-password').val();

        if( confirm_password == '' || password == '' ) {
            Swal.fire({
                icon: 'error',
                title: '<strong>Information</strong>',
                html: "Password dan Konfirmasi Tidak Boleh Kosong",
                showCloseButton: false,
                showCancelButton: false,
            })
        }
        else if ( confirm_password != password  ){
            Swal.fire({
                icon: 'error',
                title: '<strong>Information</strong>',
                html: "Password dan Konfirmasi Tidak Sama",
                showCloseButton: false,
                showCancelButton: false,
            })
        }
        else{

            var form = document.forms.namedItem("form_verifikasi");
            $.ajax({
                url: base_url+'/Anggota_Controller/updateProfil',
                type: "POST",
                data:  new FormData(form),
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                success: function(response)
                {
                    if(response.response == true){
                        
                       
                        Swal.fire({
                            icon: 'success',
                            title: '<strong>Verifikasi</strong>',
                            html: "Sukses Update Data",
                            showCloseButton: false,
                            showCancelButton: false,
                        });
                    }
                    else{
                        let html = "";
                        for( i = 0; i < response.line.length; i++){
                            html += '* '+ response.line[i].message+'<br>';
                        }
                        Swal.fire({
                            icon: 'error',
                            title: '<strong>Verifikasi</strong>',
                            html: html,
                            showCloseButton: false,
                            showCancelButton: false,
                        })
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                },
                error: function(response) 
                {   
                    /*Swal.fire(
                      'Error',
                      'Please Contact Admin',
                      'error'
                    )*/
                }           
                
            });
        }
    });

    $('#file-upload').on('change', function(){

        var file_data = $(this).prop('files')[0];
        var form_data = new FormData();
        form_data.append('file_upload', file_data);        
        form_data.append('token', $('#token').val());
        
        if( file_data != '') {
            $.ajax({
                url: base_url+"Anggota_Controller/uploadPhoto",
                type: "POST",
                data: form_data,
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.myprogress').text(percentComplete + '%');
                            $('.myprogress').css('width', percentComplete + '%');
                        }
                    }, 
                    false);
                    return xhr;
                },
                success: function( result )
                {
                    if( result.response == true ){
                        let link_image = result.message.directory+result.message.file_name;
                        $("#photo-profile").attr("src", link_image);
                    }
                    /*if(response.status == true){
                        
                        let html = "";
                        for( i = 0; i < response.line.length; i++){
                            if( response.line[i].status == false ){
                                html += '<i class="fas fa-times-circle"></i> '+ response.line[i].message+'<br>';
                            }
                            else{
                                html += '<i class="fas fa-check"></i> '+ response.line[i].message+'<br>';
                            }
                        }
                        Swal.fire({
                            icon: 'success',
                            title: '<strong>Reimbursement</strong>',
                            html: html,
                            showCloseButton: false,
                            showCancelButton: false,
                        })
                    }
                    else{
                        let html = "";
                        for( i = 0; i < response.line.length; i++){
                            html += '* '+ response.line[i].message+'<br>';
                        }
                        Swal.fire({
                            icon: 'error',
                            title: '<strong>Reimbursement</strong>',
                            html: html,
                            showCloseButton: false,
                            showCancelButton: false,
                        })
                    }*/


                },
                complete: function(){
                    $('#loading-page').fadeOut();
                },
                error: function(response) 
                {   
                    /*Swal.fire(
                      'Error',
                      'Please Contact Admin',
                      'error'
                    )*/
                }           
            })
        }
    });


    /*var username = "username_here";
    var password = "password_here";
    
    $.ajax({
        type: "GET",
        url: "https://api.seradu.id/Api/Services/jabatan",
        dataType: 'json',
        headers: {
            "Authorization": "Basic " + btoa(username + ":" + password),
            //"accept": "application/json",
            "key":"development_key"
        },
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Basic "
                + btoa("admin" + ":" + "123"));
        },
        success: function(result) {
            console.log(result)
        }
    });*/
});