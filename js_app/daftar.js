

$(document).ready( function(){

    $('.datetimepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('.select2').select2({
        theme: 'bootstrap'
    });


    var persen = 0;
    
    function make_base_auth(user, password) {
        var tok = user + ':' + password;
        var hash = btoa(tok);
        return 'Basic ' + hash;
    }

    $('#btn-update-biodata').on('click',function(){
        
        let username    = $('#username').val();
        let password    = $('#password').val();
        let confirm_password = $('#confirm-password').val();
        let email       = $('#email').val();
        console.log(email);

        if( confirm_password == '' || password == '' ) {
            Swal.fire({
                icon: 'error',
                title: '<strong>Information</strong>',
                html: "Password dan Konfirmasi Tidak Boleh Kosong",
                showCloseButton: false,
                showCancelButton: false,
            })
        }
        else if ( confirm_password != password  ){
            Swal.fire({
                icon: 'error',
                title: '<strong>Information</strong>',
                html: "Password dan Konfirmasi Tidak Sama",
                showCloseButton: false,
                showCancelButton: false,
            })
        }
        else if ( email == '' || email == null || username == '' ){
            Swal.fire({
                icon: 'error',
                title: '<strong>Information</strong>',
                html: "Email & Username Mandatory",
                showCloseButton: false,
                showCancelButton: false,
            })
        }
        else{

            var form = document.forms.namedItem("form_verifikasi");
            $.ajax({
                url: base_url+'/Landing_Page_Controller/prosesRegistrasi',
                type: "POST",
                data:  new FormData(form),
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                success: function(response)
                {
                    console.log(response);
                    if(response.response == true){
                        
                       
                        Swal.fire({
                            icon: 'success',
                            title: '<strong>Information</strong>',
                            html: "Sukses Registrasi Data",
                            showCloseButton: false,
                            showCancelButton: false,
                        });
                    }
                    else{
                        let html = "";
                        for( i = 0; i < response.line.length; i++){
                            html += '* '+ response.line[i].message+'<br>';
                        }
                        Swal.fire({
                            icon: 'error',
                            title: '<strong>Information</strong>',
                            html: html,
                            showCloseButton: false,
                            showCancelButton: false,
                        })
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                },
                error: function(response) 
                {   
                    /*Swal.fire(
                      'Error',
                      'Please Contact Admin',
                      'error'
                    )*/
                }           
                
            });
        }
    });

});