$("#btn-nav-navigation").on('click', function(){
    console.log($('.custom-sidebar:visible').length);
    if( $('.custom-sidebar:visible').length == 2 ){
        $('.custom-sidebar').hide("slide", {direction: "left"}, 500);
        setTimeout(function() {
            $('.content-custom-sidebar').removeClass('col-lg-10 col-md-9 col-sm-8');
            $('.content-custom-sidebar').addClass('col-lg-12 col-md-12 col-sm-12');
        }, 500);
    }
    else{
        $('.custom-sidebar').show("slide", {direction: "left"}, 500);   
        setTimeout(function() {
            $('.content-custom-sidebar').removeClass('col-lg-12 col-md-12 col-sm-12');
            $('.content-custom-sidebar').addClass('col-lg-10 col-md-9 col-sm-8');
        }, 500);
    }
});