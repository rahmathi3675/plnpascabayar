

$(document).ready( function(){

    $('.select2').select2({
        theme: 'bootstrap'
    });

    $('.datetimepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });


    var current_bg = "";
    var current_font = "";
    var data_global;
    var global_current_position;

    getDataCourse();
    function getDataCourse (){
        var kode_khs = $('#kode-khs').val();
        var no_pendaftaran = $('#no-pendaftaran').val();
     
        $.ajax({
            url: base_url+'/Quiz_Controller/getAllTask',
            type: "POST",
            data:  {
                'no_pendaftaran' : no_pendaftaran,
                'kode_ksh' : kode_khs
            },
            dataType: 'json',
            beforeSend: function(){
                $('#loading-page').fadeIn();
            },
            success: function(response)
            {   
                if(response.response == true){
                    
                    data_global =response.data; 
                    writeTask( response.data );
                    writePanel( response.data );
                }
            },
            complete: function(){
                $('#loading-page').fadeOut();
            },
        });
    }

    function writeTask( data ) {
    
        let i = 0;
        let n = 0;
        let selected_jawaban = "";
        let current_task = false;
        while( i < data.length ){
            
            if( data[i].is_current_position == 1){
               
                $('#task-header').empty();
                $('#task-body').empty();
                $('#kode-materi').val(data[i].kode_materi);

                $('#task-header').append(`
                    <p>
                        <span>( `+(i+1)+` )</span> 
                        <span class="bg-white font-medium ml-3" rows="3">`+data[i].deskripsi_materi+`</span>
                    </p>
                `);

                n = 0;
                while( n < data[i].detail.length ){
                    selected_jawaban = "";
                    if( data[i].detail[n].kode_jawaban == data[i].selected_jawaban ){
                        selected_jawaban = " checked ";
                    }

                    $('#task-body').append(`
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 ml-5">
                            <label for="`+data[i].detail[n].kode_jawaban+`">
                                <input id="`+data[i].detail[n].kode_jawaban+`"  `+selected_jawaban+` type="radio" name="key" class="key" value="`+data[i].detail[n].kode_jawaban+`" required> 
                                <span class="ml-3">`+data[i].detail[n].deskripsi_jawaban+`</span>
                            </label>
                        </div>
                    `);
                    n++;
                }
            }
            i++;
        }
    }

    function writePanel( $data ){

        let i = 0;
        let n = 0;
        let task_number = 1;
        let selected_jawaban = "";
        let current_task = false;
        let data = $data;

        $('#panel-jawaban').empty();
        let count_answered = 0;
        while( i < data.length ){
            
            if( data[i].selected_jawaban != '' && data[i].selected_jawaban != null ){
                count_answered++;
            }

            if( data[i].is_current_position == 1){
                
                global_current_position = i;

               
                $('#panel-jawaban').append(`
                    <div class="div-task-number col-xl-1 col-lg-1 col-md-1 col-sm-1 text-center
                        pr-xl-1 pt-xl-1 pl-xl-1 pb-xl-2 m-1
                        pr-lg-3 pt-lg-5 pl-lg-3 
                        pr-md-3 pt-md-5 pl-md-3
                        pr-sm-3 pt-sm-0 pl-sm-3
                        div-task-hover task-hover
                        ">
                        <form class="form-task-number" method="post">
                            <input type="hidden" class="task-code" value="`+data[i].kode_materi+`">
                            <input type="hidden" class="selected-jawaban" value="`+data[i].selected_jawaban+`">
                            <input type="hidden" class="is-current-position" value="`+data[i].is_current_position+`"">
                            <span class="font-light">`+ task_number +`</span>
                        </form>
                    </div>
                `);
            }
            else{
                if( data[i].selected_jawaban == '' || data[i].selected_jawaban == null ){
                    $('#panel-jawaban').append(`
                        <div class="div-task-number col-xl-1 col-lg-1 col-md-1 col-sm-1 text-center
                            pr-xl-1 pt-xl-1 pl-xl-1 pb-xl-2 m-1
                            pr-lg-3 pt-lg-5 pl-lg-3 
                            pr-md-3 pt-md-5 pl-md-3
                            pr-sm-3 pt-sm-0 pl-sm-3
                            div-task-incomplete task-incomplete
                            ">
                            <form class="form-task-number" method="post">
                                <input type="hidden" class="task-code" value="`+data[i].kode_materi+`">
                                <input type="hidden" class="selected-jawaban" value="`+data[i].selected_jawaban+`">
                                <input type="hidden" class="is-current-position" value="`+data[i].is_current_position+`"">
                                <span class="font-light">`+ task_number +`</span>
                            </form>
                        </div>
                    `);
                }
                else{
                    $('#panel-jawaban').append(`
                        <div class="div-task-number col-xl-1 col-lg-1 col-md-1 col-sm-1 text-center
                            pr-xl-1 pt-xl-1 pl-xl-1 pb-xl-2 m-1
                            pr-lg-3 pt-lg-5 pl-lg-3 
                            pr-md-3 pt-md-5 pl-md-3
                            pr-sm-3 pt-sm-0 pl-sm-3
                            div-task-complete task-complete
                            ">
                            <form class="form-task-number" method="post">
                                <input type="hidden" class="task-code" value="`+data[i].kode_materi+`">
                                <input type="hidden" class="selected-jawaban" value="`+data[i].selected_jawaban+`">
                                <input type="hidden" class="is-current-position" value="`+data[i].is_current_position+`"">
                                <span class="font-light">`+ task_number +`</span>
                            </form>
                        </div>
                    `);
                }

            }
            i++;
            task_number++;
        }

        if( count_answered == data.length){
            $('#btn-submit').fadeIn();
        }
        else{
            $('#btn-submit').hide();
        }

        if( count_answered == data.length-1){
            $('#btn-selesai').fadeIn();
        }
        else{
            $('#btn-selesai').hide();
        }

        if( global_current_position == data.length-1 ){
            $('#btn-next').hide();
        }
        else{
            $('#btn-next').fadeIn();   
        }

        if( global_current_position > 0 ){
            $('#btn-prev').fadeIn();   
        }
        else{
            $('#btn-prev').hide();
        }
    }

    $(document).on('mouseover','.div-task-number', function(){
        
        //let selected_jawaban = $(this).find('.selected-jawaban');
        //let is_current_position = $(this).find('.is-current-position');
        //console.log(number_detail);

        if( $(this).hasClass('div-task-incomplete') ){
            $(this).removeClass("div-task-incomplete");
            $(this).removeClass("task-incomplete");
        };

        if( $(this).hasClass('div-task-complete') ){
            $(this).removeClass("div-task-complete");
            $(this).removeClass("task-complete");
        };

        if( $(this).hasClass('div-task-hover') ){
            $(this).removeClass("div-task-hover");
            $(this).removeClass("task-hover");
        };
    
        $(this).addClass('div-task-hover');
        $(this).addClass('task-hover');    
    });

    $(document).on('mouseleave','.div-task-number', function(){
        
        let selected_jawaban = $(this).find('.selected-jawaban').val();
        let is_current_position = $(this).find('.is-current-position').val();



        $(this).removeClass('div-task-hover');
        $(this).removeClass('task-hover');
        if( is_current_position == '1' ){
            $(this).addClass('div-task-hover');
            $(this).addClass('task-hover');
        }
        else{
            if( selected_jawaban ==  '' || selected_jawaban == 'null' ){

                $(this).addClass('div-task-incomplete');
                $(this).addClass('task-incomplete');
            }
            else{
                $(this).addClass('div-task-complete');
                $(this).addClass('task-complete');
            }
        }            
    });

    $(document).on('click','.div-task-number', function(){

        let selected_jawaban = $(this).find('.selected-jawaban').val();
        let is_current_position = $(this).find('.is-current-position').val();
        let task_code = $(this).find('.task-code').val();

        let current_kode_jawaban = $('.key:checked').val();
        let current_kode_materi = $('#kode-materi').val();
        let current_kode_khs = $('#kode-khs').val();

        // FIND INDEX ON CLICK
        let index = findIndexData( task_code, data_global );

        // CHANGE OLD CURRENT POSTITION TO 0
        data_global[global_current_position].is_current_position = 0;
        
        // CHANGE NEW POSTITION TO 1
        data_global[index].is_current_position = 1;

        if (typeof current_kode_jawaban !== 'undefined'){
            
            $.ajax({
                url: base_url+'/Quiz_Controller/updateDetailHasilStudy',
                type: "POST",
                data:  {
                    'kode_jawaban'  : current_kode_jawaban,
                    'kode_khs' : current_kode_khs,
                    'kode_materi'   : current_kode_materi
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                success: function(response)
                {   
                    if(response.status == true){
                        
                        // CHANGE CURRENT JAWABAN TO UPDATE JAWABAN
                        data_global[global_current_position].selected_jawaban = current_kode_jawaban;

                        // REWRITE TASK
                        writeTask(data_global);

                        // REWRITE PANEL
                        writePanel(data_global);
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                },
            });
        }
        else{
            // REWRITE TASK
            writeTask(data_global);

            // REWRITE PANEL
            writePanel(data_global);
        }
    });

    function findIndexData( $kode_materi, $source ){

        let z = 0;
        while( z < data_global.length ){
            if( data_global[z].kode_materi == $kode_materi ){
                return z;
            }
            z++;
        }
    }

    $('#btn-next').on('click', function(){
        let current_div = jQuery($('.div-task-number')[global_current_position]);
        passPage( current_div, global_current_position, global_current_position+1 );
    });

    $('#btn-selesai').on('click', function(){
        let current_div = jQuery($('.div-task-number')[global_current_position]);
        passPage( current_div, global_current_position, global_current_position );
    });

    $('#btn-submit').on('click', function(){
        
        var kode_khs = $('#kode-khs').val();
        var kode_khs_no = $('#kode-khs-no').val();
     
        $.ajax({
            url: base_url+'/Quiz_Controller/finishCourse',
            type: "POST",
            data:  {
                'kode_khs' : kode_khs
            },
            dataType: 'json',
            beforeSend: function(){
                $('#loading-page').fadeIn();
            },
            success: function(response)
            {   
                if(response.response == true){
                    
                    window.location.href=base_url+'quiz/cetak_khs/'+kode_khs_no;
                    
                }
            },
            complete: function(){
                $('#loading-page').fadeOut();
            },
        });

    });

    function passPage( parsing_task, current_number, next_number ){


        let current_task = parsing_task;
        let selected_jawaban    = current_task.find('.selected-jawaban').val();
        let is_current_position = current_task.find('.is-current-position').val();
        let task_code           = current_task.find('.task-code').val();
        
        let current_kode_jawaban = $('.key:checked').val();
        let current_kode_materi = $('#kode-materi').val();
        let current_kode_khs = $('#kode-khs').val();


        // FIND INDEX ON CLICK
        let index = findIndexData( current_kode_materi, data_global );
        /*console.log("SEBELUM");
        console.log(index);
        console.log(data_global);
        console.log(global_current_position);
        console.log("SESUDAH");*/



        // CHANGE OLD CURRENT POSTITION TO 0
        data_global[global_current_position].is_current_position = '0';
        
        // CHANGE NEW POSTITION TO 1
        data_global[next_number].is_current_position = '1';

        global_current_position = next_number;

        if (typeof current_kode_jawaban !== 'undefined'){
            $.ajax({
                url: base_url+'/Quiz_Controller/updateDetailHasilStudy',
                type: "POST",
                data:  {
                    'kode_jawaban'  : current_kode_jawaban,
                    'kode_khs' : current_kode_khs,
                    'kode_materi'   : current_kode_materi
                },
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                success: function(response)
                {   
                    if(response.status == true){
                        
                        // CHANGE CURRENT JAWABAN TO UPDATE JAWABAN
                        data_global[index].selected_jawaban = current_kode_jawaban;

                        // REWRITE TASK
                        writeTask(data_global);

                        // REWRITE PANEL
                        writePanel(data_global);
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                },
            });
        }
        else{
            // REWRITE TASK
            writeTask(data_global);

            // REWRITE PANEL
            writePanel(data_global);
        }
    }

    $('#btn-prev').on('click', function(){
        let current_div = jQuery($('.div-task-number')[global_current_position]);
        //console.log( $('.div-task-number').length);
        passPage( current_div, global_current_position, global_current_position-1 );
    });

    var countDownDate = new Date( $('#tanggal-selesai').val()).getTime();

    var x = setInterval(function() {

        var now = new Date().getTime();
        var distance = countDownDate - now;
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("demo").innerHTML = `
            <span class="font-regular text-white" style="font-size: 1.5em;"> `+days + ` DAY ` + hours + ` HOUR </span>
            <br>
            <span class="font-medium font-bold text-white" style="font-size: 2em;">`+ minutes + `m : ` + seconds + `s</span>`;

        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        } 
    }, 1000);
});