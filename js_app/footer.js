$(".btn-nav-navigation").on('click', function(){
    
    if( $('.sidebar-sticky:visible').is(':visible') == true ){

        $('.custom-sidebar').hide("slide", {direction: "left"}, 500);
        setTimeout(function() {
            $('.content-custom-sidebar').removeClass('col-lg-10 col-md-9 col-sm-8 ml-sm-auto ml-md-auto ml-lg-auto ml-xl-auto');
            $('.content-custom-sidebar').addClass('col-lg-12 col-md-12 col-sm-12 ml-sm-auto ml-md-auto ml-lg-auto ml-xl-auto');
        }, 500);
    }
    else{
        $('.custom-sidebar').show("slide", {direction: "left"}, 500);   
        setTimeout(function() {
            $('.content-custom-sidebar').removeClass('col-lg-12 col-md-12 col-sm-12 ml-sm-auto ml-md-auto ml-lg-auto ml-xl-auto');
            $('.content-custom-sidebar').addClass('col-lg-10 col-md-9 col-sm-8 ml-sm-auto ml-md-auto ml-lg-auto ml-xl-auto');
        }, 500);
    }
});

$(".global-notification").fadeTo(2000, 2000).slideUp(2000, function(){
    $(".global-notification").slideUp(5000);
});
