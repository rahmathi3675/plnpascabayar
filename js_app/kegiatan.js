

$(document).ready( function(){

    $('.select2').select2({
        theme: 'bootstrap'
    });

    $('.datetimepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('#btn-cetak').on('click', function(){
        window.print();

    });

    $('#file-upload').on('change', function(){

        var file_data = $(this).prop('files')[0];
        let kode_kegiatan = $('#kode-kegiatan').val(); 
        var form_data = new FormData();
        form_data.append('file_upload', file_data);
        form_data.append('kode_kegiatan', kode_kegiatan);

        
        if( file_data != '') {
            $.ajax({
                url: base_url+"Kegiatan_Controller/uploadPhoto",
                type: "POST",
                data: form_data,
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.myprogress').text(percentComplete + '%');
                            $('.myprogress').css('width', percentComplete + '%');
                        }
                    }, 
                    false);
                    return xhr;
                },
                success: function( result )
                {
                    if( result.response == true ){
                        let link_image = result.message.directory+result.message.file_name;
                        $("#photo-kegiatan").attr("src", link_image);
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                },
                error: function(response) 
                {   
                    /*Swal.fire(
                      'Error',
                      'Please Contact Admin',
                      'error'
                    )*/
                }           
            })
        }
    });
});