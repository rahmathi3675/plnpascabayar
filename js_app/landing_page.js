    
$(document).ready(function(){

    $('.select2').select2({
        theme: 'bootstrap'
    });

    var table_penggunaan = $('#table-penggunaan').DataTable({ 
        "processing": true, 
        "serverSide": true, 
        "info": false,
        "lengthChange": true,
        "searching": true,
        "ajax": {
            "url": base_url+"Penggunaan_Controller/getPenggunaan",
            "type": "POST",
        },
        "language": {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw text-dark"></i><span class="sr-only">Loading...</span>'
        },
        "scrollX": true,
        "columnDefs": [
            { "width": "80px", "targets": 0 ,"className": "dt-center"},
            { "width": "100px", "targets": 1 ,"className": "dt-center"},
            { "width": "100px", "targets": 2 ,"className": "dt-left"},
            { "width": "200px", "targets": 3 ,"className": "dt-left"},
            { "width": "100px", "targets": 4 ,"className": "dt-center"},
            { "width": "150px", "targets": 5 ,"className": "dt-center"},
            { "width": "50px", "targets": 6 ,"className": "dt-center"},
            { "width": "50px", "targets": 7 ,"className": "dt-center"},
            { "width": "50px", "targets": 8 ,"className": "dt-center"},
            { "width": "75px", "targets": 9 ,"className": "dt-center"},
            { "width": "75px", "targets": 10 ,"className": "dt-center"},
        ],
        "order": [[ 1, 'desc' ]],
        "fixedColumns": false,
        drawCallback: function() {
        
        }  
    });

    $('#form-penggunaan').on('submit', function(e){
        e.preventDefault();

        let id_penggunaan = $('#id-penggunaan').val();
        let id_pelanggan    = $('#id-pelanggan-penggunaan').val();
        let bulan    = $('#bulan-penggunaan').val();
        let tahun    = $('#tahun-penggunaan').val();
        let meter_awal    = $('#kwh-awal-penggunaan').val();
        let meter_akhir    = $('#kwh-akhir-penggunaan').val();

        if( id_pelanggan  == '' && bulan  == '' && tahun  == '' && meter_awal  == '' && meter_akhir == '' ) {
            Swal.fire({
                icon: 'error',
                title: '<strong>Information</strong>',
                html: "Form Tidak Boleh Kosong",
                showCloseButton: false,
                showCancelButton: false,
            })
        }
        else{

            var form = document.forms.namedItem("form_penggunaan");
            $.ajax({
                url: base_url+'/Penggunaan_Controller/createPenggunaan',
                type: "POST",
                data:  new FormData(form),
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                success: function(response)
                {
                    if(response.status == true){       
                       
                        table_penggunaan.ajax.reload();
                    }
                    else{
                        let html = "";
                        for( i = 0; i < response.line.length; i++){
                            html += '* '+ response.line[i].message+'<br>';
                        }
                        Swal.fire({
                            icon: 'error',
                            title: '<strong>Information</strong>',
                            html: html,
                            showCloseButton: false,
                            showCancelButton: false,
                        }).then(function(result){
                            
                        });
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                    clearForm();
                    //window.location.href=base_url;
                }                
            });
        }
    });

    function clearForm() {
        $('#id-penggunaan').val();
        $('#id-pelanggan-penggunaan').val('').change();
        $('#bulan-penggunaan').val('').change();
        $('#tahun-penggunaan').val('').change();
        $('#kwh-awal-penggunaan').val('');
        $('#kwh-akhir-penggunaan').val('');
    }

    $(document).on('click','.btn-edit-penggunaan',function(){
        let id_penggunaan = $(this).attr('data-id');
        $.ajax({
            url: base_url+'/Penggunaan_Controller/getDetailPenggunaan',
            type: "POST",
            data: {
                id_penggunaan: id_penggunaan
            },
            dataType: 'json',
            beforeSend: function(){
                $('#loading-page').fadeIn();
            },
            success: function(response)
            {
                $('#id-penggunaan').val( response.id_penggunaan);
                $('#id-pelanggan-penggunaan').val( response.id_pelanggan).change();
                $('#bulan-penggunaan').val(response.bulan).change();
                $('#tahun-penggunaan').val(response.tahun).change();
                $('#kwh-awal-penggunaan').val(response.meter_awal);
                $('#kwh-akhir-penggunaan').val(response.meter_akhir);
            },
            complete: function(){
                $('#loading-page').fadeOut();
                //window.location.href=base_url;
            }
        });
    });

    $(document).on('click','.btn-delete-penggunaan',function(){
        let id_penggunaan = $(this).attr('data-id');
        $.ajax({
            url: base_url+'/Penggunaan_Controller/deletePenggunaan',
            type: "POST",
            data: {
                id_penggunaan: id_penggunaan
            },
            dataType: 'json',
            beforeSend: function(){
                $('#loading-page').fadeIn();
            },
            success: function(response)
            {
                table_penggunaan.ajax.reload();
            },
            complete: function(){
                $('#loading-page').fadeOut();
                //window.location.href=base_url;
            }
        });
    });

    $(document).on('click','.btn-delete-penggunaan',function(){
        let id_penggunaan = $(this).attr('data-id');
        $.ajax({
            url: base_url+'/Penggunaan_Controller/deleteDetailPenggunaan',
            type: "POST",
            data: {
                id_penggunaan: id_penggunaan
            },
            dataType: 'json',
            beforeSend: function(){
                $('#loading-page').fadeIn();
            },
            success: function(response)
            {
                $('#id-penggunaan').val( response.id_penggunaan);
                $('#id-pelanggan-penggunaan').val( response.id_pelanggan).change();
                $('#bulan-penggunaan').val(response.bulan).change();
                $('#tahun-penggunaan').val(response.tahun).change();
                $('#kwh-awal-penggunaan').val(response.meter_awal);
                $('#kwh-akhir-penggunaan').val(response.meter_akhir);
            },
            complete: function(){
                $('#loading-page').fadeOut();
                //window.location.href=base_url;
            }
        });
    });






    // TAGIHAN
    var table_tagihan = $('#table-tagihan').DataTable({ 
        "processing": true, 
        "serverSide": true, 
        "info": false,
        "lengthChange": true,
        "searching": true,
        "ajax": {
            "url": base_url+"Tagihan_Controller/getTagihan",
            "type": "POST",
        },
        "language": {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw text-dark"></i><span class="sr-only">Loading...</span>'
        },
        "scrollX": true,
        "columnDefs": [
            { "width": "80px", "targets": 0 ,"className": "dt-center"},
            { "width": "100px", "targets": 1 ,"className": "dt-center"},
            { "width": "200px", "targets": 2 ,"className": "dt-left"},
            { "width": "100px", "targets": 3 ,"className": "dt-left"},
            { "width": "100px", "targets": 4 ,"className": "dt-center"},
            { "width": "100px", "targets": 5 ,"className": "dt-center"},
            { "width": "100px", "targets": 6 ,"className": "dt-center"},
            { "width": "100px", "targets": 7 ,"className": "dt-center"},
            { "width": "100px", "targets": 8 ,"className": "dt-center"},
            { "width": "150px", "targets": 9 ,"className": "dt-center"}
            /*
            { "width": "150px", "targets": 6 ,"className": "dt-center"},
            { "width": "150px", "targets": 7 ,"className": "dt-center"}*/
        ],
        "order": [[ 1, 'desc' ]],
        "fixedColumns": false,
        "autoWidth": false,
        /*"dom": `<'row'<'col-md-6 col-sm-6'><'col-md-6 col-sm-6 text-right mb-2'B>>
                <'row'<'col-md-6 col-sm-6 col-xs-6'l><'col-md-6 col-sm-6 col-xs-6'f>>
                <'row'<'col-md-12't>>
                <'row'<'col-md-12'p>>`,
        "buttons": [{
                "extend": 'colvis',
                "columns": ':gt(0)',
                "collectionLayout": 'two-column',
                "text":'<i class="fas fa-list"></i>'
        }],*/
        /*"colVis": {
            "buttonText": "Change columns"
        },*/
        drawCallback: function() {
        }  
    });

    $('.nav-item').on('click', function(){

        /*table_tagihan.fixedHeader.adjust();
        table_pembayaran.fixedHeader.adjust();
        table_penggunaan.fixedHeader.adjust();*/
        table_tagihan.columns.adjust().draw();
        table_pembayaran.columns.adjust().draw();
        table_penggunaan.columns.adjust().draw();
    }); 



    // PEMBAYARARN 

    var table_pembayaran = $('#table-pembayaran').DataTable({ 
        "processing": true, 
        "serverSide": true, 
        "info": false,
        "lengthChange": true,
        "searching": true,
        "ajax": {
            "url": base_url+"Pembayaran_Controller/getPembayaran",
            "type": "POST",
        },
        "language": {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw text-dark"></i><span class="sr-only">Loading...</span>'
        },
        "scrollX": true,
        "columnDefs": [
            { "width": "80px", "targets": 0 ,"className": "dt-center"},
            { "width": "130px", "targets": 1 ,"className": "dt-center"},
            { "width": "130px", "targets": 2 ,"className": "dt-left"},
            { "width": "200px", "targets": 3 ,"className": "dt-left"},
            { "width": "150px", "targets": 4 ,"className": "dt-center"},
            { "width": "100px", "targets": 5 ,"className": "dt-center"},
            { "width": "100px", "targets": 6 ,"className": "dt-center"},
            { "width": "100px", "targets": 7 ,"className": "dt-center"},
            { "width": "100px", "targets": 8 ,"className": "dt-center"},
            { "width": "200px", "targets": 9 ,"className": "dt-center"},
            { "width": "150px", "targets": 10 ,"className": "dt-right"},
            { "width": "200px", "targets": 11 ,"className": "dt-center"},
            { "width": "200px", "targets": 12 ,"className": "dt-center"},
            /*
            { "width": "150px", "targets": 6 ,"className": "dt-center"},
            { "width": "150px", "targets": 7 ,"className": "dt-center"}*/
        ],
        "order": [[ 1, 'desc' ]],
        "fixedColumns": false,
        "autoWidth": false,
        /*"dom": `<'row'<'col-md-6 col-sm-6'><'col-md-6 col-sm-6 text-right mb-2'B>>
                <'row'<'col-md-6 col-sm-6 col-xs-6'l><'col-md-6 col-sm-6 col-xs-6'f>>
                <'row'<'col-md-12't>>
                <'row'<'col-md-12'p>>`,
        "buttons": [{
                "extend": 'colvis',
                "columns": ':gt(0)',
                "collectionLayout": 'two-column',
                "text":'<i class="fas fa-list"></i>'
        }],*/
        /*"colVis": {
            "buttonText": "Change columns"
        },*/
        drawCallback: function() {
        }  
    });

    $(document).on('click','.btn-edit-pembayaran',function(){
        let id_penggunaan = $(this).attr('data-id');
        $.ajax({
            url: base_url+'/Pembayaran_Controller/getDetailPembayaran',
            type: "POST",
            data: {
                id_penggunaan: id_penggunaan
            },
            dataType: 'json',
            beforeSend: function(){
                $('#loading-page').fadeIn();
            },
            success: function(response)
            {
                $('#id-penggunaan-pembayaran').val( response.id_penggunaan);
                $('#id-tagihan-penggunaan').val( response.id_tagihan);
                $('#bulan-penggunaan-pembayaran').val(response.bulan);
                $('#id-pelanggan-pembayaran').val(response.id_pelanggan);
            },
            complete: function(){
                $('#loading-page').fadeOut();
                //window.location.href=base_url;
            }
        });
    });

    $('#form-pembayaran').on('submit', function(e){
        e.preventDefault();

        let id_tagihan = $('#id-tagihan-pembayaran').val();
        let id_pelanggan    = $('#id-pelanggan-pembayaran').val();
        let bulan    = $('#bulan-penggunaan').val();
        let biaya_admin    = $('#total-biaya-admin').val();
        let total_bayar    = $('#total_pembayaran').val();

        if( id_tagihan  == '' && id_pelanggan  == '' && bulan  == '' && biaya_admin  == '' && total_bayar == '' ) {
            Swal.fire({
                icon: 'error',
                title: '<strong>Information</strong>',
                html: "Form Tidak Boleh Kosong",
                showCloseButton: false,
                showCancelButton: false,
            })
        }
        else{

            var form = document.forms.namedItem("form_pembayaran");
            $.ajax({
                url: base_url+'/Pembayaran_Controller/createPembayaran',
                type: "POST",
                data:  new FormData(form),
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                success: function(response)
                {
                    if(response.status == true){       
                       
                        table_pembayaran.ajax.reload();
                    }
                    else{
                        let html = "";
                        for( i = 0; i < response.line.length; i++){
                            html += '* '+ response.line[i].message+'<br>';
                        }
                        Swal.fire({
                            icon: 'error',
                            title: '<strong>Information</strong>',
                            html: html,
                            showCloseButton: false,
                            showCancelButton: false,
                        }).then(function(result){
                            
                        });
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                    clearForm();
                    //window.location.href=base_url;
                }                
            });
        }
    });

    $('#btn-login').on('click',function(){
        
        let username    = $('#username').val();
        let password    = $('#password').val();

        if( password == '' && username == '' ) {
            Swal.fire({
                icon: 'error',
                title: '<strong>Information</strong>',
                html: "Username & Password Tidak Boleh Kosong",
                showCloseButton: false,
                showCancelButton: false,
            })
        }
        else{

            var form = document.forms.namedItem("form_verifikasi");
            $.ajax({
                url: base_url+'/Landing_Page_Controller/prosesLogin',
                type: "POST",
                data:  new FormData(form),
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                success: function(response)
                {
                    if(response.status == true){       
                       

                        $('#loading-page').fadeOut();
                        window.location.href=base_url;
                    }
                    else{
                        let html = "";
                        for( i = 0; i < response.line.length; i++){
                            html += '* '+ response.line[i].message+'<br>';
                        }
                        Swal.fire({
                            icon: 'error',
                            title: '<strong>Information</strong>',
                            html: html,
                            showCloseButton: false,
                            showCancelButton: false,
                        }).then(function(result){
                            
                        });
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                    //window.location.href=base_url;
                },
                error: function(response) 
                {   
                    /*Swal.fire(
                      'Error',
                      'Please Contact Admin',
                      'error'
                    )*/
                }           
                
            });
        }
    });
})