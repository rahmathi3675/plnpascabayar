

$(document).ready( function(){



    $('.select2').select2({
        theme: 'bootstrap'
    });

    var table = $('#table-list-kategori-kegiatan').DataTable({ 
        "processing": true, 
        "serverSide": true, 
        "info": false,
        "lengthChange": true,
        "searching": true,
        "ajax": {
            "url": base_url+"Jenis_Kegiatan_Controller/getDataJenis",
            "type": "POST",
        },
        "language": {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw text-dark"></i><span class="sr-only">Loading...</span>'
        },
        "scrollX": true,
        "columnDefs": [
            { "width": "80px", "targets": 0 ,"className": "dt-center"},
            { "width": "50px", "targets": 1 ,"className": "dt-center"},
            { "width": "250px", "targets": 2 ,"className": "dt-left"},
            { "width": "150px", "targets": 3 ,"className": "dt-left"},
            { "width": "150px", "targets": 4 ,"className": "dt-center"},
            { "width": "150px", "targets": 5 ,"className": "dt-center"},
            { "width": "150px", "targets": 6 ,"className": "dt-center"}
        ],
        "order": [[ 1, 'desc' ]],
        "fixedColumns": false,
        "dom": `<'row'<'col-md-6 col-sm-6'><'col-md-6 col-sm-6 text-right mb-2'B>>
                <'row'<'col-md-6 col-sm-6 col-xs-6'l><'col-md-6 col-sm-6 col-xs-6'f>>
                <'row'<'col-md-12't>>
                <'row'<'col-md-12'p>>`,
        "buttons": [{
                "extend": 'colvis',
                "columns": ':gt(0)',
                "collectionLayout": 'two-column',
                "text":'<i class="fas fa-list"></i>'
        }],
        "colVis": {
            "buttonText": "Change columns"
        },
        drawCallback: function() {
        }  
    });
});