

$(document).ready( function(){



    $('.select2').select2({
        theme: 'bootstrap'
    });

    var table = $('#table-report-my-course').DataTable({ 
        "processing": true, 
        "serverSide": true, 
        "info": false,
        "lengthChange": false,
        "searching": false,
        "pageLength": 500,
        "ajax": {
            "url": base_url+"Report_Controller/getMyReport",
            "type": "POST",
        },
        "language": {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw text-dark"></i><span class="sr-only">Loading...</span>'
        },
        "scrollX": true,
        "columnDefs": [
            { "width": "100px", "targets": 0 ,"className": "dt-center"},
            { "width": "150px", "targets": 1 ,"className": "dt-center"},
            { "width": "150px", "targets": 2 ,"className": "dt-center"},
            { "width": "200px", "targets": 3 ,"className": "dt-left"},
            { "width": "100px", "targets": 4 ,"className": "dt-center"},
            { "width": "50px", "targets": 5 ,"className": "dt-center"},
            { "width": "100px", "targets": 6 ,"className": "dt-center"},
            { "width": "150px", "targets": 7 ,"className": "dt-left"},
        ],
        "order": [[ 1, 'desc' ]],
        "fixedColumns": false,
        "dom": 
            `<'row'
                <'col-md-6 col-sm-6'><'col-md-6 col-sm-6 text-right mb-2'B>
            >
            <'row'
                <'col-md-6 col-sm-6 col-xs-6'l>
                <'col-md-6 col-sm-6 col-xs-6'f>
            >
            <'row'
                <'col-md-12't>
            >
            <'row'
                <'col-md-12'p>
            >`,
        "buttons": [
            'pdf', 'print',
            {
                "extend": 'colvis',
                "columns": ':gt(0)',
                "collectionLayout": 'two-column',
                "text":'<i class="fas fa-list"></i>',
                
            }
        ],
        "colVis": {
            "buttonText": "Change columns"
        },
        drawCallback: function() {
            $('#title').text('LAPORAN HASIL KEGIATAN');
        }  
    });

     var table = $('#table-membership').DataTable({ 
        "processing": true, 
        "serverSide": true, 
        "info": false,
        "lengthChange": false,
        "searching": false,
        "pageLength": 500,
        "ajax": {
            "url": base_url+"Report_Controller/getMyMembershipReport",
            "type": "POST",
        },
        "language": {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw text-dark"></i><span class="sr-only">Loading...</span>'
        },
        "scrollX": true,
        "columnDefs": [
            { "width": "100px", "targets": 0 ,"className": "dt-center"},
            { "width": "150px", "targets": 1 ,"className": "dt-center"},
            { "width": "150px", "targets": 2 ,"className": "dt-center"},
            { "width": "200px", "targets": 3 ,"className": "dt-left"},
            { "width": "100px", "targets": 4 ,"className": "dt-center"},
            { "width": "50px", "targets": 5 ,"className": "dt-center"},
            { "width": "100px", "targets": 6 ,"className": "dt-center"},
            { "width": "150px", "targets": 7 ,"className": "dt-left"},
        ],
        "order": [[ 1, 'desc' ]],
        "fixedColumns": false,
        "dom": 
            `<'row'
                <'col-md-6 col-sm-6'><'col-md-6 col-sm-6 text-right mb-2'B>
            >
            <'row'
                <'col-md-6 col-sm-6 col-xs-6'l>
                <'col-md-6 col-sm-6 col-xs-6'f>
            >
            <'row'
                <'col-md-12't>
            >
            <'row'
                <'col-md-12'p>
            >`,
        "buttons": [
            'pdf', 'print',
            {
                "extend": 'colvis',
                "columns": ':gt(0)',
                "collectionLayout": 'two-column',
                "text":'<i class="fas fa-list"></i>',
                
            }
        ],
        "colVis": {
            "buttonText": "Change columns"
        },
        drawCallback: function() {
            $('#title').text('LAPORAN KEANGGOTAAN');
        }  
    });

    
});