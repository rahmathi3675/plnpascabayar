$('.navbar, #custom-side-bar').hide();


$(document).ready( function(){

    var persen = 0;

    $('#btn-login').on('click',function(){
        
        let username    = $('#username').val();
        let password    = $('#password').val();

        if( password == '' && username == '' ) {
            Swal.fire({
                icon: 'error',
                title: '<strong>Information</strong>',
                html: "Username & Password Tidak Boleh Kosong",
                showCloseButton: false,
                showCancelButton: false,
            })
        }
        else{

            var form = document.forms.namedItem("form_verifikasi");
            $.ajax({
                url: base_url+'/Landing_Page_Controller/prosesLogin',
                type: "POST",
                data:  new FormData(form),
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                success: function(response)
                {
                    if(response.response == true){       
                       

                        $('#loading-page').fadeOut();

                    }
                    else{
                        let html = "";
                        for( i = 0; i < response.line.length; i++){
                            html += '* '+ response.line[i].message+'<br>';
                        }
                        Swal.fire({
                            icon: 'error',
                            title: '<strong>Information</strong>',
                            html: html,
                            showCloseButton: false,
                            showCancelButton: false,
                        })
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                    window.location.href=base_url;
                },
                error: function(response) 
                {   
                    /*Swal.fire(
                      'Error',
                      'Please Contact Admin',
                      'error'
                    )*/
                }           
                
            });
        }
    });

});