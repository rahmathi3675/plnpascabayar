

$(document).ready( function(){

    $('.select2').select2({
        theme: 'bootstrap'
    });

    $('.datetimepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });


    $('#file-upload-cover').on('change', function(){

        var file_data = $(this).prop('files')[0];
        let kode_kegiatan = $('#kode-kegiatan').val(); 
        var form_data = new FormData();
        form_data.append('file_upload_cover', file_data);
        form_data.append('kode_kegiatan', kode_kegiatan);
        form_data.append('type', 'cover');
        form_data.append('file_name', 'file_upload_cover');

        
        if( file_data != '') {
            $.ajax({
                url: base_url+"Modul_Controller/uploadPhotoAjax",
                type: "POST",
                data: form_data,
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.myprogress').text(percentComplete + '%');
                            $('.myprogress').css('width', percentComplete + '%');
                        }
                    }, 
                    false);
                    return xhr;
                },
                success: function( result )
                {
                    if( result.status == true ){
                    console.log(result);
                        let link_image = result.data.message.directory+result.data.message.file_name;
                        $("#photo-cover").attr("src", link_image);
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                },
                error: function(response) 
                {   
                    /*Swal.fire(
                      'Error',
                      'Please Contact Admin',
                      'error'
                    )*/
                }           
            })
        }
    });

     $('#file-upload-modul').on('change', function(){

        var file_data = $(this).prop('files')[0];
        let kode_kegiatan = $('#kode-kegiatan').val(); 
        var form_data = new FormData();
        form_data.append('file_upload_modul', file_data);
        form_data.append('kode_kegiatan', kode_kegiatan);
        form_data.append('type', 'modul');
        form_data.append('file_name', 'file_upload_modul');

        
        if( file_data != '') {
            $.ajax({
                url: base_url+"Modul_Controller/uploadPhotoAjax",
                type: "POST",
                data: form_data,
                contentType: false,
                processData: false,
                cache: false,
                dataType: 'json',
                beforeSend: function(){
                    $('#loading-page').fadeIn();
                },
                xhr: function () {
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            percentComplete = parseInt(percentComplete * 100);
                            $('.myprogress').text(percentComplete + '%');
                            $('.myprogress').css('width', percentComplete + '%');
                        }
                    }, 
                    false);
                    return xhr;
                },
                success: function( result )
                {
                    if( result.status == true ){
                    console.log(result);
                        let link_image = result.data.message.directory+result.data.message.file_name;
                        $("#photo-modul").attr("src", link_image);
                    }
                },
                complete: function(){
                    $('#loading-page').fadeOut();
                },
                error: function(response) 
                {   
                    /*Swal.fire(
                      'Error',
                      'Please Contact Admin',
                      'error'
                    )*/
                }           
            })
        }
    });
});