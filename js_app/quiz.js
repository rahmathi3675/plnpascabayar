

$(document).ready( function(){

    $('.select2').select2({
        theme: 'bootstrap'
    });

    $('.datetimepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });

    var table_detail = $('#table-materi-quiz').DataTable({
        "processing": true, 
        "serverSide": true, 
        "info": false,
        "lengthChange": false,
        "searching": false,
        "ajax": {
            "url": base_url+"Quiz_Controller/getDataQuiz",
            "type": "POST",
            "data": {
                'kode_kegiatan': $('#kode-kegiatan').val()
            }
        },
        "language": {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw text-dark"></i><span class="sr-only">Loading...</span>'
        },
        "scrollX": true,
        "columnDefs": [
            { "width": "70px", "targets": 0 ,"className": "dt-center"},
            { "width": "500px", "targets": 1 ,"className": "dt-left"}
        ],
        "order": [[ 1, 'desc' ]],
        "fixedColumns": false,
        "pageLength": 3,
        drawCallback: function() {
        }  
    });


    $('#form-create-quiz').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            url: base_url+"Quiz_Controller/createQuiz",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'json',
            beforeSend: function(){
                $('#loading-page').fadeIn();
            },
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $('.myprogress').text(percentComplete + '%');
                        $('.myprogress').css('width', percentComplete + '%');
                    }
                }, 
                false);
                return xhr;
            },
            success: function( result )
            {
                if( result.status == true){
                    table_detail.ajax.reload();
                }
            },
            complete: function(){
                $('#loading-page').fadeOut();
            }        
        })
    });

    $('#form-edit-quiz').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            url: base_url+"Quiz_Controller/updateQuiz",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'json',
            beforeSend: function(){
                $('#loading-page').fadeIn();
            },
            success: function( result )
            {
                if( result.status == true){
                    table_detail.ajax.reload();
                }
            },
            complete: function(){
                $('#loading-page').fadeOut();
            }        
        })
    });

    $(document).on('click','.btn-edit', function(){

        $('#form-create-quiz').hide();
        $('#form-edit-quiz').fadeIn();

        let kode_quiz = $(this).attr('data-id');
        $.ajax({
            url: base_url+"Quiz_Controller/getDetailQuiz",
            type: "POST",
            data: {
                'kode_materi': kode_quiz
            },
            dataType: 'json',
            beforeSend: function(){
                $('#loading-page').fadeIn();
            },
            success: function( response )
            {
                if( response.status == true ){

                    $('#kode-materi').val( response.data.header.kode_materi );
                    $('#deskripsi-materi').val( response.data.header.deskripsi_materi );
                    let i = 0;
                    let value = 0;
                    let checked = "";
                    $('#form-jawaban').empty();
                    while( i< response.data.detail.length){
                        //  console.log(response.data.detail[i]);
                        if( response.data.detail[i].is_key == 1){
                            value = 1;
                            checked = " checked ";
                        }
                        else{
                            value = 0;
                            checked = "  ";
                        }

                        $('#form-jawaban').append(`
                            <div class="input-group input-group-md mb-3">
                                <div class="input-group-prepend">
                                    <input type="radio" `+checked+` name="is_key" value="`+i+`" style="display: -webkit-box; margin-top: 15px !important;" required>
                                </div>
                                <div class="input-group-prepend">
                                    <i class="p-2">( `+response.data.detail[i].value+` )</i> &nbsp; 
                                </div>
                                <input type="hidden" readonly name="kode_detail[]" required value="`+response.data.detail[i].kode_jawaban+`">
                                <input type="text" class="form-control form-biodata jawaban_edit" aria-label="Small" aria-describedby="inputGroup-sizing-md" name="jawaban_edit[]" required value="`+response.data.detail[i].deskripsi_jawaban+`">
                            </div>
                        `);
                        i++;
                    }
                }
            },
            complete: function(){
                $('#loading-page').fadeOut();
            }        
        })
    });
});