    
$(document).ready(function(){

    $('.select2').select2({
        theme: 'bootstrap'
    });

    var list_data = [70,50,90];

    $("#input-menu").focus();
    
    $("#input-menu").keyup(function(event) {
        if (event.which === 13) {
            let menu = $(this).val();
            
            if( menu == 3 ){

                $('#div-menu').hide();
                $('#div-menu-searching').fadeIn();
                $("#input-nilai-search").focus();
            }
            else if( menu == 1){
                searchListDataArray(list_data);
                $('#div-menu').hide();
                $('#div-menu-input').fadeIn();
                $("#input-nilai-angka").focus();   
            }
            else if( menu == 2){
                $('#div-menu').hide();
                $('#div-menu-sortir').fadeIn();   

                list_data.sort();
                sortDataArray(list_data);
            }
        }
    });


    $("#input-nilai-search").keyup(function(event) {
        if (event.which === 13) {

            let nilai = $(this).val();
            
            if( nilai != '' ){

                $('#div-note-searching').hide();

                 //if string in array
                let found = list_data.find( element => element == nilai);

                if( found ){
                    $('#hasil-searching').text( found );
                }                
                else{
                    $('#hasil-searching').text( "Angka Tidak Ditemukan" );

                }
                $('#div-hasil-searching').fadeIn();
            }
        }
    });

    function sortDataArray(list_data){
     
        let i = 0;
        while( i < list_data.length ){
            $('#list-hasil-sortir').append(list_data[i]+`,`);
            i++;
        }
    }

    function searchListDataArray( list_data ){
        let i = 0;
    
        while( i < list_data.length ){
            $('#list-array').append(`
                <h6 class="font-light">Angka `+(i+1)+` : `+list_data[i]+`</h6>
            `);
            i++;
        }
    }

    $("#input-nilai-angka").keyup(function(event) {
        if (event.which === 13) {

            let nilai = $(this).val();
            if( nilai != '' ){
                $('#div-note-input').fadeIn();
                let found = list_data.find( element => element == nilai);
                if( found ){
                    $('#hasil-searching').text( found );
                }                
                else{
                    list_data.push( parseInt(nilai));
                    $('#list-array').empty();
                    searchListDataArray(list_data);
                }
                $('#div-hasil-searching').fadeIn();
            }
        }
    });
})