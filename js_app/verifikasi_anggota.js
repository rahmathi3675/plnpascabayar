

$(document).ready( function(){

    $('.datetimepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('.select2').select2({
        theme: 'bootstrap'
    });

    /*$('#postal-code').select2({
        allowClear: true,
        minimumInputLength: 3,
        tags: false,
        ajax: {
            url: base_url+ '/Anggota_Controller/getPostalCode',
            dataType: 'json',
            type: "POST",
            data: function (keywords) {
                return {
                    postal_code: keywords
                };
            },
            processResults: function (ParseDataJson) {
                return {
                    results: $.map(ParseDataJson.data, function (item) {
                        return {text: item.postal_code.toUpperCase()+', '+ item.province.toUpperCase() +', '+ item.city.toUpperCase() +', '+item.kecamatan.toUpperCase()+', '+item.kelurahan.toUpperCase(), 
                        id: item.postal_code+','+item.id_province+','+item.id_city+','+item.kecamatan+','+item.kelurahan+','}
            
                    })
                };
            }
        }
    });*/

    $('#postal-code').on('change', function(){
        let address = $(this).text();
        let rincian = address.split(',');
        console.log(rincian);
        $("#provinsi").val(rincian[1]);
        $("#kabupaten-kota").val(rincian[2]);
        $("#kecamatan").val(rincian[3]);
        $("#kelurahan").val(rincian[4]);
    });

    var persen = 0;
    
    function make_base_auth(user, password) {
        var tok = user + ':' + password;
        var hash = btoa(tok);
        return 'Basic ' + hash;
    }

    $('#btn-verifikasi').on('click',function(){

        var form = document.forms.namedItem("form_verifikasi");
        $.ajax({
            url: base_url+'/Anggota_Controller/confirmVerification',
            type: "POST",
            data:  new FormData(form),
            contentType: false,
            processData: false,
            cache: false,
            dataType: 'json',
            beforeSend: function(){
                $('#loading-page').fadeIn();
            },
            success: function(response)
            {
                if(response.response == true){
                    
                   
                    Swal.fire({
                        icon: 'success',
                        title: '<strong>Verifikasi</strong>',
                        html: "Sukses Verifikasi Data",
                        showCloseButton: false,
                        showCancelButton: false,
                    });
                }
                else{
                    let html = "";
                    for( i = 0; i < response.line.length; i++){
                        html += '* '+ response.line[i].message+'<br>';
                    }
                    Swal.fire({
                        icon: 'error',
                        title: '<strong>Verifikasi</strong>',
                        html: html,
                        showCloseButton: false,
                        showCancelButton: false,
                    })
                }
            },
            complete: function(){
                $('#loading-page').fadeOut();
            },
            error: function(response) 
            {   
                /*Swal.fire(
                  'Error',
                  'Please Contact Admin',
                  'error'
                )*/
            }           
            
        });
    });

});